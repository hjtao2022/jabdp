<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<html>
<head>
	<title>iDesigner</title>
	<%@ include file="/common/meta.jsp" %>
	<script type="text/javascript" src="${ctx}/js/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" src="${ctx}/js/ueditor/editor_api.js"></script>
	<script type="text/javascript" src="${ctx}/js/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript" src="${ctx}/js/jquery/jquery-1.7.2.min.js"></script>
  	<script type="text/javascript" src="${ctx}/js/ztree/jquery.ztree.all-3.2.min.js"></script>
  	<link rel="stylesheet" type="text/css" href="${ctx}/js/ztree/themes/blue/zTreeStyle.css">
  	<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/themes/blue/easyui.css">
  	<style type="text/css">
    	ul.ztree.zTreeDragUL{z-index:99999}
    </style>
  	<script type="text/javascript">
		var editor;
		//ztree配置
		var setting = {
			view: {
				selectedMulti: false
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				onDblClick: zTreeOnDblClick,
				onMouseDown: zTreeOnMouseDown,
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop
			}
		};
		//module里面存放数据的地方，有什呢？自己用console.log(module)看吧
		var module = window.opener.parent.$.fn.module.getModule();
		//构建ztree
		var zNodes = [{id:1, pId:0, name:module.dataSource.formList[0].caption, icon:"${ctx}/js/ztree/themes/blue/img/diy/2.png", node_type:"root"}];
		//字段数量
		var field_num = 0;
		//var myform = module.dataSource.formList[0];
		var myFormKey = window.opener.parent.$.fn.module.getTabList(${param.rows},${param.cols}).form;
		var myform = window.opener.parent.$.fn.module.getFormByKey(myFormKey);
		for(var i=0; i<myform.fieldList.length; i++) {
			//console.log(myform.fieldList[i]);
			var field = myform.fieldList[i];
			if(field.key && (!myform.isMaster || (field.tabRows==${param.rows} && field.tabCols==${param.cols})) && !field.isCaption) {
				//var content = myform.fieldList[i];
				//字段名必填字段加提示
				var name = "";
				if(field.dataProperties.notNull) {
					name = "[非空]" + field.caption + "[字段]";
				}else{
					name = field.caption + "[字段]";
				}
				zNodes.push({id:10+i, pId:1, name:name, icon:"${ctx}/js/ztree/themes/blue/img/diy/1_close.png", node_type:"field",formindex:myform.index, content:field, tablekey:myform.key});
				//控件
				var type = field.caption + "[控件]";
				zNodes.push({id:10+i, pId:1, name:type, icon:"${ctx}/js/ztree/themes/blue/img/diy/1_open.png", node_type:"control",formindex:myform.index, content:field, tablekey:myform.key});
				field_num++;
			}
		}
		/* 初始化 */
		$(document).ready(function() {
			var rows = ${param.rows};
			var cols = ${param.cols};
			var x = window.opener.parent.$.fn.module.getTabList(rows,cols);
			//将content的内容填入editor中
			editor = UE.getEditor("myEditor",{"initialContent":x.content.html});
			editor.setOpt('mytopsize_options',x.content.paddingTop);
			editor.setOpt('mybottomsize_options',x.content.paddingBottom);
			editor.setOpt('myleftsize_options',x.content.paddingLeft);
			editor.setOpt('myrightsize_options',x.content.paddingRight);
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		});
		/* ztree整树数据导入table,双击事件 */
		function zTreeOnDblClick(event, treeId, treeNode) {
			var rows = ${param.rows};
			var cols = ${param.cols};
			var x = window.opener.parent.$.fn.module.getTabList(rows,cols);
			if(!editor.hasContents()&&treeNode.id=="1") {
				var opt = {
						numcol:'',
						numrow:''
			    	};
				opt.numcol = x.tableCols * 2;
				opt.numrow = Math.ceil(field_num/x.tableCols);
				editor.execCommand('myautocreatetable', opt);
				var index = 0;
				for(var i=0; i<myform.fieldList.length; i++) {
					var field = myform.fieldList[i];
					var node = {
							index:"",
							html:""
					};
					if(field.key!="" && (!myform.isMaster || (field.tabRows==${param.rows} && field.tabCols==${param.cols})) && !field.isCaption) {
						node.index = index * 2;
						node.html = '<span style="text-decoration: underline; padding:0; font-size:16">'
						+ field.caption
						+ ':</span>';
						//写入字段名
						editor.execCommand('ztreeauto', node);
						node.index = index * 2 + 1;
						node.html = "<img id='" + myFormKey + "_" 
						+ field.key + "_"
						+ field.editProperties.editType
						+ "' style='height:100%;width:100%;display:block' src='" + "${ctx}/jsp/editTypeImages/"
						+ window.opener.parent.$.fn.module.getEditTypeImgDic()[field.editProperties.editType]
						+ "' formindex='" + myform.index + "' fieldindex='"
						+ field.index +"'/>";
						//写入控件
						editor.execCommand('ztreeauto', node);
						index++;
					}
				}
			}
		};
		/* 鼠标按下事件 */
		function zTreeOnMouseDown(event, treeId, treeNode) {
			//判断被拖动的是控件还是标签，如果是控件node=生成控件对应的表示图片，是标签node=字段名
			var node = "";
			if(treeNode) {
				if(treeNode.node_type == "control") {
					node = "<img style='height:100%;width:100%;display:block' src='" + "${ctx}/jsp/editTypeImages/"
					+ window.opener.parent.$.fn.module.getEditTypeImgDic()[treeNode.content.editProperties.editType]
					+ "' id = '" + treeNode.tablekey + "_" + treeNode.content.key + "_" 
					+ treeNode.content.editProperties.editType + "' formindex='" + treeNode.formindex + "' fieldindex='"
					+ treeNode.content.index + "'/>";
					//将node传入ueditor，ztree命令负责在ueditor相应位置插入node内容
					editor.execCommand('ztree', node);
				}
				else if(treeNode.node_type == "field") {
					node = '<span style="padding:0; font-size:16">'
					+ treeNode.content.caption
					+ '</span>';
					//将node传入ueditor，ztree命令负责在ueditor相应位置插入node内容
					editor.execCommand('ztree', node);
				}
				else if(treeNode.node_type == "root") {
					//按下的是节点不用操作
				}
			}
		};
		/* 鼠标拖动开始之前 */
		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].node_type == "root") {
					return false;
				}
			}
			return true;
		}
		/* 鼠标拖动结束之前 */
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			return false;
		}
		//保存
		function save() {
			$("#msg_box").text("亲，正在保存中，请稍等片刻...");
			var rows = ${param.rows};
			var cols = ${param.cols};
			var x = window.opener.parent.$.fn.module.getTabList(rows,cols);
			var htmlstr = editor.getContent().replace();
			htmlstr = htmlstr.replace(/<br\/>/g,'');
			htmlstr = htmlstr.replace(/<p><\/p>/g,'');
			x.content = {"html":htmlstr
					,"paddingTop":editor.options.mytopsize_options
					,"paddingBottom":editor.options.mybottomsize_options
					,"paddingLeft":editor.options.myleftsize_options
					,"paddingRight":editor.options.myrightsize_options
					};
			window.opener.replaceTabs(x,"excel");
			//图片宽高复制
			editor.execCommand('ztreecopy', window.opener.parent.$.fn.module);
			/* var myfields = window.opener.parent.$.fn.module.getModule().dataSource.formList[0].fieldList;
			for(var i=0; i<myfields.length; i++) {
				if(myfields[i].caption != "") {
					editor.execCommand('ztreecopy', myfields[i]);
				}
			} */
			$("#msg_box").text("亲，保存成功");
		}
		//完成
		function finish() {
			if(confirm('关闭前是否自动保存？')){
				save();
			}
			window.close();
		}
	</script>
</head>
<body class="easyui-layout" style="margin:0px;z-index:0">
	<div region='west' split=false title='字段' background-color='red' style="width:200px;padding:0px">
		<div style="color:red;"><span>双击根节点即可自动生成字段布局</span></div>
		<div id="treeDemo" class="ztree" style="z-index:9999"></div>
	</div>
	<div id='center' region='center' style="width:100%;padding:0px">
		<script id="myEditor" type="text/plain" style="width:100%;height:92%"></script>
	</div>
	<div region='south' align="center" style="height:30px;padding:1px;">
		<a id="save" class="easyui-linkbutton" iconCls="icon-save" onclick="save()">保存</a>
		<a id="finish" class="easyui-linkbutton" iconCls="icon-ok" onclick="finish()">保存并关闭</a>
		<span id="msg_box" style="color:red;"></span>
	</div>
</body>
</html>