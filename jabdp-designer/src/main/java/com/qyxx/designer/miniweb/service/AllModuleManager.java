package com.qyxx.designer.miniweb.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import com.qyxx.designer.miniweb.cache.ModuleEntityCache;
import com.qyxx.designer.modules.utils.PathUtils;
import com.qyxx.jwp.bean.Field;
import com.qyxx.jwp.bean.Form;
import com.qyxx.jwp.bean.Item;
import com.qyxx.jwp.bean.Module;



@Component
public class AllModuleManager {

	/**
	 * 读取所有模块信息
	 * 
	 * @param path
	 * @param treeId
	 */
	public void readModule(String path,String treeId){
		List<File> file = new ArrayList<File>();
		try {
			List<File> allFile =PathUtils.getSourceList(path, file);
			ConcurrentMap<String,Module> fm = new ConcurrentHashMap<String,Module>();
			for(File l:allFile ){
				Module module = PathUtils.getRootByFile(l.getPath());
				fm.put(module.getModuleProperties().getKey().toLowerCase(), module);
			}
			ModuleEntityCache.getInstance().put(treeId, fm);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
	}
	/**
	 * 获得所有的表名
	 * @param treeid 模块标示
	 * @return */
	public Map<String,String> getTabName(String treeId){
		Map<String,String> allTabName = ModuleEntityCache.getInstance().getAllTable(treeId);
		return allTabName;
	}
	/**
	 * 获得所有的每张表下所有的字段名
	 * @param treeId 模块标示
	 * @param formKey 表的key
	 * @return*/
	public Map<String,String> getFieldName(String treeId,String formKey){
		Map<String,String> fieldsName = ModuleEntityCache.getInstance().getFieldName(treeId, formKey);
		return fieldsName;
	}
	
	/**
	 * 获取所有表
	 * 
	 * @param treeId
	 * @return
	 */
	public List<Item> getModuleTables(String treeId) {
		return ModuleEntityCache.getInstance().getModuleTables(treeId);
	}
	/**
	 * 获取表字段
	 * 
	 * @param treeId
	 * @param entityName
	 * @return
	 */
	public List<Item> getTableFields(String treeId, String entityName) {
		return ModuleEntityCache.getInstance().getTableFields(treeId, entityName);
	}
	/**
	 * 获取所有公用表
	 * 
	 * @param treeId
	 * @return
	 */
	public List<Item> getCommonTables(String treeId) {
		return ModuleEntityCache.getInstance().getModuleCommonTables(treeId);
	}
	/**
	 * 获取公用表字段
	 * 
	 * @param treeId
	 * @param entityName
	 * @return
	 */
	public List<Field>  getCommonTableFields(String treeId, String entityName) {
		return ModuleEntityCache.getInstance().getCommonTableFields(treeId, entityName);
	}
	/**
	 * 获取所有模块表单的表
	 * 
	 * @param treeId
	 * @return
	 */
	public List<Item> getFormsTables(String treeId) {
		return ModuleEntityCache.getInstance().getModuleFormsTables(treeId);
	}
	
	/**
	 * 获取默认查询语句
	 * 
	 * @param treeId
	 * @param entityName
	 * @return
	 */
	public String  getDefaultSql(String treeId, String formKey) {
		Form form = ModuleEntityCache.getInstance().getForm(treeId, formKey);
		return form.buildHql();
	}
	
	/**
	 * 把map转换成obj类型
	 * @param map map集合
	 * @return obj*/
	/*public Object covMapToObj(Map<String,String> map){
		List<fieldEntity> fe = new ArrayList();
		Iterator ite = map.entrySet().iterator();
		int flag = 0;
		while(ite.hasNext()){
			  fieldEntity fl = new fieldEntity();;
			  Map.Entry entry = (Map.Entry)ite.next(); 
			  fl.setKey(entry.getKey().toString());
			  fl.setCaption(entry.getValue().toString());
			  fe.set(flag, fl);
			  flag++;
			 
		}
		return fe;
	}*/
	
	public static void main(String[] args) {
		AllModuleManager amm = new AllModuleManager();
		amm.readModule("E:/project/hj/20140922/config/module", "hj");
		List<Item> tableList = amm.getModuleTables("hj");
		StringBuffer sb = new StringBuffer();
		for(Item it : tableList) {
			sb.append("/*" + it.getCaption() + "*/\r\n");
			sb.append("delete from ");
			sb.append(it.getColumn() + ";");
			sb.append("\r\n");
		}
		try {
			FileUtils.writeStringToFile(new File("E:/project/hj/del_table_1.txt"), sb.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
}
