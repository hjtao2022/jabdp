$.jwpf = {
	system : {
		alertinfo : {
			errorTitle : "错误",
			errorInfo : "对不起,出现错误!",
			titlet : "消息框",
			info : "请稍等...",
			titleInfo : "提示信息",
			file : "文件",
			uploadFailed : "上传失败:",
			tryAgain : "请稍后重试！",
			serverException : "服务器出现异常，请稍后重试",
			failedInfo : "上传失败，错误信息如下",
			set : "请选择",
			addType : "--增加类型--",
			confirmTitle:"确认信息",
			operDialog:"操作对话框",
			expTitle:"异常提示：",
			loadDataError:"加载数据出现异常：",
			getDataError:"获取数据出现异常：",
			currPage:"当页：",
			selPage:"选中："
		},
		attach : {
			attachList : "附件列表"
		},
		process : {
			processStartWin : "流程启动窗口",
			startSuccess : "流程启动成功"
		},
		button : {
			expand : "全部展开",
			contract : "全部收缩",
			deleteImage:"删除图片",
			add :"新增",
			edit:"编辑",
			view:"查看",
			save:"保存",
			saveNew:"保存并新增",
			close:"关闭",
			quickAddEdit:"快速新增或修改",
			quickSelect:"快速选择",
			ok:"确定",
			cancel:"取消"
		},
		confirm:{
			selectOnlyOneRow:"请选择且只能选择1条数据！",
			sureDeleteSelRow:"确认要删除选中的数据？",
			selectRow:"请先选择数据！",
			nodata:"暂无数据",
			exportAllData:"您没有选择数据，确认要导出全部数据吗？",
			selectInsertRow:"请选择插入行的位置"
		},
		module : {
			status : {
				"00":"初始化",
				"10":"草稿",
				"20":"审批中",
				"30":"审批结束",
				"31":"审批通过",
				"32":"审批不通过",
				"35":"审核通过",
				"40":"作废"
			},
			statusSub : {
				"10":"启用",
				"40":"禁用"
			},
			statusList:[
				{"id":'00,10',"text":'草稿'},
				{"id":'20',"text":'审批中'},
				{"id":'31,35',"text":'审批通过'},
				{"id":'32',"text":'审批不通过'},
				{"id":'40',"text":'已作废'}          
			],
			reviseLog:"修订日志"
		},
		notice : {
			style:{
				"01":"普通通知",
				"02":"业务提醒",
				"03":"待办事宜",
				"04":"业务确认",
				"05":"提醒通知",
				"06":"业务办理"
			}
		}
	}
};