$.jwpf = {
	system : {
		alertinfo : {
			errorTitle : "Error",
			errorInfo : "Sorroy,Something is wrong!",
			titlet : "Prompt",
			info : "Please Wait...",
			titleInfo : "Title Info",
			file : "File",
			uploadFailed : "Upload Failed:",
			tryAgain : "Please try again later！",
			serverException : "Server Exception，Please try again later！",
			failedInfo : "Upload Failed，Error Info",
			set : "--Please Select--",
			addType : "--Add Type--"
		},
		attach : {
			attachList : "Attach List"
		},
		process : {
			processStartWin : "ProcessStart Window",
			startSuccess : "Start Process Successful!"
		},
		button : {
			expand : "ExpandAll",
			contract : "ContractAll",
			deleteImage:"Delete Picture"
		},
		module : {
			status : {
				"00":"init",
				"10":"draft",
				"20":"approving",
				"30":"finish",
				"31":"approval",
				"32":"disapproval",
				"35":"passed",
				"40":"invalid"
			}
		}
	}
};
