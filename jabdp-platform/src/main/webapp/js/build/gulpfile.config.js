//生成uuid
var genUuid = function(len, radix) {
	  var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
	  var uuid = [], i;
	  radix = radix || chars.length;
	 
	  if (len) {
	   // Compact form
	   for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
	  } else {
	   // rfc4122, version 4 form
	   var r;
	 
	   // rfc4122 requires these characters
	   uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
	   uuid[14] = '4';
	 
	   // Fill in random data. At i==19 set the high bits of clock sequence as
	   // per rfc4122, sec. 4.1.5
	   for (i = 0; i < 36; i++) {
	    if (!uuid[i]) {
	     r = 0 | Math.random()*16;
	     uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
	    }
	   }
	  }
	 
	  return uuid.join('');
};

var SRC_DIR = './';     // 源文件目录  
var DIST_BASE_DIR = './dist/';
var REV_JSP_DIR = '../common/';//修改版本号的jsp目录
var REV_JSP_FILE = 'taglibs.jsp';
var JS_VERSION = genUuid(8,16);
var DIST_DIR = DIST_BASE_DIR + JS_VERSION + '/';   // 文件处理后存放的目录
var DIST_FILES = DIST_DIR + '**'; // 目标路径下的所有文件  

var Config = {
    jsVersion: JS_VERSION,
    jspDir: REV_JSP_DIR,
    jspTplVersion: 'JS_VERSION',
    jspTplPath: REV_JSP_DIR+'taglibs-tpl.jsp',
    jspPath: REV_JSP_DIR + REV_JSP_FILE,
    jspFileName: REV_JSP_FILE,
    src: SRC_DIR,
    baseDist: DIST_BASE_DIR,
    dist: DIST_DIR,
    dist_files: DIST_FILES,
    html: {  
        dir: SRC_DIR,
        src: SRC_DIR + '*.html',  
        dist: DIST_DIR  
    },  
    assets: {  
        dir: SRC_DIR + 'assets',
        src: SRC_DIR + 'assets/**/*',            // assets目录：./src/assets  
        dist: DIST_DIR + 'assets'                // assets文件build后存放的目录：./dist/assets  
    },  
    sass: {  
        dir: SRC_DIR + 'sass',
        src: SRC_DIR + 'sass/**/*.scss',         // SASS目录：./src/sass/  
        dist: DIST_DIR + 'css'                   // SASS文件生成CSS后存放的目录：./dist/css  
    },
    css: {  
        dir: SRC_DIR + 'css',
        src: ["./easyui-1.4/themes/icon.css"
              ,"./loadmask/jquery.loadmask.css","./common/scripts/focus_tips.css"
              ,"./ztree/blue/zTreeStyle.css", "./ztree/blue/icon.css", "./easyui/icons/icon-ztree.css", "../themes/default/blue/css/main.css"
              ,"./imgpreview/blue/jquery.gzoom.css", "./qtip2/jquery.qtip.min.css"
              ,"./webuploader/webuploader.css","./toastr/toastr.min.css", "./nprogress/nprogress.css"],           // CSS目录：./src/css/
        dist: DIST_DIR,                   // CSS文件build后存放的目录：./dist/css 
        build_name: 'app.css'
    },  
    cssPortal: {  
        dir: SRC_DIR + 'css',
        src: ["./slides-JS/jquery.slides.css","./jquery.marquee/css/jquery.marquee.css"
              ,"./news/css/jquery.galleryview-3.0-dev.css", "./easyui/icons/portal.css"],           // CSS目录：./src/css/
        dist: DIST_DIR,                   // CSS文件build后存放的目录：./dist/css 
        build_name: 'app-portal.css'
    },
    cssEasyuiBlue: {  
        dir: SRC_DIR + 'css',
        src: ["./easyui-1.4/themes/default/easyui.css","./easyui/blue/easyui-add.css"],          // CSS目录：./src/css/
        dist: DIST_BASE_DIR,                   // CSS文件build后存放的目录：./dist/css 
        build_name: 'easyui-blue.css'
    },
    cssEasyuiGray: {  
        dir: SRC_DIR + 'css',
        src: ["./easyui-1.4/themes/gray/easyui.css","./easyui/gray/easyui-add.css"],           // CSS目录：./src/css/
        dist: DIST_BASE_DIR,                   // CSS文件build后存放的目录：./dist/css 
        build_name: 'easyui-gray.css'
    },
    cssEasyuiBootstrap: {  
        dir: SRC_DIR + 'css',
        src: ["./easyui-1.4/themes/bootstrap/easyui.css","./easyui/gray/easyui-add.css"],           // CSS目录：./src/css/
        dist: DIST_BASE_DIR,                   // CSS文件build后存放的目录：./dist/css 
        build_name: 'easyui-bootstrap.css'
    },
    jsV1: {  
        dir: SRC_DIR + 'js',
        src: ["./easyui/scripts/jquery.easyui.min.js","./easyui/scripts/easyui.extend.js"
              ,"./easyui/scripts/datagrid-detailview.js", "./easyui/scripts/jquery.edatagrid.js"],             // JS目录：./src/js/  
        dist: DIST_DIR,                   // JS文件build后存放的目录：./dist/js  
        build_name: 'app-v1.js'                   // 合并后的js的文件名  
    },
    jsV2: {  
        dir: SRC_DIR + 'js',
        src: ["./easyui-1.4/jquery.easyui.min.js","./easyui-1.4/easyui.extend.js"
              ,"./easyui-1.4/datagrid-detailview.js", "./easyui-1.4/jquery.edatagrid.js"],             // JS目录：./src/js/  
        dist: DIST_DIR,                   // JS文件build后存放的目录：./dist/js  
        build_name: 'app-v2.js'                   // 合并后的js的文件名  
    },
    jsPortal: {  
        dir: SRC_DIR + 'js',
        src: ["./slides-JS/jquery.slides.min.js","./jquery.marquee/lib/jquery.marquee.js"
              ,"./news/js/jquery.timers-1.2.js", "./news/js/jquery.easing.1.3.js"
              ,"./news/js/jquery.galleryview-3.0-dev.js", "./easyui/scripts/jquery.portal.js"
              ,"./jquery.marquee/lib/MSClass.js"],             // JS目录：./src/js/  
        dist: DIST_DIR,                   // JS文件build后存放的目录：./dist/js  
        build_name: 'app-portal.js'                   // 合并后的js的文件名  
    },
    jsIndex: {  
        dir: SRC_DIR + 'js',
        src: ["./metismenu/metisMenu.min.js","./index/jquery.nicescroll.min.js"
              ,"./index/contabs.min.js", "./index/index.min.js"], // JS目录：./src/js/  
        dist: DIST_DIR,                   // JS文件build后存放的目录：./dist/js  
        build_name: 'app-index.js'                   // 合并后的js的文件名  
    },
    js: {  
        dir: SRC_DIR + 'js',
        src: [
              "./form/scripts/jquery.form.js","./form/scripts/json2form.js","./loadmask/jquery.loadmask.min.js","./common/scripts/common.js"
              ,"./common/scripts/json2.js","./common/scripts/jwpf.js", "./common/scripts/focus_tips.js", "./ztree/scripts/jquery.ztree.all.min.js"
              ,"./common/scripts/theme.js", "./common/scripts/gscommon.js", "./store/store.everything.min.js"
              ,"./hotkey/jquery.hotkeys.js", "./imgpreview/scripts/jquery.gzoom.js", "./imgpreview/scripts/ui.core.min.js"
              ,"./imgpreview/scripts/ui.slider.min.js", "./common/scripts/xdate.js", "./common/scripts/accounting.js"
              ,"./qtip2/jquery.qtip.min.js", "./qtip2/imagesloaded.pkgd.min.js", "./clipboard/clipboard.min.js"
              ,"./webuploader/webuploader.min.js", "./webuploader/es.filebox.js", "./toastr/toastr.min.js"
              ,"./common/scripts/Native_FullScreen.js", "./nprogress/nprogress.js", "./common/scripts/sprintf.min.js"
              ,"./index/pace.min.js"],             // JS目录：./src/js/  
        dist: DIST_DIR,                   // JS文件build后存放的目录：./dist/js  
        build_name: 'app.js'                   // 合并后的js的文件名  
    },
    img: {  
        dir: SRC_DIR + 'images',
        src: SRC_DIR + 'images/**/*',            // images目录：./src/images/  
        dist: DIST_DIR + 'images'                // images文件build后存放的目录：./dist/images  
    },
    copy: {
    	src:["./zeroclipboard/ZeroClipboard.swf","./webuploader/Uploader.swf"],
    	dist: DIST_DIR
    }
};

module.exports = Config;