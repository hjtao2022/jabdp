@echo off

cd /d %~dp0
if '%1=='## goto ENVSET

rem 库文件所在的目录，相对于当前路径
set "CURRENT_DIR=%cd%"
SET LIBDIR=lib
SET PATH=%CURRENT_DIR%/../../../jre/bin;%PATH%

rem 要启动的类名
SET CLSNAME=com.qyxx.platform.common.utils.DbUtils

rem 设定CLSPATH
SET CLSPATH=.
FOR %%c IN (%LIBDIR%\*.jar) DO CALL %0 ## %%c
SET CLSPATH=%CLSPATH%;

rem 运行
GOTO RUN

:RUN
rem echo 路径：%CLSPATH%
echo 启动数据库初始化程序---开始
java -cp %CLSPATH% %CLSNAME% %CURRENT_DIR%
echo 启动数据库初始化程序---结束
pause
goto END

:ENVSET
set CLSPATH=%CLSPATH%;%2
goto END

:END