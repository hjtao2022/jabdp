<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%
	SystemParam systemParam = SpringContextHolder.getBean("systemParam");
%>
<!DOCTYPE html>
<html> 
　<head> 
		<meta charset="utf-8" />
　	<meta name="viewport" content="width=device-width, initial-scale=1"> 	
		<title><%=systemParam.getSystemTitle()%></title>
		<link rel="stylesheet" href="${ctx}/js/jquery.mobile/jquery.mobile-1.3.2.min.css" />
		<%@ include file="/common/meta-min.jsp" %>
		<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
		<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>
	　<script type="text/javascript" src="${ctx}/js/jquery.mobile/jquery.mobile-1.3.2.min.js"></script>
<style type="text/css">
img.main-image{margin-top:-15px;width:100%;display:block}
</style>
	<script type="text/javascript">
//检查用户是否已经登入
		function doCheckUserIsLogin(userName, userPass) {
			var flag = false;
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!checkUserIsLogin.action',
					data : {
						"userName" : userName,
						"userPass" : userPass
					},
					success : function(data) {
						var fm = data.msg;
						if(fm) {
							switch(fm.flag) {
								case "0":
									$("#popMsg").data("userName",userName);
									var cMsg = "<div>" + fm.msg + "。是否确认将其强制下线？</div>";
									$("#popMsg").html(cMsg);
									$.mobile.changePage("#popdiv",{ transition: "pop" });
									
									//$('#popdiv').dialog('open');
									break;
								case "2":
									$(".user_login_msg").text(fm.msg);
									break;
								default:
									flag = true;
							}
						}
					}
			};
			fnFormAjaxWithJson(options);
			return flag;
		}
//移除其他IP的在线用户
		function doRemoveOnlineUser(userName) {
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!removeLoginUser.action',
					data : {
						"userName" : userName
					},
					success : function(data) {
						$("#loginForm").unbind("submit");
						$("#loginForm").submit();
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		function isLogin(r){			
				if (r=="1"){
					var userName = $("#popMsg").data("userName");
					doRemoveOnlineUser(userName);
				} else{
					$("#j_username").val("");
					$("#j_password").val("");
					$("#j_username").focus();
				}
		}
		
		$(document).ready(function() {
			<%
				AuthenticationException authException = (AuthenticationException)session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				if(authException!=null && authException instanceof SessionAuthenticationException) {
					String errorMsg = authException.getMessage();
			%>
					$(".user_login_msg").text("<%=errorMsg%>");
			<%
				}
			%>
			
			$("#loginForm").submit(function() {
				var userName = $("#j_username").val();
				if(!userName) {
					$(".user_login_msg").text("请输入用户名");
					$("#j_username").focus();
					return false;
				}
				var pass = $("#j_password").val();
				if(!pass) {
					$(".user_login_msg").text("请输入密码");
					$("#j_password").focus();
					return false;
				}
				return doCheckUserIsLogin(userName, pass);
			});
		});
	</script>
</head>
<%
	String username = "";
    Cookie[] cs = request.getCookies();
	if(null!=cs) {
		for(Cookie ck : cs) {
			if(Constants.USER_NAME_COOKIE.equals(ck.getName())) {
				username = java.net.URLDecoder.decode(ck.getValue(), Constants.DEFAULT_ENCODE);
				break;
			}
		}
	}
%>

<body>
<div data-role="page" data-theme="b" id="p1">
	<div data-role="header" style="background:#6ccae4">
	   <img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:40px;vertical-align:top;"/>
	   <span style="position: relative;top: 10px;left: 10px;"><%=systemParam.getSystemTitle()%></span>
    </div>
    <div data-role="content">
        <form id="loginForm" action="${ctx}/j_spring_security_check" method="post" data-ajax="false">
        <input type="hidden" name="spring-security-redirect" value="/index-mobile.action"></input>
        <div data-role="fieldcontain">
            <label for="textinput2">用户名:</label>
            <input type="text" name="j_username" placeholder="请输入用户名" id="j_username" value="" />
        </div>
        <div data-role="fieldcontain">
            <label for="textinput3"> 密码:</label>
            <input type="password" name="j_password" placeholder="请输入密码" value=""   id="j_password"/>
        </div>
        
        <!-- 	<input type="submit"  name="bt_login" value="登入">
        	<a href="index.html" data-role="button">取消</a> -->
        	<div class="ui-grid-a">
    					<div class="ui-block-a">
    						<input type="submit"  name="bt_login" value="登入">
    					</div>
    					<div class="ui-block-b">
    						<input type="reset" value="取&nbsp;&nbsp;消"/>
    					</div>
    			</div>
        	
        	 <div class="user_login_msg">
					<s:if test="#parameters.error!=null">
						<span><s:text name="system.login.error.userpass.title"/></span>&nbsp;&nbsp;
					</s:if>
					<s:if test="#parameters.timeout!=null">
						<span><s:text name="system.login.error.timeout.title"/></span>&nbsp;&nbsp;
					</s:if>
			</div>
        </form>
<!-- <a href="#popdiv" data-role="button" data-rel="dialog" data-transition="fade" data-inline="true">dialog</a> -->
    </div>
</div>

  <div data-role="page" data-theme="b" id="popdiv">
    <div  data-role="header">     
    	  <h3> 该用户已经登录</h3>
    </div>
 
    <div data-role="content"> 
    <div id="popMsg"></div>
      <a data-role="button" data-inline="true" href='javascript:void(0);' onclick="isLogin(1);" >确定</a>
		 <a data-role="button" data-inline="true" href='javascript:void(0);' onclick="$('#popdiv').dialog('close');">取消</a>
    </div>
 
  </div>
</body>
</html>