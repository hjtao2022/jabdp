<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
SystemParam systemParam = SpringContextHolder.getBean("systemParam");
String portalUrl = "";
String systemPortalUrl = systemParam.getSystemPortalUrl();
if(StringUtils.isNotBlank(systemPortalUrl)) {
	portalUrl = systemPortalUrl;
}
User user = (User)session.getAttribute("USER");
if(null!=user) {
	String rolePortalUrl = user.getPortalUrl();
	if(StringUtils.isNotBlank(rolePortalUrl)) {
		portalUrl = rolePortalUrl;
	}
} else {
	response.sendRedirect(request.getContextPath() + "/login-mobile.action");
}
%>
<!DOCTYPE html>
<html> 
<head> 
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1"> 	
<title><%=systemParam.getSystemTitle()%></title>
<link rel="stylesheet" href="${ctx}/js/jquery.mobile/jquery.mobile-1.3.2.min.css" />
<link rel="stylesheet" href="${ctx}/js/jquery.mobile/gabarit.css" />
<%@ include file="/common/meta-min.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/bootstrap/easyui.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/icon.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.3.3/themes/portal.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui/scripts/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/common.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/json2.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/gscommon.js"></script>
<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.mobile/jquery.mobile-1.3.2.min.js"></script>
<style type="text/css">
ul,li {padding:0; margin:0; list-style:none; padding-top:10px;}
ul li {padding-top:25px;}
ul li span.imgIcon {
	border : 0 ;
	height:	60px;
	width: 95px;
	text-align:center; 
	background-position: center;
}
ul li div.imgContainer{
	text-align:center; 
	margin:0 auto;
}
ul li a {
	color:#000;
}
#imglist li { float:left; text-align:center; line-height:30px; margin:0 0 0 10px; width:95px; white-space:nowrap; overflow:hidden; display:inline; }
#imglist li span { display:block; text-align:center; }
</style>
</head>
<body>
<div data-role="page" id="page_1">
 <div data-role="header"  data-theme="a" data-position="inline" data-backbtn="false">
  <a href='javascript:void(0);' id="bt_back" data-icon="back">返回</a>	
  <h1><%=systemParam.getSystemTitle()%></h1>
  <a href='javascript:void(0);' onclick="doLogout();" data-icon="back">退出</a>
  <div data-role="navbar" data-iconpos="top" id="d_navbar">
   <ul>
    <li>
     <a href="javascript:void(0);" onclick="initMenus();" data-transition="none" data-theme="b" data-icon="search">
          系统功能       
     </a>
    </li>
    <li>
     <a href="javascript:void(0);" onclick="getNotices();" data-transition="none" data-theme="b" data-icon="star">
          消息通知
     </a>
    </li>
    <li>
     <a href="javascript:void(0);" onclick="getToDoLists();" data-transition="fade" data-theme="b" data-icon="info">
          待办事宜
     </a>
    </li>
    <li>
     <a href="javascript:void(0);" onclick="doView();" data-transition="fade" data-theme="b" data-icon="home">
          个人信息
     </a>
    </li>
   </ul>
  </div>
 </div>  
 <div data-role="content" id="menuTabs" style="width:100%;height:100%;"></div>
<!--     <div data-role="footer" class="footer">
    <ul><li><a href="/Redirect.aspx" rel="external">JiaWa Soft</a></li></ul>
  <small>
        <div id="ucFooter_uxCopyright">
	©2013 RBFF All Rights Reserved
</div>
    </small>
</div> -->      
</div>
</body>
<script type="text/javascript">
$(function() {
	initMenus();
	$("#bt_back").click(function() {
		var active_a = $("#d_navbar").find("a.ui-btn-active");
		$(active_a[0]).click();
	});
});
function initMenus() {
	var options = {
			url:'${ctx}/index!view.action',
			success:function(data) {
				if(data.msg) {			
					initPanelsMenu(data.msg);
				}
			}
	};
	fnFormAjaxWithJson(options,true);			
}
//初始化菜单模块
function initPanelsMenu(menus) {
	var content = [];
	for(var i=0;i<menus.length;i++) {
		var menu = menus[i];
		if(menu.childs) {
			for(var j=0;j<menu.childs.length;j++) {
				var remainder = null;
				if(j!=0){
					remainder = j%4;
				}
				var cmenu = menu.childs[j];
				var icon = cmenu.iconSkin;
				var name = cmenu.name;
				var url = cmenu.resourceUrl;
				if(!icon) {
					icon = "icon-default-big";
				}else{
					icon = icon +"-big";
				}
				var ulli = "<li><a href='javascript:void(0);' url='${ctx}"+url+"' ><div class='imgContainer'><span class='imgIcon "+icon+"'></span></div><span class ='fontName'>"+name+"</span></a></li>";
				content.push(ulli);
			}
		}
	}
	var src = content.join("");
	var tab_ct ="<div data-role='navbar' data-iconpos='top' id='imglist'><ul type='none'>"+src+"</ul></div>";		
	$('#menuTabs').html(tab_ct);
	$("#imglist li a").click(function() {
		var url = $(this).attr("url");
		//var aa = '<iframe scrolling="auto" name="roleFrame" id="roleFrame frameborder="0" src="${ctx}/sys/account/role!init.action?orgId=1" style="width: 100%; height: 100%;"></iframe>';
		var fm = '<div><iframe  src="'+url+'" frameborder="0" style="width:100%;height:100%;overflow:hidden;"></iframe></div>';
		$('#menuTabs').html(fm);
		//$("#page_1").page();
		//window.location.replace($(this).attr("url"));
	});
}
//退出
function doLogout() {
	window.location.replace("${ctx}/logout-mobile.action");
}
function doNotices() {
	window.location.replace("${ctx}/sys/mobile/notices-mobile.action");
}
//获取通知消息
function getNotices(){
	var options = {
		url : '${ctx}/sys/notice/notice!queryNoReadNoticeList.action',
		success:function(data) {
		if(data.msg) {
			var notices = data.msg;
		 	var content = [];
			for(var j=0;j<notices.length;j++) {
				var title = notices[j].title;
				var createTime =notices[j].createTime.substring(0, 10);
				var id = notices[j].id;
				ntuId = notices[j].ntuId;
				var style = notices[j].style;
				var ulli = "<li><a href='javascript:void(0);'  onclick='doReadNotices("+id+","+ntuId+","+style+");'>标题："+title+"<div class='create-text' style='float:right;'>"+createTime+"</div></a></li>";
				content.push(ulli);
			}
			var src = content.join("");
			var ul = "<ul id ='notices' data-role='listview' style='margin-top:25px;' data-theme='d' data-inset='true'>"+src+"</ul>";
			$('#menuTabs').html(ul);
			//$("#page_1").page();
			$("#notices").listview();
			}
		}
	};
	fnFormAjaxWithJson(options,true);
}
//获取待办事宜
function getToDoLists(){
	var options = {
		url : '${ctx}/gs/process!toDoListMobile.action',
		success:function(data) {
		if(data.msg){
			var notices = data.msg;
		  	var content = [];
			for(var j=0;j<notices.length;j++) {
				var title ="任务名："+notices[j].name;		
				var createTime =notices[j].createTime.substring(0, 10);
				var ulli = "<li><a href='javascript:void(0);' onclick='doSignin("+notices[j].id+");'>"+title+"<div class='create-text' style='float:right;'>"+createTime+"</div></li>";
				
				content.push(ulli);
			}
			var src = content.join("");
			var ul = "<ul id ='todo' data-role='listview' style='margin-top:25px;' data-theme='d' data-inset='true'>"+src+"</ul>";
			$('#menuTabs').html(ul);
			$("#todo").listview(); 		 		
			}
		}
	};
	fnFormAjaxWithJson(options,true);
}
function doReadNotices(id,ntuId,style){
	doModify(id);
	//doRead(ntuId);
	//window.location.replace("${ctx}/sys/notice/notice!viewNoticeToUserMobile.action?id="+ id+"&nTouId="+ntuId+"&style="+style);
}
function doSignin(id){
	//window.location.replace("${ctx}/gs/process!getProcess.action?taskId="+id);
	var url = "${ctx}/gs/process!getProcessMobile.action?taskId=" + id;
	var t = "<div><iframe src='" + url + "' style='width:100%;height:100%;overflow:hidden;' frameborder='0'></iframe></div>";
	$('#menuTabs').html(t);
}	
function doInitForm(data) {
	if(data.msg) {
		var jsonData = data.msg;
		var title = jsonData.title;
		var content = jsonData.content;
		var t = "<div style='text-align:center'><h3>"+title+"</h3></div><div style='text-align:center'>"
		+ content + "</div><div style='text-align:center;padding-top:20px'><a class='l-btn' id='mm' title='已阅' href='javascript:void(0)'"
		+ "onclick='doRead("+ntuId+");'>"
		+ '<span class="l-btn-left"><span class="l-btn-text icon-ok" style="padding-left: 20px;"></span><span style="position: relative;top:-2px;">已阅</span></span>'
		+ "</div>";
		$('#menuTabs').html("");
		$('#menuTabs').html(t); 
	}
}
//页面数据初始化
function doModify(id) {
	var options = {
		url : '${ctx}/sys/notice/notice!updateNoticeState.action',
		data : {
			"id" : id
		},
		success : function(data) {
			doInitForm(data);
		}
	};
	fnFormAjaxWithJson(options);
}
function doRead(nTouId) {
	var options = {
		url : '${ctx}/sys/notice/notice!doRead.action',
		data : {
			"noticeToUserId" : nTouId
		},
		success : function(data) {
			getNotices();
		}
	};
	fnFormAjaxWithJson(options); 
}
function doView() {
	var content = [];
	content.push("<li><h3>登入名:<s:property value='#session.USER.realName'/>");
	content.push("</h3></li><li><h3>用户名:<s:property value='#session.USER.nickname' />");
	content.push("</h3></li><li><h3>手机号:<s:property value='#session.USER.mobilePhone' />");
	content.push("</h3></li><li><h3>邮箱:<s:property value='#session.USER.email'/></h3></li>");
	var src = content.join("");
	var ul = "<ul class='userInfo' id ='userInfo' style='margin-top:25px;' data-role='listview' data-theme='d' data-inset='true'>"
		+ src
		+ "</ul>"
		+ "<ul class='userInfo' style='margin-top:25px;' data-role='listview' data-theme='d' data-inset='true'>"
		+ "<li><h3>"
		+ "<span style='padding-right:30px'>签到:</span><a href='javascript:void(0);' id='qiandao'></a>"
		+ "<span style='padding:0px 30px'>查看：</span><a href='javascript:void(0);' id='chakan'></a>"
		+ "</h3></li>"
		+ "</ul>";
	$('#menuTabs').html(ul);
	$(".userInfo").listview(); 
	$(".userInfo").listview('refresh');
	$("#qiandao").linkbutton({
		"iconCls" : "icon-checkins"
	}).click(function(e) {
		window.location.href = "${ctx}/sys/checkins/checkins-map.action";
	});
	$("#chakan").linkbutton({
		"iconCls" : "icon-search"
	}).click(function(e) {
		$.ajax({
			url : "${ctx}/sys/checkins/checkins!queryMobile.action",
			success : doCheckinsView
		});
	});
	$(".l-btn-empty").css({"position":"relative","top":"-3px"});
}
function doCheckinsView(data) {
	var content = [];
	if(data && data.length) {
		for(var i in data) {
			var ulli = "<li><a href='javascript:void(0);' onclick='doCaKan(\""+data[i].palce+"\");'>标题："
				+ data[i].title 
				+ "<div class='create-text' style='float:right;'>"
				+ data[i].checkinsTime
				+ "</div></a></li>";
			content.push(ulli);
			/* content = content + "<tr num_index="+data[i].id+">"
			+ "<td style='border: 1px solid black;'>" + data[i].title + "</td>"
			+ "<td style='border: 1px solid black;'>" + data[i].content + "</td>"
			+ "<td style='border: 1px solid black;'>"
			+ "<a href='javascript:void(0);' onclick='doCaKan(\""+data[i].palce+"\");'>查看</a>"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;"
			+ "<a href='javascript:void(0);' onclick='doDelete(\""+data[i].id+"\");'>删除</a>"
			+ "</td>"
			+ "</tr>"; */
		}
	}
	var src = content.join("");
	var ul = "<ul id ='checkins' data-role='listview' style='margin-top:25px;' data-theme='d' data-inset='true'>"+src+"</ul>";
	$('#menuTabs').html(ul);
	$("#checkins").listview();
	/* var htmlstr = "<table id='tt_chakan' style='border: 1px solid black;text-align:center;'>"
		+ "<tr>"
		+ "<th style='border:1px solid black;'>主题</th><th style='border:1px solid black;'>内容</th><th style='border:1px solid black;'>操作</th>"
		+ "</tr>"
		+ content
		+ "</table>";
	$('#menuTabs').html(htmlstr); */
}
function doCaKan(e) {
	alert("签到地点："+e);
}
function doDelete(e) {
	if(confirm("你确信要删除？")) {
		$.ajax({
			url : "${ctx}/sys/checkins/checkins!delete.action",
			data : {"id" : e},
			success : function() {
				alert("删除成功");
				$("tr[num_index="+e+"]").remove();
			}
		});
	}
}

function addTab(title, url) {
	var win = $("iframe")[0].contentWindow.window;
	win.location.replace(url);
}
function closeTab(title) {
	initMenus();
}
function closeCurrentTab() {
	initMenus();
}
</script>
</html>
