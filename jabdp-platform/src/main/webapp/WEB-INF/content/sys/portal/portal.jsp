<%@page import="java.util.Random"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Jabdp Portal</title>
<%@ include file="/common/meta.jsp" %>
<link rel="stylesheet" type="text/css" href="${ctx}/js/dist/${jsVersion}/app-portal.min.css"/>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app-portal.min.js"></script>
	<style type="text/css">
		.title{
			font-size:16px;
			font-weight:bold;
			padding:20px 10px;
			background:#eee;
			overflow:hidden;
			border-bottom:1px solid #ccc;
		}
		.t-list{
			padding:5px;
		}
		a {text-decoration: none;}
　　　　 a:link { text-decoration: none;color: blue}
　　　　 a:active { text-decoration:blink}
　　　　 a:hover { text-decoration:underline;color: red} 
　　　　 a:visited { text-decoration: none;color: green}

		ul li {
			padding:3px 3px;
		}
	</style>
</head>
<body class="easyui-layout" >
	<div region="center" border="false">
		<div id="tt"  fit="true" border="false">
		 <c:forEach items="${list}" var="item">
			    <div title="${item.title}" closed="false" tools="#p-tools_${item.desktop.id}"
			      href="<c:url value="/sys/portal/panel.action"> <c:param name="id" value="${item.id}"/> <c:param name="desktopId" value="${item.desktop.id}"/> <c:param name="layoutType" value="${item.layoutType}"/><c:param name="layoutVal" value="${item.layoutVal}"/></c:url>"></div>
		 </c:forEach> 
		 <div title="快捷菜单"><iframe src="${ctx}/sys/menu/menu-icon.action" style="width: 100%;height: 100%" frameborder="0" ></iframe></div>
		</div> 
		<div id="tab-tools">
			<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" onclick="addTabs()"></a>
		</div>
	</div>
	
	<!-- 添加首页内容 修改首页布局按钮 -->
	<div id="ptools">
		<c:forEach items="${list}" var="item">
			 <div id="p-tools_${item.desktop.id}">
				<a href="#" class="icon-mini-add" onclick="addPortal(${item.desktop.id})"></a>
				<a href="#" class="icon-mini-edit" onclick="modifyPortal(${item.desktop.id})"></a>
				<a href="#" class="icon-mini-refresh" onclick="refreshPor(${item.desktop.id})"></a>
			</div>
		</c:forEach> 
	</div>
	<!--   <div id="tab-tools">
		<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-add" onclick="doAddTab();"></a>
		<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-reload" onclick="doRefreshTab();"></a>
	</div> -->
	  	<!-- 添加首页Tabs弹出框   -->
	  <div id="aboutTabs" class="easyui-window" closed="true" minimizable="false" modal="true" title="添加Tab页" style="width:220px;height:400px;">
				<div style="padding:10px 10px;" class="easyui-layout" fit="true">
						<div region="center" border="false"> 
						 <div> <ul class="easyui-tree" id="tree_tab"></ul></div>
						</div>
						<div region="south" border="false"
						 style="text-align: right; ">
							 <a  class="easyui-linkbutton" 
								href="javascript:void(0);" onclick="doAddTab();"><s:text
								name="system.button.ok.title" /></a> 
							 <a class="easyui-linkbutton"
								href="javascript:void(0);"
								onclick="javascript:$('#aboutTabs').window('close');"><s:text
								name="system.button.cancel.title" /></a>
					  	</div>
				</div>
	  </div>
	<!-- 添加首页内容弹出框   -->
	  <div id="aboutPortal" class="easyui-window" closed="true" minimizable="false" modal="true" title="添加首页内容" style="width:220px;height:400px;">
				<div style="padding:10px 10px;" class="easyui-layout" fit="true">
						<div region="center" border="false"> 
						 <div> <ul class="easyui-tree" id="tree_portal"></ul></div>
						</div>
						<div region="south" border="false"
						 style="text-align: right; ">
							 <a  class="easyui-linkbutton" 
								href="javascript:void(0);" onclick="doAdd();"><s:text
								name="system.button.ok.title" /></a> 
							 <a class="easyui-linkbutton"
								href="javascript:void(0);"
								onclick="doCanel();"><s:text
								name="system.button.cancel.title" /></a>
					  	</div>
				</div>
	  </div>
	<!--   修改首页布局弹出框 -->
	   <div id="panelmodifycol" class="easyui-window" closed="true" minimizable="false" modal="true" title="<s:text name="system.sysmng.desktop.modifyLayout.title"/>" style="width:200px;height:300px;">
				<div style="text-align:center;padding:10px 10px;" class="easyui-layout" fit="true">
						<div region="center" border="false">
							<div id="panelmodifycol">
								<div style="display:inline;padding:10px;">
								  <fieldset>
								  <legend> <label align="left"> <s:text name="system.sysmng.desktop.modifyLayout.title"/></label> </legend>
								   <div style="width:40px;display:inline"></div>
								  <a href="javascript:void(0)" onclick="setLayout(1);"> <img id="colsOne" alt="修改成一列布局" src="${ctx}/js/portal/images/oneCols.jpg" style="padding-top: 20px" /></a>
								   <a href="javascript:void(0)" onclick="setLayout(2);"> <img id="colsTwo" alt="修改成两列布局" src="${ctx}/js/portal/images/twoCols.jpg" style="padding-top: 20px" /></a>
								    <a href="javascript:void(0)" onclick="setLayout(3);"><img id="colsThree" alt="修改成三列布局" src="${ctx}/js/portal/images/threeCols.jpg" style="padding-top: 20px" /></a>
								 </fieldset>
								</div>
							     <fieldset>
							      
							           <legend><label align="left"> <s:text name="system.sysmng.desktop.modifyWidth.title"/></label></legend>
							           <div id="layoutdisplay1" class="cols_div" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.leftWidth.title"/> <input type="text"  class="easyui-numberbox"  name="layout1" style="width:30px"  id="layout1" >% 
										</div>
										<div id="layoutdisplay2" class="cols_div" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.centreWidth.title"/> <input type="text"  class="easyui-numberbox" name="layout2" style="width:30px"  id="layout2" >%
										</div>
										<div id="layoutdisplay3" class="cols_div" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.rightWidth.title"/>  <input type="text"  class="easyui-numberbox"  name="layout3" style="width:30px"  id="layout3">%
										</div>
										 <a id="mm" class="easyui-linkbutton" 
											href="javascript:void(0);" onclick="changeColWidth();"><s:text name="system.button.ok.title" /></a> 
							     </fieldset>
								
						</div>
					  </div>
               </div>
       </div>
       
        <div id="aboutPorperties" class="easyui-window" closed="true" minimizable="false" modal="true" title="<s:text name="system.sysmng.desktop.modifyModelInfo.title"/>" style="width:250px;height:280px;">
				<div style="text-align:center;padding:10px 10px;" class="easyui-layout" fit="true">
						<div region="center" border="false">
							<form action="" method="post" name="viewForm" id="viewForm" style="margin: 0px;">
							    <div id="pTitle" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.modelTitle.title"/> <input type="text"  name="title" style="width:100px" class="block_input" id="title" />
								</div>
								<div id="pDisplayNum" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.displayNum.title"/><input type="text"  class="easyui-numberbox"  name="displayNum" style="width:100px"  id="displayNum" />
								</div>
								<div id="pFontNum" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.modelWord.title"/> <input type="text"  class="easyui-numberbox"   name="fontNum" style="width:100px"  id="fontNum" />
								</div>
								<div id="pHeight" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.modelHeight.title"/> <input type="text"  class="easyui-numberbox"   name="height" style="width:100px" id="height" />
								</div>
								<div id="showCreate" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.createUser.title"/> <input type="checkbox"  name="createUser" style="width:100px"   id="createUser" />
								</div>
								<div id="showCtreateTime" style="width:90%;padding:5px;color:#333333;display:"><s:text name="system.sysmng.desktop.createTime.title"/> <input type="checkbox"  name="createTime"  style="width:100px"  id="createTime" />
								</div>
							</form>
						</div>
						<div region="south" border="false"
						 style="text-align: right; ">
							 <a id="mm" class="easyui-linkbutton" 
								href="javascript:void(0);" onclick="doUpdate();"><s:text
								name="system.button.ok.title" /></a> 
							 <a class="easyui-linkbutton" 
								href="javascript:void(0);"onclick="cancelUpdate();"><s:text
								name="system.button.cancel.title" /></a>
					  	</div>
				</div>
	  </div>
       
<!--控制页面的函数  -->
	<script>
	
	var _userList={};
	
	function doSignin(id,name){
		window.parent.addTab(name,'${ctx}/gs/process!getProcess.action?taskId='+id);
	}
	 	 
          
      	//查询tab下所有的panel
      	function modifyPanels(id,parentId,layout) {
      		var options = {
      				url:'${ctx}/sys/desktop/desktop-to-user!queryLevel2DesktopToUser.action',
      				 data : {
      	                 "parentId" :parentId
      	             },
      				success:function(data) {
      					if(data.msg) {
      						var panels=panelData(data.msg);
      						initPanels(id,layout,panels);
      						
      					}
      				}
      		};
      		fnFormAjaxWithJson(options,true);			
      	}
	
	//定义构建panel数组
	function panelData(data){
		 var panels=[];
         var url="";
		for(var i=0;i<data.length;i++){
			var closeId=data[i].id;
			var id = data[i].desktop.id;
			var pid="p"+id;
			var type=data[i].desktop.type;
			switch(type){
			case "toDoList":url="${ctx}/sys/portal/task-list.action?id=";break;
			case "textList": url="${ctx}/sys/portal/textList.action?id=";break;
			case "picTextList":url="${ctx}/sys/portal/picTextList.action?id=";break;
			case "picList":url="${ctx}/sys/portal/picture-slides.action?id=";break;
			case "datagrid": url="${ctx}/gs/process.action?entityName=contracts.MainTable";break;
			case "statistic2": url="${ctx}/sys/portal/statistic2.action?id=";break;
			case "statistic1": url="${ctx}/sys/portal/statistic1.action?id=";break;
			case "weather": url="${ctx}/sys/portal/weather.action?id=";break;
			case "datetime": url="${ctx}/sys/portal/dateTime.action?id=";break; 
			case "iframe":url="${ctx}/sys/portal/ifr.action?id=";break;
			case "textSlides":url="${ctx}/sys/portal/text-slides.action?id=";break;
			}
			var temp=$.extend({"title":data[i].title,"height":data[i].height,collapsible:true,onMaximize:maxPanel,maximizable:true,closable:true,onClose:doClose,href:url+id},{"id":pid,"closeId":closeId});
			panels.push(temp);
		}
		return panels;
		
	}
	

	function doClose(){
		var id=$(this).data("closeId");
		var options = {
  				url:'${ctx}/sys/desktop/desktop-to-user!delete.action',
  				 data : {
  	                 "id" :id
  	             },
  				success:function(data) {
  					
  				},
	      	   traditional:true
  		};
  		fnFormAjaxWithJson(options,true);		
	}
	
	function getCookie(name){
		var cookies = document.cookie.split(';');
		if (!cookies.length) return '';
		for(var i=0; i<cookies.length; i++){
			var pair = cookies[i].split('=');
			if ($.trim(pair[0]) == name){
				return $.trim(pair[1]);
			}
		}
		return '';
	}
	//得到每个面板的id
	function getPanelOptions(id,panels){
		for(var i=0; i<panels.length; i++){
			if (panels[i].id == id){
				return panels[i];
			}
		}
		return undefined;
	}
	//得到每个面板id的状态
	function getPortalState(id){
		var aa = [];
		for(var columnIndex=0; columnIndex<3; columnIndex++){
			var cc = [];
			var panels = $('#pp'+id).portal('getPanels', columnIndex);
			for(var i=0; i<panels.length; i++){
				cc.push(panels[i].attr('id'));
			}
			aa.push(cc.join(','));
		}
		return aa.join(':');
	}
	//把面板重新添加到body中
	function addPanels(portalState,id,panels){
		
		var columns = portalState.split(':');
		for(var columnIndex=0; columnIndex<columns.length; columnIndex++){
			var cc = columns[columnIndex].split(',');
			for(var j=0; j<cc.length; j++){
				var options = getPanelOptions(cc[j],panels);
				if (options){
			 		var p = $('<div/>').attr('id',options.id).appendTo('body');
			 		var closeId = options.closeId;
					p.data("closeId",closeId);
					p.data("pid",id);
					var tl = {  
						    iconCls:'icon-edit',  
						    handler:updatePorperties
						   
						  };
					var t2 = {  
							 iconCls:'icon-reload',  
							 handler:updatePanle						   
						  };					
					//自定义工具条
					$.extend(options, {
						 tools: [tl,t2]
				    });
					p.panel(options);
					$('#pp'+id).portal('add',{
						panel:p,
						columnIndex:columnIndex
					});
				}
			}
		}
		
	}
	
	//修改元素信息
	 function updatePorperties(){
		 $("#aboutPorperties").window("open");
		 var pa = $(this).parent().parent().next();
		 var ps = pa.attr("id");
		 var id=$("#"+ps).data("closeId");
		 $("#aboutPorperties").data("panelId",ps);
		 var options = {
	   				url:'${ctx}/sys/desktop/desktop-to-user!queryDesktopToUserById.action',
	   				 data : {
	   	                 "id" :id
	   	             },
	   				success:function(data) { 
	   					var jsonData=data.msg;
	   					var options=jsonData.options;
	   					$('#viewForm').form('load', jsonData);
	   					if(options){
	   						var opt=$.parseJSON(options);
			   				$("#createUser").attr("checked",opt.createUser);
		                    $("#createTime").attr("checked",opt.createTime);
	   					}
	   					
	   				}
	   		};
	   		fnFormAjaxWithJson(options,true);		
		
	 }
	function updatePanle(){
		var pa = $(this).parent().parent().next();
		var ps = pa.attr("id");
		var p=$("#"+ps).panel();
		p.panel("refresh");
	}
	
	 function doUpdate(){
		var layTitle=$("#title").val();
		var layDisplayNum=$("#displayNum").val();
		var layFontNum=$("#fontNum").val();
		var layHeight=$("#height").val();
		var ps=$("#aboutPorperties").data("panelId");
		var p=$("#"+ps).panel();
	    var id=$("#"+ps).data("closeId");
	    var isCreateUser=false;
	    var isCreateTime=false;
	    if($("#createUser").attr("checked")){
	    	isCreateUser=true;
	    }
	    if($("#createTime").attr("checked")){
	    	isCreateTime=true;
	    }
	    var opt='{"createUser":'+isCreateUser+',"createTime":'+isCreateTime+'}';
		var options = {
   				url:'${ctx}/sys/desktop/desktop-to-user!updateDesktopDisplayNum.action',
   				 data : {
   	                 "id" :id ,
   	                 "title":layTitle,
   	                 "displayNum":layDisplayNum,
   	                 "fontNum":layFontNum,
   	                 "height":layHeight,
   	                 "options":opt
   	             },
   				success:function(data) {  
   					p.panel("setTitle",layTitle);
   					p.panel('resize',{
   						//width: 100,
   						height: layHeight
   					});
   					p.panel("refresh");
   					$("#aboutPorperties").window("close");
   				}
   		};
   		fnFormAjaxWithJson(options,true);		
		 
	 }

	 function saveStateToDesttop(id,layoutVal){
		 var options = {
   				url:'${ctx}/sys/desktop/desktop-to-user!updateDesktopLayoutVal.action',
   				 data : {
   	                 "id" :id,
   	                 "layoutVal":layoutVal
   	             },
   				success:function(data) {  
   					
   				}
   		};
   		fnFormAjaxWithJson(options,true);		
	 }
	
	function initPanels(id,layout,panels){
		$('#pp'+id).portal({
			fit:true,
			border:false,
			//设置cookie把每个id的状态存入cookie中
			onStateChange:function(){
				var state = getPortalState(id);
				var date = new Date();
				date.setTime(date.getTime() + 24*3600*1000);
				//document.cookie = 'portal-state'+id+'='+state+';expires='+date.toGMTString();
				//将状态永久的保存在数据库中，防止缓存被清掉
				saveStateToDesttop(id,state);
				
			}
		});
	/* 	var state = getCookie('portal-state'+id);
		if (!state){
			//state = 'p1,p2:p3,p4:p5';	// the default portal state
			state=layout;
		} */
		//把新的面板状态丢入body中
		addPanels(layout,id,panels);
		$('#pp'+id).portal('resize');
		
		$('#hidPP'+id).portal({
			border:false,
			fit:true
		});
	}

	
	
	//实现最大化
	function maxPanel() {
		var pid=$(this).data("pid");
		var closeId=$(this).data("closeId");
		var id = $(this).attr("id");
		$("#hidPP"+pid).show();
		$("#pp"+pid).hide();
		var p = $('<div/>').appendTo('body');
		var option = $.extend({}, $("#"+id).panel("options"));
		$.extend(option, {
				id:"tmp_panel"+pid,
				maximized:true,
				closable:false,
				onMaximize:function() {},
				onRestore:function() {restorePanel(pid);}
		});
		p.panel(option);
		p.data("closeId",closeId);
		$('#hidPP'+pid).portal('add', {
			panel:p,
			columnIndex:0
		});
		$('#hidPP'+pid).portal('resize');
		$("#"+id).panel("restore");
	}

	function restorePanel(pid) {
		$("#hidPP"+pid).hide();
		$("#pp"+pid).show();
		var panels = $("#hidPP"+pid).portal("getPanels", 0);
		$.each(panels, function(k, v) {
			$("#hidPP"+pid).portal('remove', v);
		});
	}
		 function addTabs(){
			 $("#aboutTabs").window("open");
			 queryDesktopLevel1(); 
		 }
	
		 function addPortal(id){
			 $("#tt").data("parentId",id);
			 $("#aboutPortal").window("open");
			 queryDesktopLevel2(id);
		 }
		 
		 function modifyPortal(id){
			 $("#tt").data("parentId",id);
			 $("#panelmodifycol").window("open");
			 modifyLayout();
			 
		 }
		function refreshPor(id){
			var options = {
				url:'${ctx}/sys/desktop/desktop-to-user!refreshByDesktopId.action',
				 data : {
	                 "desktopId" :id
	             },
				success:function(data) {
					if(data.msg) {
						refreshPortal(data.msg[0]);
						}
  					}
				};
			fnFormAjaxWithJson(options,true);				 
		}
		 //查询当前用户下的Tabs页
		 function queryDesktopLevel1(){
				var options = {
	      				url:'${ctx}/sys/desktop/desktop-to-user!queryLevel1Desktop.action',
	      				success:function(data) {  
	      					var obj=data.msg;
		      				  treeInit(obj,"tree_tab",{"id":"id","text":"title","pid":null},"",{"id":"0","text":"tab页内容","pid":""},"",true);
	      				}
	      		};
	      		fnFormAjaxWithJson(options,true);
		 }
		
		 //查询当前tab下课自定义的桌面
		 function queryDesktopLevel2(parentId) {
	      		var options = {
	      				url:'${ctx}/sys/desktop/desktop-to-user!queryLevel2Desktop.action',
	      				 data : {
	      	                 "parentId" :parentId
	      	             },
	      				success:function(data) {  
	      					var obj=data.msg;
		      				  treeInit(obj,"tree_portal",{"id":"id","text":"title","pid":"parentId"},"",{"id":"0","text":"首页内容","pid":""},"",true);
	      				}
	      		};
	      		fnFormAjaxWithJson(options,true);			
	    }
		 
		function modifyLayout(){
			$("div.cols_div").hide();
		    var parentId=$("#tt").data("parentId");
			var options = {
      				url:'${ctx}/sys/desktop/desktop-to-user!queryDesktopToUserByDesktopId.action',
      				 data : {
      	                 "parentId" :parentId 	               
      	             },
      				success:function(data) {  
      					var obj=data.msg;
      					if(obj) {  
      						 var layType=obj.layoutType;
      					     var sType=layType.split(";");
      					      for(var i=0;i<sType.length;i++){
      					    	 var j=i+1;
      					    	 if(sType[i]){
      					    		 var s=sType[i].substring(0,2);
      					    		 $("#layoutdisplay"+j).show();
            					     $("#layout"+j).val(s);
      					    	 }
      					    	
      					    }   
      					}
      				}
      		};
      		fnFormAjaxWithJson(options,true);	
		}
		 
	
       //修改桌面布局	
		 function setLayout(layoutType){
    	  
				   var parentId=$("#tt").data("parentId");
				   var type="";
				   if(layoutType==1){
						type="90%";
					}else if(layoutType==2){
						type="45%;45%";
					}else if(layoutType==3){
						type="30%;30%;30%";
					}
		       		var options = {
		       			
		      				url:'${ctx}/sys/desktop/desktop-to-user!updateLayout.action',
		      				 data : {
		      	                 "parentId" :parentId,
		      	                 "strType":type
		      	                  
		      	             },
		      				success:function(data) {  
		      					var obj=data.msg;
		      					if(obj) {  
		      						//刷新tab下的布局
		      						refreshPortal(obj);
		      						$("#panelmodifycol").window("close");
		      					}
		      				}
		      		};
		      		fnFormAjaxWithJson(options,true); 		
			 
		 }
		 
       //刷新tab页面
		 function refreshPortal(obj){
			 var layoutType=obj.layoutType;
		     var layoutVal=obj.layoutVal;
			 var id=obj.id;
			 var desktopId=obj.desktop.id;
			 var tab = $('#tt').tabs('getSelected');
			 var url=[];
			 url.push("${ctx}/sys/portal/panel.action?id=");
			 url.push(id);
			 url.push("&desktopId=");
             url.push(desktopId);
             url.push("&layoutType=");
             url.push(encodeURIComponent(layoutType));
             url.push("&layoutVal=");
             url.push(encodeURIComponent(layoutVal));
             tab.panel("refresh", url.join(""));
		 }
		 
	
		 function selectPortal(){
			 var nodes=$("#tree_portal").tree("getChecked","check");
			 var ids=[];
			 $.each(nodes,function(i,v){
				 if(v.id!=0){
					 ids.push(v.id);
				 }
				
			 });
			return ids;
		 }
		 
		//用户自定义个人桌面
		 function doAdd(){
			 var parentId=$("#tt").data("parentId");
			 var ids=selectPortal();
		     var options = {
		      			url:'${ctx}/sys/desktop/desktop-to-user!addPortal.action',
		      			data : {
		      	               "ids":ids,
		      	               "parentId" :parentId
		      	         },
		      			success:function(data) {  
		      				var obj=data.msg;
	      					if(obj) {  
	      						//刷新tab下的布局
	      						refreshPortal(obj);
	      						$("#aboutPortal").window("close");
	      					}
		      			},
		      	       traditional:true
		      	};
		    fnFormAjaxWithJson(options,true);			
			
		 }
		
		//修改列宽	
		function changeColWidth(){
			var colsOne=$("#layout1").val()+"%";
			var colsTwo=$("#layout2").val()+"%";
			var colsThree=$("#layout3").val()+"%";
			var cols=[];
			cols.push(colsOne);
			cols.push(colsTwo);
			cols.push(colsThree);
			var sType=cols.join(";");
			 var parentId=$("#tt").data("parentId");
	      		var options = {
	      				url:'${ctx}/sys/desktop/desktop-to-user!updateColsHeight.action',
	      				 data : {
	      	                 "parentId" :parentId,
	      	                 "strType":sType
	      	             },
	      				success:function(data) {  
	      					var obj=data.msg;
	      					if(obj) {  
	      						//刷新tab下的布局
	      						refreshPortal(obj);
	      						$("#panelmodifycol").window("close");
	      					}
	      				}
	      		};
	      		fnFormAjaxWithJson(options,true);			
		 
		}
		 
		 function doCanel(){
			 $("#aboutPortal").window("close");
		 }
		 
	
		 
		 function cancelUpdate(){
			 $("#aboutPorperties").window("close");
			 
		 }
		 //添加自定义Tab页
		 function doAddTab(){
			 var nodes=$("#tree_tab").tree("getChecked","check");
			 var ids=[];
			 $.each(nodes,function(i,v){
				 if(v.id!=0){
					 ids.push(v.id);
				 }				
			 });		
		     var options = {
		      			url:'${ctx}/sys/desktop/desktop-to-user!addTabs.action',
		      			data : {
		      	               "ids":ids
		      	         },
		      			success:function(data) {  
		      				var obj=data.msg;
		      				if(obj){
		      					$("#aboutTabs").window("close");
	      						for(var i =0;i<obj.length;i++) { 	  
	      							var divs = [];	      				
	      							
	      							divs.push('<div id="p-tools_');
	      							divs.push(obj[i].desktop.id);
	      							divs.push('"><a href="#" class="icon-mini-add" onclick="addPortal(');
	      							divs.push(obj[i].desktop.id);
	      							divs.push(')"></a><a href="#" class="icon-mini-edit" onclick="modifyPortal(');
	      							divs.push(obj[i].desktop.id);
	      							divs.push(')"></a><a href="#" class="icon-mini-refresh" onclick="refreshPor(');
	      							divs.push(obj[i].desktop.id);
	      							divs.push(')"></a></div>');
	      							$("#ptools").append(divs.join(""));
	      							$('#tt').tabs('add',{
	      								title:obj[i].title,
	      								href:"${ctx}/sys/portal/panel.action?id="+obj[i].id+"&desktopId="+obj[i].desktop.id+"&layoutType="+encodeURIComponent(obj[i].layoutType)+"&layoutVal="+encodeURIComponent(obj[i].layoutVal),			
	      								closable:false,
	      								tools: "#p-tools_"+obj[i].desktop.id	      						
	      								});

	      							} 
		      					} 		
		      					
		      				},
		      	       traditional:true
		      	};
		    fnFormAjaxWithJson(options,true);	
		 }
		 
		 function doRefreshTab(){
			 alert("doRefreshTab");
		 }
		 		
		 function openImageNews(url,title){
			parent.addTab(title,url);
		  }
		
	   
		 function changeReportType(report_type,chartdiv){
			 switch(report_type){
			 //Single Series Charts
			 case "Column3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Column3D.swf"});break;
			 case "Column2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Column2D.swf"});break;
			 case "Line":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Line.swf"});break;
			 case "Area2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Area2D.swf"});break;
			 case "Bar2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Bar2D.swf"});break;
			 case "Pie2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Pie2D.swf"});break;
			 case "Pie3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Pie3D.swf"});break;
			 case "Doughnut2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Doughnut2D.swf"});break;
			 case "Doughnut3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Doughnut3D.swf"});break;
			 case "Pareto2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Pareto2D.swf"});break;
			 case "Pareto3d":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Pareto3D.swf"});break;
			 //Multi-series Charts 
			 case "MSColumn3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSColumn3D.swf"});break;
			 case "MSColumn2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSColumn2D.swf"});break;
			 case "MSBar2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSBar2D.swf"});break;
			 case "MSBar3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSBar3D.swf"});break;
			 case "MSArea":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSArea.swf"});break;
			 case "MSLine":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSLine.swf"});break;
			 case "Marimekko":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Marimekko.swf"});break;
			 case "ZoomLine":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/ZoomLine.swf"});break;
			 //Stacked Charts
			 case "StackedColumn2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedColumn2D.swf"});break;
			 case "StackedColumn3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSColumn3D.swf"});break;
			 case "StackedBar2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedBar2D.swf"});break;
			 case "StackedBar3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedBar3D.swf"});break;
			 case "StackedArea2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedArea2D.swf"});break;
			 case "MSStackedColumn2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSStackedColumn2D.swf"});break;
			 case "MSColumn2DLineDY":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSCombiDY2D.swf"});break;
			 //Combination Charts 
			 case "MSCombi3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSCombi3D.swf"});break;
			 case "MSCombi2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSCombi2D.swf"});break;
			 case "MSColumnLine3D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSColumnLine3D.swf"});break;
			 case "StackedColumn2DLine":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedColumn2DLine.swf"});break;
			 case "StackedColumn3DLine":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedColumn3DLine.swf"});break;
			 case "MSCombiDY2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSCombiDY2D.swf"});break;
			 case "MSColumn3DLineDY":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSColumn3DLineDY.swf"});break;
			 case "StackedColumn3DLineDY":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedColumn3DLineDY.swf"});break;
			 case "MSStackedColumn2DLineDY":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSStackedColumn2DLineDY.swf"});break;
			 //XY Plot Charts 
			 case "Scatter":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Scatter.swf"});break;
			 case "Bubble":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/Bubble.swf"});break;
			 //Scroll Charts 
			 case "ScrollColumn2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/ScrollColumn2D.swf"});break;
			 case "ScrollLine2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/ScrollLine2D.swf"});break;
			 case "ScrollArea2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/ScrollArea2D.swf"});break;
			 //case "StackedColumn2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/StackedColumn2D.swf"});
			// case "MSCombi2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSCombi2D.swf"});
			// case "MSCombiDY2D":$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/MSCombiDY2D.swf"});
			
			 default:$("#"+chartdiv).updateFusionCharts({"swfUrl": "${ctx}/js/FusionCharts/swf/SSGrid.swf"});break;
			 
			 }

		} 
		
		 
			function initContent(type,id,uId) {
				 var options = {
							url:'${ctx}/sys/desktop/desktop-to-user!queryDesktopSource.action',
							 data : {
				                 "id" :id
				             },
							success:function(data) {
									var wid = $("#"+uId).innerWidth();
								    var obj=data.msg;
									var hg = obj.height-55;
								    var showDisplayNum=obj.displayNum;
								    var showFontNum=obj.fontNum;
								    var opts=obj.options;//{'createUser':true,'createTime':true}
								    var jsonData=obj.list;
								    var len=jsonData.length;
								    var content=[];
								    if(showDisplayNum<=len){
								    	len=showDisplayNum;
								    }
								    for(var i=0;i<len;i++){
								    	//创建人
								    	 var userName=jsonData[i]["createUserCaption"];
								    	 /*var uObj=_userList[jsonData[i].createUser];
			                             if(uObj){
			                                 userName=uObj.realName;
			                             }else{
			                                 userNeme=jsonData[i].createUser;
			                             }*/
			                             //标题字数控制
			                             var title=jsonData[i].title;
			                             var showTitle="";
			                             if(showFontNum<=title.length){
			                            	 showTitle=title.substr(0,showFontNum)+"...";
			                             }else{
			                            	showTitle=title; 
			                             }
			                             //是否显示创建者, 创建时间
			                             var createUser="";
			                             var createTime="";
			                             if(opts){
			                            	 var opt=$.parseJSON(opts);
			                                 if(opt.createUser){
			                                	 createUser="["+userName+"]";
			                                 }
			                                 if(opt.createTime){
			                                	 createTime="("+jsonData[i].createTime+")";
			                                 }
			                             }
			                               if(type=="textList"){
			                            	   var str ="<li><a href='javascript:void(0);' onclick='openImageNews(\""+"${ctx}/"+obj.desktop.dataUrl+jsonData[i].id+"\",\""+jsonData[i].title+"\")' >"+showTitle+createTime+createUser+"</a></li>";
											   content.push(str);
			                               }else if(type=="picList"){
			                            	   content.push("<div  style='z-index: 3; display: list-item;'><a   href='javascript:void(0);' onclick='openImageNews(\""+"${ctx}/"+obj.desktop.dataUrl+jsonData[i].id+"\",\""+jsonData[i].title+"\")' ><img  title='"+showTitle+"' src='"+obj.systemPath+jsonData[i].src+"' style='border:0;width:100%;height:" + hg + "px;'/><span>"+showTitle+"</span></a></div>");
			                               }else if(type=="textSlides"){//文字滚动集合标签装配
			                            	   var str ="<li><a href='javascript:void(0);' onclick='openImageNews(\""+"${ctx}/"+obj.dataUrl+jsonData[i].id+"\",\""+jsonData[i].title+"\")' >"+showTitle+createTime+createUser+"</a></li>";
											   content.push(str);
											}
										 
			                             
								    }
								    if(type=="textList"){
								    	if(obj.desktop.moduleUrl){
								    	   content.push("<div style='text-align:right'><a href='javascript:void(0);' onclick='openImageNews(\""+"${ctx}/"+obj.desktop.moduleUrl+"\",\""+obj.title+"\")' ><s:text name="system.sysmng.info.more.title"/></a></div>");
								    	}
								    	   var res=content.join("");
										   $("#"+uId).append(res); 			   
								    }else if(type=="picList"){//初始化图片切换插件
							    		var res=content.join("");
							    		$("#"+uId).append(res);
										 if(len==1){
											 $("#"+uId).slidesjs({
												 width:wid,
											 	 height: hg,
									        	 navigation: false										 
											 });
									    }else if(len>1){
									    	$("#"+uId).slidesjs({
												 width:wid,
												 height: hg,
									        	 navigation: false,						     
		 							        	 play: {
									        		 auto: true,
									                 interval: 4000,
									                 pauseOnHover: true
									             } 
											 });
									    }
									   }else if(type=="iframe"){
												   var url=obj.desktop.moduleUrl || obj.desktop.dataUrl;
												   $("#"+uId).attr("src",url);
										}else if(type=="textSlides"){//初始化文字滚动插件
											var res=content.join("");
											var ul = uId+"_ul";
											$("#"+ul).append(res); 	
											new Marquee([uId,ul],2,1,wid,40,30,0,0);	
										}
							 }
					};
					fnFormAjaxWithJson(options,true); 		 
			}
		 

		 
		 //tab初始化
		 $(function (){
			 //_userList=findAllUser();
			 $("#tt").tabs({
				 tools:"#tab-tools"/* , 
			     onContextMenu: function(e, node){
					   $('#updateCols').menu('show', {
							left: e.pageX,
							top: e.pageY
						});
				 } */
				
			});
		 });
		
	</script>
	<!--[if lte IE 7]>  
  	<script type="text/javascript">
  		$(function() {
  			$(document.body).layout("resize");
  		});
  	</script>  
	<![endif]-->    
</body>
</html>

