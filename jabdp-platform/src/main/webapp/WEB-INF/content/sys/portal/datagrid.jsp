<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
    Random rad=new Random();
    int radParam=rad.nextInt();
%> 
   <script type="text/javascript">
    $(document).ready(function(){
    	$('#tt_${param.id}_<%=radParam%>').datagrid({
			url: '${ctx}/js/datagrid_data3.json',
			//: 'DataGrid',
			//width: "100%",
			//height: "100%",
			fitColumns: true,
			nowrap:false,
			agination : true,
			pagination:true,
			//rownumbers:true,
			showFooter:false,//定义是否显示一行页脚。
			columns:[[
				{field:'itemid',title:'姓名',width:80},
				{field:'productid',title:'性别',width:80},
				{field:'listprice',title:'城市',width:80,align:'right'},
				{field:'unitcost',title:'Email',width:80,align:'right'},
				{field:'attr1',title:'创建人',width:80} ,
				{field:'status',title:'状态',width:60,align:'center'} 
			]],
			onClickRow:function(rowIndex, rowData){
                 openImageNews("http://www.baidu.com","测试标题");
            }
		});
    });
          
   </script>

<table id="tt_${param.id}_<%=radParam%>"></table>
</body>
</html>