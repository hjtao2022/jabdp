<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="/common/meta.jsp"%>
<script type="text/javascript">
	var _userList = {};
	$(document).ready(function() {
		//_userList = findAllUser();
		$("#queryList").datagrid(getOption());
	});

	//初始化流程流程的datagrid历史记录
<%--var pId = "${param.processInstanceId}";--%>
function getOption() {
	return {
		width : 'auto',
		height : 'auto',
		fit: true,
		nowrap : false,
		striped : true,
		url : '${ctx}/gs/process!getProcessInfoListById.action?processInstanceId=${param.processInstanceId}&id=${param.id}&entityName=${param.entityName}',
		sortName : 'id',
		sortOrder : 'desc',
		remoteSort: true,
		idField : 'id',
		pageSize: 20,
		pageList: [5, 10, 20, 50, 100],
		columns : [ [
				{
					field : 'userId',
					title : '<s:text name="system.sysmng.process.dealUser.title"/>',
					width : 80,
					formatter:function(value, rowData, rowIndex){
						return rowData["userName"];
                        /*var uObj=_userList[value];
                        if(uObj){
                        	//var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
                            //return uObj.realName + "[" + uObj.loginName + "]" + nickName;
							return uObj.nickName;
                        }else{
                            return value;
                        }*/
               		 }
				},
				{
					field : 'dealTime',
					title : '<s:text name="system.sysmng.process.dealTime.title"/>',
					width : 150,
					sortable : true
				},
				{
					field : 'taskName',
					title : '<s:text name="system.sysmng.process.task.title"/>',
					width : 150,
					sortable : true
				},
				{
					field : 'isApprove',
					title : '<s:text name="system.sysmng.process.isApprove.title"/>',
					width : 60,
					align : 'center',
					formatter:function(value,rowData,rowIndex){
						if(value==1){
							return "同意";
						}else if(value==0){
							return "不同意";
						}
						else{
							return "";
						}
					}
				}, {
					field : 'reason',
					title : '<s:text name="意见（原因）"/>',
					width : 400,
					align : 'center'
				}
		] ],
		pagination : true,
		rownumbers : true
	};
}
 </script>
 </head>  
 <body class="easyui-layout" >
   <div region="center"  style="overflow:hidden;" border="false">
   	<table class="easyui-datagrid" id="queryList" border="false"></table>
   </div>
</body>
</html>