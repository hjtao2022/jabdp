<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="/common/meta.jsp"%>
</head>
<body class="easyui-layout" fit="true">
 	<div region="center" border="false"
				style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
		<form action="" method="post">
			<input type="hidden" id="taskIds" name="taskIds" value="${param.taskIds}" />
					<table align="center">
						<tbody >
							<tr id="userSel">
							<th><label ><s:text name="system.sysmng.process.customUser.title"/>:</label>
								</th>
								<td >
								<input type="hidden" name="userIds" id="userIds"/>
							<ul  style="text-align: center; width: 230px;height:200px;" id="roleIds" class="ztree"></ul>	
							</td>			
							</tr>
						</tbody>	
					</table>

				</form>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
		initMenu();
	});

var setting = {
		check: {
			enable: true
		},
		data: {
			simpleData: {
				enable: true
			}
		}
	};


	//根据一个任务环节角色，显示用户树
 function initMenu(){
		var options = {
				url : '${ctx}/gs/process!getRolesList.action',
				data : {
					"roles":"${param.roles}"
				},
				success : function(data) {
					if (data.msg && data.msg.length>0) {						
						$.fn.zTree.init($("#roleIds"), setting, data.msg);
						zTree = $.fn.zTree.getZTreeObj("roleIds");
						zTree.expandAll(true);
					}else{
						$("#userSel").hide();
					}
				}
			};
			fnFormAjaxWithJson(options,true);
	} 
 function getIds(){
	    var uids = null;
		var zTree = $.fn.zTree.getZTreeObj("roleIds");
		if(zTree!=null){
			var nodes = zTree.getCheckedNodes(true);
			if(nodes.length){
				var nodeIds = [];
				$.each(nodes, function(k,v) {
					nodeIds.push(v.id);
				});
				uids = nodeIds.join(",");
				$("#userIds").val(uids);
			} else {
				$.messager.alert('提示信息','请选择一个或多个审批人','info');
				return false;
			}			
		}
		return uids;
 }
 //启动流程
/* function doAgree(){
	 var flag = getIds();
	 if(flag){
		var tids = $("#taskIds").val();
		var taskId = tids.split(",");
		var uids = $("#userIds").val();
		jwpf.doBatchAgreeProcess(taskId, uids);
		
	 }
 }*/
 </script>	
</body>
</html>
	
