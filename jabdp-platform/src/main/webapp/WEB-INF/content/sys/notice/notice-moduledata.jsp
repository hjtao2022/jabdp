<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
	<script type="text/javascript">
	$(function(){
			showData();		
		});

		function showData(){
			var getDataFunc = getJsonData || parent.getJsonData;
			var data = getDataFunc();			
			var moduleKey = data["moduleKey"];
			var moduleDataId = data["moduleDataId"];
			$("#id").val(moduleDataId);
			$("#entityName").val(moduleKey);
			var moduleFields = data["moduleFields"];	
			var fieldsJson = jQuery.parseJSON(moduleFields);
			var table = [];
			table.push('<table align="center" border="0" cellpadding="0" cellspacing="1" class="table_form"><tbody>');
			var len = fieldsJson.length;
			for(var i =0;i<len;i++){
			table.push('<tr><th><label for="');
			table.push("notice_"+fieldsJson[i].key);
			table.push('">');
			table.push(fieldsJson[i].caption);
			table.push(':</label></th><td><textarea style="windth:100%;height:60px;" name="');
			table.push("notice_"+fieldsJson[i].key);
			table.push('" id="');
			table.push(fieldsJson[i].key);
			table.push('" ></textarea></td></tr>');
			}
			table.push('</tbody></table>');
			$("#saveForm").append(table.join(""));
		} 
		
		function saveData(){
			var options = {
					url : '${ctx}/gs/gs-mng!saveFieldsData.action',
					success : function(data) {
						if (data.msg) {
						var	noticeToUserId = _nTouId || parent._nTouId;
						window.doRead(noticeToUserId);
						}
					}
				};
					fnAjaxSubmitWithJson('saveForm', options);

		}
		function cancel(){
			$("#dataWin").window("close");
		}
	</script>
<div class="easyui-layout" fit="true" >

		<div region="center"  style="overflow:hidden;padding:20px 50px;">
			<div region="center" border="false"
				style="padding: 2px; background: #fff; overflow: auto; border: 1px solid #ccc;">
				<form action="" name="saveForm" id="saveForm" method="post">
					<input type="hidden"  name="id" id="id"/>
					<input type="hidden"  name="entityName" id="entityName"/>
				
				</form>
			</div>
		</div>
		
	<div region="south" border="false"
				style="text-align: right; height: 30px; line-height: 30px;">
				
				<a id="mm" class="easyui-linkbutton" iconCls="icon-ok"href="javascript:void(0)" 
					onclick="saveData()"><s:text name="system.button.save.title" /></a> 
					<a class="easyui-linkbutton" iconCls="icon-cancel" href="javascript:void(0)"
						onclick="cancel();"><s:text name="system.button.cancel.title" /> </a>
			</div>

</div>
