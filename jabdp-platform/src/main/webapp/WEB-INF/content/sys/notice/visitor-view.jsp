<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!-- 查看更多 -->
<div id="moreVisitors"></div>
<!-- 访问者css -->
<style type="text/css">
.visitor {
 padding: 10px;
 background-color: #fff;
}
.visitor .content {
 padding: 5px;
 margin-left: 10px;
 background-color: #eee;
}
.visitor a {
 padding: 5px;
 margin-left: 10px;
 text-decoration: blink;
}
</style>
<script type="text/javascript">
var _relevanceId = "${param.id}"; // 关联ID
var _pageSize = 10; // 查询几位最近访问者
function doInitForm(data) { // 页面元素加载
	if(data && data.length) {
		var _visitor = $("#visitor");
		_visitor.addClass("visitor");
		_visitor.append("<sapn>最近访问者：</span>");
		for(var i in data) {
			/*var uObj = _userList[data[i].USER_ID];
			var name = "未知者";
			if(uObj) {
				name = uObj.realName;
			}*/
			var name = data[i]["userName"];
			_visitor.append("<span class='content'>" + name + "</span>");
		}
		_visitor.append('<a onclick="moreVisitors();" href="javascript:void(0)" title="点击查看更多">更多... </a>');
	}
}
function initVisitor() { // 页面元素初始化
	$("#moreVisitors").window({ // 初始化easyui窗口
		minimizable : false,
		maximizable : false,
		modal : true,
		resizable : false,
		title : "<s:text name='访问者列表'/>",
		width : 500,
		height : 400,
		closed : true,
		onOpen:function() {
			$(this).window("move", {
				top:($(window).height()-400)*0.5,
				left:($(window).width()-500)*0.5
			});
		}
	});
}
function moreVisitors() { // 查看所有访问者
	$("#moreVisitors").window("open");
	var url = "${ctx}/sys/notice/visitor-grid.action?id=" + _relevanceId;
	$("#moreVisitors").load(url);
}
function doAddVisitor() { // 记录访问者
	var options = {
		url : '${ctx}/sys/visitor/visitor!addVisitor.action',
		data : {
			id : _relevanceId
		}
	};
	fnFormAjaxWithJson(options);
}
function doModify() { // 页面数据初始化
	var options = {
		url : '${ctx}/sys/visitor/visitor!queryLateVisitors.action',
		data : {
			id : _id,
			size : _pageSize
		},
		success : function(data) {
			doInitForm(data.msg);
		}
	};
	fnFormAjaxWithJson(options);
}
$(function() {
	doAddVisitor();
	doModify();
	initVisitor();
});
</script>
