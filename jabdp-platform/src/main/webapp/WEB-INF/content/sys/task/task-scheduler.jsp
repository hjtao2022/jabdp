<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
var operMethod="view";
var _userList={};
var _statusDict = {"0":"停止","1":"运行"};
var _dateTypeDict = {"Everyday":"每天","Someday":"每月","Date":"指定日期","Week":"每周","Hour":"每隔*小时","Minute":"每隔*分钟","Second":"每隔*秒"};
function getOption() {
	return {
		title:'<s:text name="任务调度"/>',
		width:600,
		height:350,
		nowrap: false,
		striped: true,
		fit: true,
		url:'${ctx}/sys/task/task-scheduler!queryList.action',
		sortName: 'id',
		sortOrder: 'desc',
		idField:'id',
		frozenColumns:[[
          {field:'ck',checkbox:true}
		]],
		columns:[[
					{field:'name',title:'<s:text name="调度名称"/>',width:160},
					{field:'dateType',title:'<s:text name="调度周期"/>',width:100,
					 formatter:function(value,rowData,rowIndex){
						var val = _dateTypeDict[value];
						if(val) {
							return val;
						} else {
							return "";
						}
					}},
					{field:'status',title:'<s:text name="运行状态"/>',width:60,
						 formatter:function(value,rowData,rowIndex){
							var val = _statusDict[value];
							if(val) {
								return val;
							} else {
								return "";
							}
					}},
					{field:'remark',title:'<s:text name="调度描述"/>',width:200},
					{field:'createTime',title:'<s:text name="创建时间"/>',width:120,align:'center'},
					{field:'createUser',title:'<s:text name="创建者"/>',width:120,formatter:formatterUser},
					//{field:'lastUpdateTime',title:'<s:text name="上次修改时间"/>',width:150,align:'center'},
					//{field:'lastUpdateUser',title:'<s:text name="上次修改者"/>',width:150,formatter:formatterUser},
					{
						field : 'oper',
						title : '<s:text name="system.button.oper.title"/>',
						width : 250,
						align : 'center',
						formatter : operFormatter
					}
				]],
		toolbar : [
				'-'
				,{
				    id : 'bt_add',
				    text : '<s:text name="system.button.add.title"/>(I)',
				    iconCls : 'icon-add',
				    handler : function() {
				        doAdd();
				    }
				}
				,
				{
				    id : 'bt_del',
				    text : '<s:text name="system.button.delete.title"/>(D)',
				    iconCls : 'icon-remove',
				    handler : function() {
				        doDelete();
				    }                           
				}
				,
				{
				    id : 'bt_view',
				    text : '<s:text name="system.button.view.title"/>(E)',
				    iconCls : "icon-search",
				    handler : function() {
				        doView();
				    }
				}
		],		
		pagination:true,
		rownumbers:true,
		onDblClickRow:function(rowIndex, rowData){
        	doDblView(rowData.id);
        	//$('#queryList').datagrid('unselectRow',rowIndex);
        }
	};
}

function operFormatter(value, rowData, rowIndex){
	var strArr = [];
	strArr.push('<a href="javascript:void(0);" onclick="doDblView(');
	strArr.push(rowData.id);
	strArr.push(')"><s:text name="system.button.view.title"/></a>');
	if(rowData.status == "0") {
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doEdit(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="system.button.modify.title"/></a>');
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doDelete(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="system.button.delete.title"/></a>');
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doStart(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="启动"/></a>');
	} else {
		strArr.push('&nbsp;<a href="javascript:void(0);" onclick="doStop(');
		strArr.push(rowData.id);
		strArr.push(')"><s:text name="停止"/></a>');
	}
	return strArr.join("");
}

function formatterUser(value, rowData, rowIndex){
	/*var uObj=_userList[value];
    if(uObj){
        return uObj.realName + "[" + uObj.loginName + "]";
    }else{
        return value;
    }*/
    return rowData["createUserCaption"];
}

function doStart(id) {
	var options = {
	          url : '${ctx}/sys/task/task-scheduler!startTaskScheduler.action',
	          data : {
	              "id" : id
	          },
	          async:false,
	          success : function(data) {
	        	  doQuery();
	          }
	};
	fnFormAjaxWithJson(options);
}

function doStop(id) {
	var options = {
	          url : '${ctx}/sys/task/task-scheduler!stopTaskScheduler.action',
	          data : {
	              "id" : id
	          },
	          async:false,
	          success : function(data) {
	        	  doQuery();
	          }
	};
	fnFormAjaxWithJson(options);
}

function doChangeDateType(val) {
	var dateType = val;
	$("#Month").hide();
	$("#Week").hide();
	$("#Hour").show();
	$("#Minute").show();
	$("#Second").hide();
	switch(dateType) {
		case "Everyday":
			$("#Second").hide();
			break;
		case "Someday":
			$("#Month").show();
			$("#MonthMonth").hide();
			$("#Week").hide();
			break;
		case "Date":
			$("#Month").show();
			$("#MonthMonth").show();
			$("#Week").hide();
			break;	
		case "Week":
			$("#Month").hide();
			$("#Week").show();
			break;
		case "Hour":
			$("#Hour").show();
			$("#Minute").hide();
			$("#Second").hide();
			break;
		case "Minute":
			$("#Hour").hide();
			$("#Minute").show();
			$("#Second").hide();
			break;
		case "Second":
			$("#Hour").hide();
			$("#Minute").hide();
			$("#Second").show();
			break;
		default:
			break;	
	}
}

function checkWeek(){
	return $("input[name=runWeeks]:checked").length == 0;
}	

function checkValues(){
	var dateTypeVal = $("#dateType").val();
  	if (dateTypeVal == "Week") {
	   if (!checkWeek()) {
		  $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>','必须从每周中选择一天','info');
	      return false;		   			
	   }
	}
    return true;
}

function doQuery() {
	var param = $("#queryForm").serializeArrayToParam();
	$("#queryList").datagrid("load", param);
}

//新增
function doAdd() {
    operMethod = "add";
    var title = "<s:text name="system.button.add.title"/><s:text name="任务调度"/>";
	$("#detail_win").window("setTitle", title);
	$("#detail_win").window("open");
}

//修改
function doEdit(id) {
	operMethod = "modify";
	$("#detail_win").data("id", id);
	var title = "<s:text name="system.button.modify.title"/><s:text name="任务调度"/>";
	$("#detail_win").window("setTitle", title);
	$("#detail_win").window("open");
}

//查看
function doView() {
	 var id=0;
	 var rows = $('#queryList').datagrid('getSelections');
	  if(rows.length!=1){
		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
	     return ;
	 } else{
		 id=rows[0].id;
	 }
	 doDblView(id); 
}

//查看tab
function doDblView(id) {
	operMethod = "view";
	$("#detail_win").data("id", id);
	var title = "<s:text name="system.button.view.title"/><s:text name="任务调度"/>";
	$("#detail_win").window("setTitle", title);
	$("#detail_win").window("open");
}

function clearAddField() {
	$("#id").val("");
	$("#atmId").val("");
	$("#flowInsId").val("");
	$("#status").val("10");
}

function doPrepareForm(jsonData) {
	var weeks = jsonData["runWeek"];
	if(weeks) {
		$("input[name=runWeeks]").each(function(i) {
			if(weeks.charAt(i) == "1") {
				$(this).attr("checked","checked");
			} else {
				$(this).removeAttr("checked");
			}
		});
	}
	doChangeDateType(jsonData["dateType"]);
}

function doInitForm(data, callFunc) {
	if (data.msg) {
          var jsonData = data.msg;
          doPrepareForm(jsonData);
          $('#viewForm').form('load',jsonData);
          if(operMethod == "edit") {
          	clearAddField();
          }
    }
    if(callFunc) {
    	callFunc();
    }
}		

//页面数据初始化
function doModify(id, callFunc) {
    	var options = {
          url : '${ctx}/sys/task/task-scheduler!view.action',
          data : {
              "id" : id
          },
          success : function(data) {
              doInitForm(data, callFunc);
          }
     };
     fnFormAjaxWithJson(options);
}

//保存
function doSaveObj(){
	if(checkValues()) {
		var weeks = [];
		$("input[name=runWeeks]").each(function(i) {
			if($(this).attr("checked")) {
				weeks.push("1");
			} else {
				weeks.push("0");
			}
		});
		$("#runWeek").val(weeks.join(""));
		var options = {
				url : '${ctx}/sys/task/task-scheduler!save.action',
				success : function(data) {
					doAfterSave();
				}
		};
		fnAjaxSubmitWithJson("viewForm",options);
	}
}

//保存后继续新增
function doAfterSave() {
	doQuery();
	operMethod="add";
	doCloseWindow();
}

//删除一条记录
function doDelete(id) {
    var ids = [];
    if(id) {   
    	ids.push(id);
    } else {
        var rows = $('#queryList').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
        	if(rows[i].status == "1") {
        		$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="运行状态的记录不能被删除，请先停止运行，再进行删除"/>', 'info');
        		return;
        	}
            ids.push(rows[i].id);
        }
    } 
    if (ids != null && ids.length > 0) {
        $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
            if (r) {
                var options = {
                    url : '${ctx}/sys/task/task-scheduler!delete.action',
                    data : {
                        "ids" : ids
                    },
                    success : function(data) {
                        if (data.msg) {
                            $('#queryList').datagrid('clearSelections');
                            doQuery();
                        }
                    },
                    traditional:true
                };
                fnFormAjaxWithJson(options);
            }
        })

    } else {
        $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
    }
}

//关闭窗口
function doCloseWindow() {
	$("#detail_win").window("close");
}

function doEnable() {
    $("div.file_upload_button").show();
	$("input[type!=hidden],textarea,select").removeAttr("disabled");
	$("input.easyui-combobox").combobox('enable');
	$("select.easyui-combogrid").combogrid('enable');
	$("select.easyui-combotree").combotree('enable');
	$('input.Idate').addClass("Wdate");
	$(".jquery_ckeditor").each(function() {
		try {
			$(this).ckeditorGet().setReadOnly(false);
		} catch(e) {
			$(this).ckeditorGet().setReadOnly(false);
		}
	});
	$("#stSave").show();
}	

//将页面上的控件置为不可编辑状态
function doDisabled() {
    $("div.file_upload_button").hide();
	$("input[type!=hidden],textarea,select").attr("disabled", "disabled");
	$("input.easyui-combobox").combobox('disable');
	$("select.easyui-combogrid").combogrid('disable');
	$("select.easyui-combotree").combotree('disable');
	$('input.Idate').removeClass("Wdate");
	$(".jquery_ckeditor").each(function() {
		try {
			$(this).ckeditorGet().setReadOnly(true);
		} catch(e) {
			$(this).ckeditorGet().setReadOnly(true);
		}
	});
	$("#stSave").hide();
}

//设置焦点
function focusFirstElement() {
	if(operMethod != "view") {
			jwpf.focusFirstInputOnForm("viewForm");
		}
}	

function formClear() {
	$('#viewForm').form('clear');
	$("#status").val("10");
	$(".jquery_ckeditor").each(function() {
		try {
			$(this).ckeditorGet().setData("");
		} catch(e) {
			$(this).ckeditorGet().setData("");
		}
	});
	$("img.previewImageCls").attr("src", "");
}

function comboboxInit() {
	var taskJson = getJsonObjByUrl({}, "${ctx}/sys/task/task!findList.action");
	selectinit(taskJson, "taskId", {"key":"id","caption":"name"});
}

function initDetailWindow() {
	$("#detail_win").window({
		onOpen:function() {
			$(this).window("move", {
				top:($(window).height()-450)*0.5,
				left:($(window).width()-650)*0.5
			});
			var id = $("#detail_win").data("id");
			switch(operMethod) {
				case "add"://新增
					focusFirstElement();
					break;
				case "view"://查看
					doModify(id, doDisabled);
					break;
				case "modify"://修改
					doModify(id, focusFirstElement);
					break;
				case "edit"://复制
					doModify(id, focusFirstElement);
					break;
				default:
					doModify(id, doDisabled);
			}
		},
		onClose:function() {
			doEnable();
			formClear();
		}
	});
}

$(document).ready(function() {
	//_userList=findAllUser();
	
	focusEditor("query_name");
    doQuseryAction("queryForm");
	
	$('#queryList').datagrid(getOption());
	
	comboboxInit();
	initDetailWindow();
	
	$("#dateType").combobox({
		onChange:function(newVal, oldVal) {
			doChangeDateType(newVal);
		}
	});
	
	$("#bt_query").click(doQuery);
	$("#bt_reset").click(function() {
		$("#queryForm")[0].reset();
		doQuery();
	});
	
});
	</script>
</head>
<body class="easyui-layout">
    <div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr>
							<th><label for="query_name"><s:text name="调度名称"/>:</label></th>
							<td><input type="text" name="filter_LIKES_name" id="query_name" class="Itext"></input></td>
						</tr>
						<tr>
							<th><label for="query_dateType"><s:text name="调度周期"/>:</label></th>
							<td><select id="query_dateType" class="easyui-combobox" name="filter_EQS_dateType" style="width:160px">
									<option value="">所有</option>
									<option value="Everyday">每天</option>
									<option value="Someday">每月</option>
									<option value="Date">指定日期</option>
									<option value="Week">每周</option>
									<option value="Hour">每隔*小时</option>
									<option value="Minute">每隔*分钟</option>
									<option value="Second">每隔*秒</option>
								</select>
							</td>
						</tr>
						<tr>
							<th><label for="query_status"><s:text name="运行状态"/>:</label></th>
							<td><select id="query_status" class="easyui-combobox" name="filter_EQS_status" style="width:160px">
									<option value="">所有</option>
									<option value="0">停止</option>
									<option value="1">运行</option>
								</select>
							</td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
	</div>
	<div region="center" title="" style="overflow:hidden;" border="false">
		<table id="queryList" border="false"></table>
	</div>
	<div style="display:none;">
		<div id="detail_win" class="easyui-window" closed="true" modal="true" title="任务调度设置" resizable="false" minimizable="false"
			iconCls="icon-save" style="width: 650px; height: 450px; padding: 0px; background: #fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="padding:4px;background:#fff;border:1px solid #ccc;">
					<form action=""
					name="viewForm" id="viewForm" method="post">
					<input type="hidden" name="id" id="id"/>
					<input type="hidden" name="runWeek" id="runWeek"/>
					<table align="center" border="0" cellpadding="0" cellspacing="1"
						class="table_form">
						<tbody>
							<tr>
								<th><label for="name"><s:text
											name="调度名称" />:</label>
								</th>
								<td><input type="text" name="name"
									id="name" class="easyui-validatebox Itext" required="true" style="width:300px;"></input>
								</td>
							</tr>
							<tr>
								<th><label for="remark"><s:text
											name="调度描述" />:</label>
								</th>
								<td><textarea name="remark" id="remark" style="width:300px;height:80px;"></textarea>
								</td>
							</tr>
							<tr>
								<th><label for="taskId"><s:text
											name="调度任务" />:</label>
								</th>
								<td>
								<input id="taskId" name="taskId" style="width:160px" required="true"  class="Itext"/>
								</td>
							</tr>
							<tr>
								<th><label for="dateType"><s:text
											name="调度周期" />:</label>
								</th>
								<td>
								<select id="dateType" name="dateType" style="width:160px">
									<option value="Everyday">每天</option>
									<option value="Someday">每月</option>
									<option value="Date">指定日期</option>
									<option value="Week">每周</option>
									<option value="Hour">每隔*小时</option>
									<option value="Minute">每隔*分钟</option>
									<option value="Second">每隔*秒</option>
								</select>
								</td>
							</tr>
							<tr id="Month" style="display:none;">
									<th>执行日期:</th>
									<td>
										<table border="0">
											<tr>
												<td id="MonthMonth"><select id="runMonth"
													name="runMonth" class="Itext">
														<option value=""></option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
												</select> <font size="1">月 </font>
												</td>
												<td><select name="runDay" id="runDay"  class="Itext">
														<option value=""></option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														<option value="13">13</option>
														<option value="14">14</option>
														<option value="15">15</option>
														<option value="16">16</option>
														<option value="17">17</option>
														<option value="18">18</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="21">21</option>
														<option value="22">22</option>
														<option value="23">23</option>
														<option value="24">24</option>
														<option value="25">25</option>
														<option value="26">26</option>
														<option value="27">27</option>
														<option value="28">28</option>
														<option value="29">29</option>
														<option value="30">30</option>
														<option value="31">31</option>
												</select><font size="1">日</font>
												</td>
											</tr>
										</table></td>
								</tr>
								<tr id="Week" style="display:none;">
									<th>星期:</th>
									<td>
										<table border="0">
											<tr>
												<td><input name="runWeeks" id="sunday" type="checkbox"
													value="sunday"><font size="1">日</font></input>
												</td>
												<td><input name="runWeeks" id="monday" type="checkbox"
													value="monday"><font size="1">一</font></input>
												</td>
												<td><input name="runWeeks" id="tuesday" type="checkbox"
													value="tuesday"><font size="1">二</font></input>
												</td>
												<td><input name="runWeeks" id="wednesday"
													type="checkbox" value="wednesday"><font size="1">三</font></input>
												</td>
												<td><input name="runWeeks" id="thursday" type="checkbox"
													value="thursday"><font size="1">四</font></input>
												</td>
												<td><input name="runWeeks" id="friday" type="checkbox"
													value="friday"><font size="1">五</font></input>
												</td>
												<td><input name="runWeeks" id="saturday" type="checkbox"
													value="saturday"><font size="1">六</font></input>
												</td>
											</tr>
										</table></td>
								</tr>
								<tr>
									<th>执行时间:</th>
									<td><span id="Hour"><input type="text" class="easyui-numberbox" id="runHour" name="runHour" min="0" max="23" style="width:40px;"/>时：</span>
										<span id="Minute"><input type="text" class="easyui-numberbox" id="runMinute" name="runMinute" min="0" max="59" style="width:40px;"/>分：</span>
										<span id="Second" style="display:none;"><input type="text" class="easyui-numberbox" id="runSecond" name="runSecond" min="0" max="59" style="width:40px;"/>秒</span>
									<font color="red">*</font>(24小时制)</td>
								</tr>
								<tr>
								<th><label for="status"><s:text
											name="运行状态" />:</label>
								</th>
								<td>
								<select id="status" class="easyui-combobox" name="status" style="width:160px">
									<option value="0">停止</option>
									<option value="1">运行</option>
								</select>
								</td>
							</tr>
						</tbody>
					</table>
					</form>								
				</div>
				<div region="south" border="false" style="text-align:right;height:30px;line-height:30px;">
					<a id="stSave" class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="doSaveObj()"><s:text
						name="system.button.save.title" /></a> 
					<a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)" onclick="doCloseWindow()"><s:text
						name="system.button.cancel.title" /></a>
				</div>
			</div>	
		</div>	
	</div>
</body>
</html>