<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<script type="text/javascript">
	var attachId = "${param.attachId}";

	function getOptionAttach() {
		return {
			width : '100%',
			height : 'auto',
			fitColumns : true,
			nowrap : false,
			striped : true,
			url : '${ctx}/sys/attach/attach!queryList.action',
			queryParams:{"attachId":attachId},
			sortName : 'id',
			sortOrder : 'desc',
			remoteSort: true,
			idField : 'id',
			pageSize: 5,
			pageList: [5, 10, 20],
			frozenColumns : [ [ {
				field : 'ck',
				checkbox : true
			}  ] ],
			columns : [ [ {
				field : 'fileName',
				title : '<s:text name="system.attach.attachName.title"/>',
				width : 180,
				sortable : true
			}, {
				field : 'fileSize',
				title : '<s:text name="system.attach.attachSize.title"/>(B)',
				width : 140,
				sortable : true
			}, {
				field : 'createUser',
				title : '<s:text name="system.sysmng.user.createuser.title"/>',
				width : 120,
				sortable : true,
				formatter : function(value, rowData, rowIndex){	
                    return rowData["createUserCaption"];
				}
			}, {
				field : 'createTime',
				title :  '<s:text name="system.sysmng.user.createtime.title"/>',
				width : 150,
				sortable : true
			},{
				field : 'oper',
				title : '<s:text name="system.button.oper.title"/>',
				width : 120,
				align : 'left',
				formatter : operFormatterAttach
			} ] ],
			pagination : true,
			rownumbers : true,
			//if("${param.readOnly}" == true){
			toolbar : [ 
			{
				id : 'bt_batch_download',
				text : '<s:text name="批量下载"/>',
				iconCls : 'icon-download-ico',
				handler : function() {
					doDownloadAttach();
				}
			},'-'
			<c:if test="${param.readOnly=='false'}">
			,{
				id : 'bt_del',
				text : '<s:text name="system.button.batchDelete.title"/>',
				iconCls : 'icon-remove',
				handler : function() {
					doDeleteIdsAttach();
				}
			}
			</c:if>
			],
			onDblClickRow :function(rowIndex, rowData){
				var url = "${systemParam.virtualPicPath}"+ rowData.filePath;
				window.open(url);
			}
		};
	}
	function doDeleteIdsAttach() {
		var ids = [];
		var rows = $("#attachList").datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i].id);
		}
		if (ids != null && ids.length > 0) {
			doDeleteAttach(ids);
		} else {
			$.messager.alert(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.question"/>',
					'info');
		}
	}
	function operFormatterAttach(value, rowData, rowIndex) {
		return '<a href="'
		+ "${systemParam.virtualPicPath}"
		+ rowData.filePath
		+ '" target= "_blank") ><s:text name="system.button.view.title"/></a>&nbsp;'
		<c:if test="${param.readOnly=='false'}">
		+'<a href="javascript:void(0);" onclick="doDeleteAttach('
				+ rowData.id
				+ ')"><s:text name="system.button.delete.title"/></a>'
		</c:if>;
	}
	//删除附件文件
	function doDeleteAttach(ids) {
		$.messager
				.confirm(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'<s:text name="system.javascript.alertinfo.info"/>',
						function(r) {
							if (r) {
								var options = {
									url : '${ctx}/sys/attach/attach!delete.action',
									data : {
										"ids" : ids
									},
									traditional : true ,									
									success : function(data) {
										
										if (!data.msg) {
											if (window.doUpdateAttach) {
												$("#_file_box_").filebox("setValue","");
												var opts ={"attachId":"",
								           				 "id":"${param.bid}",
								           				 "key":"${param.key}",
								           				 "entityName":"${param.entityName}",
								           				 "dgId":"${param.dgId}",
								           				 "flag":"${param.flag}",
								           				 "readOnly" :"${param.readOnly}"
								           		};
												window.doUpdateAttach(opts);
											}
										}
										$("#attachList").datagrid('clearSelections');
										$("#attachList").datagrid("load");	
										
									}
								};
								fnFormAjaxWithJson(options);
							}
						});
	}
	function downloadFile(url) {   
		try{ 
			var elemIF = document.createElement("iframe");   
			elemIF.src = url;   
			elemIF.style.display = "none";   
			document.body.appendChild(elemIF);   
		} catch(e){ 
	
		} 
	}  
	
	function getDownloadFileUrl(fileName, filePath) {
		var downUrl = ["${ctx}/sys/attach/upload-file!downloadFile.action?filedataFileName="];
		downUrl.push(encodeURIComponent(fileName));
		downUrl.push("&filePath=");
		downUrl.push(encodeURIComponent(filePath));
		return downUrl.join("");
	}
	
	//批量下载文件
	function doDownloadAttach() {
		var rows = $("#attachList").datagrid('getSelections');
		if (rows && rows.length) {
			for(var i=0,len=rows.length;i<len;i++) {
				var rowData = rows[i];
				//var url = "/jwpf/upload/gs-attach/"+ rowData.filePath;
				//var url = "${systemParam.virtualPicPath}"+ rowData.filePath;
				var url = getDownloadFileUrl(rowData.fileName, rowData.filePath);
				downloadFile(url);
				//console.log(rowData.id);
				//window.open(url, "附件" + rowData.id);
			}
		} else {
			$.messager.alert(
					'<s:text name="system.javascript.alertinfo.title"/>',
					'<s:text name="system.javascript.alertinfo.operRecord"/>',
					'info');
		}
	}

	$(document)
			.ready(
					function() {
						$("#attachList").datagrid(getOptionAttach());
						$("#_file_box_").filebox({
							baseUrl:"${ctx}",
							language:"${locale}",
							value:attachId,
							onAfterUploadFinished:function(val, oldVal, data) {
								if(val != oldVal) {//更新业务表附件字段
									if (window.doUpdateAttach) {
										var opts ={"attachId":val,
						           				 "id":"${param.bid}",
						           				 "key":"${param.key}",
						           				 "entityName":"${param.entityName}",
						           				 "dgId":"${param.dgId}",
						           				 "flag":"${param.flag}",
						           				 "readOnly" :"${param.readOnly}"
						           					};
										window.doUpdateAttach(opts);
									}
								}
								setTimeout(function() {$("#attachList").datagrid("reload",{"attachId" : val});}, 500);
							}
						});
	});
</script>
<div class="easyui-layout" fit="true">
	<div region="center" border="false">
		<c:if test="${param.readOnly=='false' || param.readOnly=='onlyUpload'}">
		<div id="_file_box_"></div>
		</c:if>
		<table id="attachList">
		</table>
	</div>
</div>




