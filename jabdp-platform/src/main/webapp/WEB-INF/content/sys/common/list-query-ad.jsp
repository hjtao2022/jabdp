<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%-- //列表高级查询界面  --%>
<div class="container-fluid advance-query-form" style="margin-top:10px;">
	<form id="queryForm">
	<div class="row form-group">
		<div class="col-sm-12">
			<div class="input-group input-group-sm">
			  <span class="input-group-addon" id="sizing-addon3">自定义查询条件</span>
			  <select id="queryFieldSel"></select>
			</div>
		</div>
	</div>
	<%-- <div class="row form-group form-group-sm">
		<div class="col-sm-2 form-horizontal text-right">
			<label for="exampleInputFile" class="control-label">单号</label>
		</div>
		<div class="col-sm-8 form-inline">
			<div class="form-group">
			  <select id="queryFieldSel1" name="queryFieldSel1"></select>
			</div>
			<div class="form-group">
			  <input type="text" class="form-control" aria-label="...">
			</div>
			<div class="form-group">
			  <input type="number" class="form-control" aria-label="..." step="0.01" style="text-align:right;">
			</div>
		</div>
		<div class="col-sm-2">
			<button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		</div>
	</div>
	<div class="row form-group form-group-sm">
		<div class="col-sm-2 form-horizontal text-right">
			<label for="exampleInputFile" class="control-label">时间</label>
		</div>
		<div class="col-sm-8 form-inline">
			<div class="form-group">
			  <input type="text" class="form-control" aria-label="..."> --
			</div>
			<div class="form-group">
			  <input type="text" class="form-control" aria-label="...">
			</div>
		</div>
		<div class="col-sm-2">
			<button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		</div>
	</div>
	<div class="row form-group form-group-sm">
		<div class="col-sm-2 form-horizontal text-right">
			<label for="exampleInputFile" class="control-label">客户</label>
		</div>
		<div class="col-sm-8">
			<div class="form-group">
			 	<select id="queryFieldSel2" name="queryFieldSel2" class="form-control" ></select>
			</div>
		</div>
		<div class="col-sm-2">
			<button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		</div>
	</div>--%>
	</form>
</div>
<script type="text/javascript">
function _initAdQuery_(opt) {
	$ES.initComboBox("#queryFieldSel", {data:opt.data, multiple:true, width:'100%'
		,onSelect:function(data) {
			var strHtml = "";
			switch(data.editType) {
				case "TextBox":	
					if(["dtInteger","dtLong","dtDouble"].indexOf(data.dataType) >= 0) {
						if(data.precision) {
							data.step = Math.pow(10, -1*parseInt(data.precision));
						} else {
							data.step = "1";
						}
						strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
						 '<div class="col-sm-2 form-horizontal text-right">',
						 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
						 '</div>',
						 '<div class="col-sm-10 form-inline">',
						 '<div class="form-group">',
						 '<input type="number" class="form-control" placeholder="最小值" name="filter_GE%(dt)s_%(fieldKey)s" id="query_%(id)sStart" step="%(step)s">',
						 '</div>',
						 '<div class="form-group">',
						 ' -- <input type="number" class="form-control" placeholder="最大值" name="filter_LE%(dt)s_%(fieldKey)s" id="query_%(id)sEnd" step="%(step)s">',
						 '</div>',
						 '</div>',
						 '</div>'].join(""), data);
						$("#queryForm").append(strHtml);
					} else {
						strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
											 '<div class="col-sm-2 form-horizontal text-right">',
											 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
											 '</div>',
											 '<div class="col-sm-10 form-inline">',
											 '<div class="form-group">',
											 '<select class="form-control" name="query_%(id)sCondSel" id="query_%(id)sCondSel" forId="query_%(id)s"></select>',
											 '</div>',
											 '<div class="form-group">',
											 '<input type="text" class="form-control" placeholder="%(text)s" name="filter_%(oper)sS_%(fieldKey)s" forName="%(fieldKey)s" id="query_%(id)s" >',
											 '</div>',
											 '</div>',
											 '</div>'].join(""), data);
						$("#queryForm").append(strHtml);
						var fieldKey = sprintf("#query_%(id)sCondSel",data);
						$ES.initComboBox(fieldKey, {"data":queryMap.queryOperOpt.data,"width":"160px", "onSelect":function(result) {
							var oper = result.id;
							var forId = $(this).attr("forId");
							var fieldName = $("#"+forId).attr("forName");
							$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
						}, "value":"EQ"});
						if(data.oper=="USER") {
							$ES.setComboBoxVal(fieldKey, "LIKE");
							$(fieldKey).prop("disabled", true);
						}
					}
					break;
				case "DialogueWindow":
					strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
										 '<div class="col-sm-2 form-horizontal text-right">',
										 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
										 '</div>',
										 '<div class="col-sm-10 form-inline">',
										 '<div class="form-group">',
										 '<select class="form-control" name="query_%(id)sCondSel" id="query_%(id)sCondSel" forId="query_%(id)stext"></select>',
										 '</div>',
										 '<div class="form-group">',
										 '<input type="text" class="form-control" placeholder="%(text)s" name="filter_EQS_%(fieldKey)stext" forName="%(fieldKey)stext" id="query_%(id)stext" >',
										 '</div>',
										 '</div>',
										 '</div>'].join(""), data);
					$("#queryForm").append(strHtml);
					var fieldKey = sprintf("#query_%(id)sCondSel",data);
					$ES.initComboBox(fieldKey, {"data":queryMap.queryOperOpt.data,"width":"160px", "onSelect":function(result) {
						var oper = result.id;
						var forId = $(this).attr("forId");
						var fieldName = $("#"+forId).attr("forName");
						$("#"+forId).attr("name", "filter_" + oper + "S_" + fieldName);
					}, "value":"EQ"});
					break;
				case "DateBox":
					strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
					 '<div class="col-sm-2 form-horizontal text-right">',
					 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
					 '</div>',
					 '<div class="col-sm-10 form-inline">',
					 '<div class="form-group">',
					 '<input type="text" class="Wdate form-control input-sm input-sm-date" placeholder="起始值" name="filter_GE%(dt)s_%(fieldKey)s" id="query_%(id)sStart" ',
					 ' onclick="WdatePicker({dateFmt:\'%(fmt)s\',onpicked:function(dp) {if(!$dp.$(\'query_%(id)sEnd\').value)$dp.$(\'query_%(id)sEnd\').value=this.value;}})">',
					 '</div>',
					 '<div class="form-group">',
					 ' -- <input type="text" class="Wdate form-control input-sm input-sm-date" placeholder="结束值" name="filter_LE%(dt)s_%(fieldKey)s" id="query_%(id)sEnd"',
					 ' onclick="WdatePicker({dateFmt:\'%(fmt)s\'})" >',
					 '</div>',
					 '</div>',
					 '</div>'].join(""),data);
					$("#queryForm").append(strHtml);
					break;
				case "ComboBox":
				case "ComboBoxSearch":
				case "CheckBox":
				case "RadioBox":
				case "ComboCheckBox":
				case "ComboRadioBox":
					strHtml = sprintf(['<div id="_qf_%(id)s_" class="row form-group form-group-sm">',
										 '<div class="col-sm-2 form-horizontal text-right">',
										 '<label for="query_%(id)s" class="control-label">%(text)s</label>',
										 '</div>',
										 '<div class="col-sm-10 form-inline">',
										 '<div class="form-group">',
										 '<select class="form-control" name="filter_IN%(dt)s_%(fieldKey)s" id="query_%(id)s" multiple="true"><select>',
										 '</div>',
										 '</div>',
										 '</div>'].join(""), data);
					$("#queryForm").append(strHtml);
					var fieldKey = sprintf("#query_%(id)s", data);
					var selDataKey = sprintf("query_%(id)sJson", data);
					if(window[selDataKey]) {
						$ES.initComboBox(fieldKey, {"data":window[selDataKey],"width":"500px"});
					} else {
						var selDataParam = sprintf("query_%(id)sJsonParam", data);
						if(window[selDataParam]) {
							$ES.initComboBox(fieldKey, {"queryParams":window[selDataParam],"width":"500px"});
						}
					}
					break;
				case "ComboTree":
				case "DictTree":
					break;
				default:
					
					break;
					
			}
		}
		,onUnSelect:function(data) {
			$(sprintf('#_qf_%(id)s_', data)).remove();
		}});
}
var queryMap = {
		queryField:{},//新增查询字段
		queryCondList:[],//查询条件
		queryOperOpt:{
			"esType":"ComboBox"
			,"jwDataType":"S"
			,"data":[
				{"id":'EQ',"text":'等于(=)'},
				{"id":'NE',"text":'不等于(!=)'},
				{"id":'LIKE',"text":'模糊匹配(LIKE)'},
				{"id":'LIKEL',"text":'开始于(START WITH)'},
				{"id":'LIKER',"text":'结束于(END WITH)'}
			]
			,"locale":"zh-CN"
			,"placeholder":"查询匹配(Query Oper)"
			,"esFieldKey":"queryOper"
		}
	};

$(function() {
	if(window["_ad_query_opt_"]) {
		_initAdQuery_(window["_ad_query_opt_"]);
	}
});
</script>