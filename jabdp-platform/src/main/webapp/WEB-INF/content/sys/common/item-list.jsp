<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
	<style type="text/css">
		#_itemList_ ul {
			list-style:decimal;
		}
		#_itemList_ ul li {
			float:left;
			white-space:nowrap; 
			overflow:hidden; 
			display:inline;
			line-height:30px; 
			width:150px;
		}
	</style>
	<script type="text/javascript">
		function renderItemList__(jsonData){
			var data = jsonData["list"];	
			var mapping = jsonData["map"];
			var rowNumPerCol = parseInt("${param.rowCount}"||"10");
		  	var fields = [];
		  	fields.push("<ul>");
		    if(data && data.length) {
		    	for(var i=0,len=data.length;i<len;i++) {
		    		var frow = [];
		    		var fid = "_chk_item_" + i;
		    		frow.push("<li>");
		    		frow.push("<input type='checkbox' name='_chk_item_' id='");
		    		frow.push(fid);
		    		frow.push("' value='");
		    		frow.push(data[i]["key"]);
		    		frow.push("'/><label for='");
		    		frow.push(fid);
		    		frow.push("'><span>");
		    		frow.push(data[i]["caption"]);
		    		frow.push("</span></label>");
		    		frow.push("</li>");
		    		/*if(i%rowNumPerCol == 0) {
		    			fields.push("<ul>");
		    		}*/
		    		fields.push(frow.join(""));
		    		/*if(i%rowNumPerCol == (rowNumPerCol-1)) {
		    			fields.push("</ul>");
		    		}*/
		    	}
		    }
		    fields.push("</ul>");
		    $("#_itemList_").html(fields.join(""));
		}
		function _getItemList_(){
			var list = [];
			$("input[type=checkbox][name=_chk_item_]:checked", "#_itemList_").each(function() {
				list.push($(this).val());
			});
			if(list && list.length > 0) {
				return list;
			} else {
				$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.operRecord"/>', 'info');
				return false;
			}
		}
		$(function(){
			var param =$("#_hid_itemList_params_").val();
			var filterParam = $.parseJSON(param);
			var sqlKey = "${param.sqlKey}";
			var data = jwpf.getFilterData(sqlKey,filterParam);
			renderItemList__(data);
		});	
</script>
<div class="easyui-layout" fit="true" >
	<div region="center">
		<div id="_itemList_"></div>	
	</div>
	<form method="post" name="_hidItemListFrm_" id="_hidItemListFrm_" >
		<textarea style="display:none;" name="_hid_itemList_params_" id="_hid_itemList_params_">${param.filterParam}</textarea>
	</form>
</div>
