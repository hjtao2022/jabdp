<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ page import="com.qyxx.platform.sysmng.utils.Constants" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
	SystemParam systemParam = SpringContextHolder.getBean("systemParam");
	String systemLoginUrl = systemParam.getSystemLoginUrl();
	if(StringUtils.isNotBlank(systemLoginUrl)) {
		response.sendRedirect(request.getContextPath() + "/" + systemLoginUrl);
	}
%>
<!DOCTYPE html>
<html>
<head>
	<title><%=systemParam.getSystemTitle()%></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<meta http-equiv="Cache-Control" content="no-store"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta http-equiv="Expires" content="0"/>
	<link rel="icon" type="image/x-icon" href="${ctx}/favicon.ico"></link> 
	<link rel="shortcut icon" type="image/x-icon" href="${ctx}/favicon.ico"></link>
	<link href="${cssPath}/login/css/bootstrap.min.css" rel="stylesheet">
	<link href="${cssPath}/login/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="${cssPath}/login/css/animate.css" rel="stylesheet">
	<link href="${cssPath}/login/css/style.css" rel="stylesheet">
</head>
<%
	String username = "";
    Cookie[] cs = request.getCookies();
	if(null!=cs) {
		for(Cookie ck : cs) {
			if(Constants.USER_NAME_COOKIE.equals(ck.getName())) {
				username = java.net.URLDecoder.decode(ck.getValue(), Constants.DEFAULT_ENCODE);
				break;
			}
		}
	}
%>
<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
				<div>
					<div class="pull-left"><img src="${imgPath}/<%=systemParam.getSystemLogo()%>" style="height:40px;vertical-align:top;"/></div>
					<div><h1 class="font-bold"><%=systemParam.getSystemTitle()%></h1></div>
				</div>
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form id="loginForm" class="m-t" role="form" action="${ctx}/j_spring_security_check" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="用户名" id='j_username' name='j_username' 
	                            <s:if test="#parameters.error!=null">
								value='<%=session.getAttribute(WebAttributes.LAST_USERNAME)%>'</s:if>
								<s:else>value='<%=username%>'</s:else>	/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="密码" id='j_password' name='j_password' />
                        </div>
                        <div class="form-group">
                            <s:select labelposition="top" cssClass="form-control" theme="simple"
								name="request_locale" id="request_locale"
								listKey="value" listValue="displayName"
				      			list="%{@com.qyxx.platform.sysmng.dictmng.web.DefinitionCache@getSet('SYS_LOCALE_DICT','zh_CN')}">
				      		</s:select>
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                        <p class="text-muted text-center">
                        	<div class="user_login_msg" style="color:red;">
								<s:if test="#parameters.error!=null">
									<span><s:text name="system.login.error.userpass.title"/></span>&nbsp;&nbsp;
								</s:if>
								<s:if test="#parameters.timeout!=null">
									<span><s:text name="system.login.error.timeout.title"/></span>&nbsp;&nbsp;
								</s:if>
							</div>
                        </p>
                    </form>
                    <p class="m-t">
                        <small></small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
            	<ul class="m-b" style="list-style-type:none;">
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 建议使用IE9及以上、chrome、firefox浏览器</li>
                        <li><i class="fa fa-arrow-circle-o-right m-r-xs"></i> 建议最佳使用分辨率：1366*768及以上</li>
                </ul>
            </div>
            <div class="col-md-6 text-right">
               <small><%=systemParam.getSystemPowerBy()%> &copy; 2018 All Rights Reserved.</small>
            </div>
        </div>
    </div>
	<script src="${ctx}/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="${ctx}/js/layer/layer.js" type="text/javascript"></script>
	<script type="text/javascript" src="${ctx}/js/common/scripts/common-min.js"></script>
	<script type="text/javascript">
		//平台、设备和操作系统
		var system = {
			win : false,
			mac : false,
			xll : false
		};
		//检测平台
		var p = navigator.platform;
		system.win = p.indexOf('Win') == 0;
		system.mac = p.indexOf('Mac') == 0;
		system.x11 = (p == 'X11') || (p.indexOf('Linux') == 0);
		//跳转语句
		if(!(system.win||system.mac||system.xll)) {//转向手机登陆页面
			window.location.href = "${ctx}/login-mobile.action";
		}
		<s:if test="#parameters.timeout!=null">
			if(window.top!=window) {
				window.top.document.location.replace("${ctx}/login.action?timeout=true");
			}
		</s:if>

		/**
		 * 检查用户是否登录
		 * @param userName
		 * @param userPass
		 * @returns {boolean}
		 */
		function doCheckUserIsLogin(userName, userPass) {
			var flag = false;
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!checkUserIsLogin.action',
					data : {
						"userName" : userName,
						"userPass" : userPass
					},
					success : function(data) {
						var fm = data.msg;
						if(fm) {
							switch(fm.flag) {
								case "0":
									var cMsg = "<div>" + fm.msg + "。是否确认将其强制下线？</div>";
									layer.confirm('该用户已经登录', {
										  btn: ['确认','取消'], //按钮
										  title:"确认信息",
										  content:cMsg
										}, function(){
											doRemoveOnlineUser(userName);
										}, function(){
											//$("#j_username").val("");
											$("#j_password").val("");
											$("#j_username").focus();
									});
									break;
								case "2":
									$(".user_login_msg").text(fm.msg);
									break;
								default:
									flag = true;
							}
						}
					}
			};
			fnFormAjaxWithJson(options);
			return flag;
		}

		function doRemoveOnlineUser(userName) {
			var options = {
					async:false,
					url : '${ctx}/sys/account/online-user!removeLoginUser.action',
					data : {
						"userName" : userName
					},
					success : function(data) {
						$("#loginForm").unbind("submit");
						$("#loginForm").submit();
					}
			};
			fnFormAjaxWithJson(options);
		}
		
		$(document).ready(function() {
			<%
				AuthenticationException authException = (AuthenticationException)session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
				if(authException!=null && authException instanceof SessionAuthenticationException) {
					String errorMsg = authException.getMessage();
			%>
					$(".user_login_msg").text("<%=errorMsg%>");
			<%
				}
			%>
			// 登录
			$("#loginForm").submit(function() {
				var userName = $("#j_username").val();
				if(!userName) {
					$(".user_login_msg").text("请输入用户名");
					$("#j_username").focus();
					return false;
				}
				var pass = $("#j_password").val();
				if(!pass) {
					$(".user_login_msg").text("请输入登录密码");
					$("#j_password").focus();
					return false;
				}
				return doCheckUserIsLogin(userName, pass);
			});

			$("#j_username").focus();
		});
	</script>
</body>
</html>