<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>403 - 缺少权限</title>
	<script type="text/javascript">
		function doHome() {
			window.top.location.replace("${ctx}/");
		}

		function doLogout() {
			window.top.location.replace("${ctx}/j_spring_security_logout");
		}
	</script>
</head>
<body>
<div>
	<div><h1>你没有访问该页面的权限.</h1></div>
	<div><a href="javascript:void(0);" onclick="doHome();">返回首页</a> <a href="javascript:void(0);" onclick="doLogout();">退出登录</a></div>
</div>
</body>
</html>