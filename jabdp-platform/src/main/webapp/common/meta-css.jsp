<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta http-equiv="Cache-Control" content="no-store"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> -->
<link rel="icon" type="image/x-icon" href="${ctx}/favicon.ico"></link> 
<link rel="shortcut icon" type="image/x-icon" href="${ctx}/favicon.ico"></link>
<link rel="stylesheet" type="text/css" href="${ctx}/js/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/select2/css/select2.min.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/select2/css/select2-bootstrap.min.css"/>
<link rel="stylesheet" href="${ctx}/js/index/pace-theme-minimal.css">
<!-- easyui -->
<c:choose>
   <c:when test="${sessionScope.THEME_VERSION == 'bootstrap'}">  
<link rel="stylesheet" type="text/css" href="${ctx}/js/dist/easyui-bootstrap.min.css" colorTitle="bootstrap"/>
   </c:when>
   <c:otherwise> 
<link rel="stylesheet" type="text/css" href="${ctx}/js/dist/easyui-gray.min.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/dist/easyui-blue.min.css" colorTitle="blue"/>
   </c:otherwise>
</c:choose>
<%-- 
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.4/themes/gray/easyui.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.4/themes/default/easyui.css" colorTitle="blue"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/gray/easyui-add.css" colorTitle="gray"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/blue/easyui-add.css" colorTitle="blue"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui-1.4/themes/icon.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>
--%>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>

<%-- 
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-add.css"/>

<!-- jquery loadmask -->
<link href="${ctx}/js/loadmask/jquery.loadmask.css" rel="stylesheet" type="text/css"/>

<link href="${ctx}/js/common/scripts/focus_tips.css" rel="stylesheet" type="text/css"/>

<!-- ztree -->
<link href="${ctx}/js/ztree/blue/zTreeStyle.css" rel="stylesheet" type="text/css"/>
<link href="${ctx}/js/ztree/blue/icon.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/icons/icon-ztree.css"/>

<link href="${cssPath}/main.css" rel="stylesheet" type="text/css"/>

<!-- imgpreview -->
<link href="${ctx}/js/imgpreview/blue/jquery.gzoom.css" rel="stylesheet" type="text/css"/>

<!-- uploadify -->
<link href="${ctx}/js/uploadify/uploadify-3.1.css" rel="stylesheet"
	type="text/css" />

<!-- qtip2 -->
<link type="text/css" rel="stylesheet" href="${ctx}/js/qtip2/jquery.qtip.min.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/js/webuploader/webuploader.css">
<link rel="stylesheet" href="${ctx}/js/toastr/toastr.min.css">--%>

<link rel="stylesheet" type="text/css" href="${ctx}/js/dist/${jsVersion}/app.min.css"/>
<style type="text/css">
    #global-zeroclipboard-html-bridge {z-index: 99999999 !important;}
    .numberbox .textbox-text {text-align: right;}
    .table_form th{background:#fff !important;font-weight:700 !important;}
    .Itext,.Idate, textarea, .datagrid-view .Itext{border: #95B8E7 1px solid;margin: 0;padding: 1px;
	  vertical-align: top;outline-style: none;resize: both;-moz-border-radius: 5px 5px 5px 5px;
	  -webkit-border-radius: 5px 5px 5px 5px;border-radius: 5px 5px 5px 5px;font-size:12px !important;}
	.input-xs {height: 22px !important; padding: 2px 5px;font-size: 12px;line-height: 1.5;border-radius: 3px;}
	.input-sm-date {height:30px !important; border: 1px solid #ccc !important; padding: 5px 10px !important;font-size:12px;border-radius:4px !important;}
	.datagrid-row-selected {background: #ffe48d;color:#000;}	
	.simple-query-form .form-group {margin-bottom:2px;}
	/*::-webkit-input-placeholder{ color:#F8F8FF;font-size:12px;}
	:-moz-placeholder{color:#F8F8FF;font-size:12px;}
	::moz-placeholder{color:#F8F8FF;font-size:12px;}
	:-ms-input-placeholder{color:#F8F8FF;font-size:12px;}*/
	.form-control{color:#000;}
	.select2-container--bootstrap .select2-selection--single .select2-selection__rendered{color:#000;}
	.select2-container--bootstrap .select2-selection--multiple .select2-selection__choice{color:#000;}
<%--	
/*bootstrap兼容问题和easyui的bug*/
/*.panel-header, .panel-body {border-width: 0px;}
.datagrid,.combo-p{border:solid 1px #D4D4D4;}
.datagrid *{-webkit-box-sizing: content-box;-moz-box-sizing: content-box;box-sizing: content-box;}
.easyui-layout *{-webkit-box-sizing: content-box;-moz-box-sizing: content-box;box-sizing: content-box;}*/--%>
</style>
<!--[if lt IE 9]>
<script src="${ctx}/js/dist/${jsVersion}/html5shiv.min.js"></script>
<script src="${ctx}/js/dist/${jsVersion}/respond.min.js"></script>
<![endif]-->