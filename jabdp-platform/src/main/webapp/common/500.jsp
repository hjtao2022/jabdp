<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.slf4j.Logger,org.slf4j.LoggerFactory" isErrorPage="true" %>
<%@ page import="java.io.StringWriter,java.io.PrintWriter" %>
<%@ page import="com.qyxx.platform.sysmng.exception.GsException" %>

<%
	Throwable ex = exception;
	if (request.getAttribute("javax.servlet.error.exception") != null)
		ex = (Throwable) request.getAttribute("javax.servlet.error.exception");
	String msg = "";
	if(ex!=null) {
		if(ex instanceof GsException) {
			
		} else {
			//记录日志
			Logger logger = LoggerFactory.getLogger("500.jsp");
			logger.error(ex.getMessage(), ex);	
		}
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		msg = sw.toString();
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>500 - 系统内部错误</title>
	<script type="text/javascript">
		function doHome() {
			window.top.location.replace("${ctx}/");
		}
	</script>
</head>
<body>
<div><h1>系统发生内部错误.</h1></div>
<div style="display:none;"><pre><%=msg%></pre></div>
<div><a href="javascript:void(0);" onclick="doHome();">返回首页</a></div>
</body>
</html>
