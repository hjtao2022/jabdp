<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
		var setting = {
			callback : {
				onClick : onTreeClick
			}
		};
		var zNodes =[
			<#if mdsList?? && mdsList?size gt 0>
				<#list mdsList as mds>
					{id:"${mds.id}", name:"${mds.name}", type:"${mds.type}", key:"${mds.key}", iconSkin:"${(mds.iconSkin)!"iconGroup"}"	
							<#assign mdList = mds.module/>
                            <#if mdList?? && mdList?size gt 0>
                            	,children: [
                                <#list mdList as md>
	                             	{id:"${md.id}", name:"${md.name}", type:"${md.type}", key:"${md.key}", 
	                             	 iconSkin:"${(md.iconSkin)!"iconMenu"}",entityName:"${md.entityName}"}<#if md_has_next>,</#if>
                                </#list>
                                ]
                            </#if>
					}<#if mds_has_next>,</#if>
				</#list>
			</#if>	
		];
		
		function onTreeClick(event, treeId, treeNode, clickFlag) {
			var entityName = treeNode.entityName;
			if(entityName) {
				var strUrl = "${r"${ctx}"}/gs/gs-mng.action?entityName=" + entityName;
				$("#ifr_dicts").attr("src", strUrl);
			}
		}

		$(document).ready(function(){
			var ztreeObj = $.fn.zTree.init($("#dictsTree"), setting, zNodes);
			ztreeObj.expandAll(true);
		});            
</script>
</head>            
<body class="easyui-layout" fit="true">
          <div region="west" title="<s:text name='system.sysmng.dicts.title'/>" border="false"
                 split="true" style="width:215px;padding:0px;"
                 iconCls="icon-search">
               	<div><ul id="dictsTree" class="ztree"></ul></div>
          </div>
          <div region="center" title="" border="false">
                <iframe  id="ifr_dicts" scrolling="auto" frameborder="0" src=""  
                	style="width:100%;height:100%;overflow:hidden;visibility:visible;">
                </iframe>
          </div>
</body>
</html>