<?xml version="1.0" encoding="utf-8"?>  
<!DOCTYPE hibernate-mapping PUBLIC  
        "-//Hibernate/Hibernate Mapping DTD 3.0//EN"  
        "http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd">  
<hibernate-mapping>  
    <class entity-name="${entity.entityName}" table="${entity.tableName}" dynamic-update="true" dynamic-insert="true">
        <#if entity.teAttr?exists>  
            <id name="id" column="ID" type="java.lang.Long">  <#-- 主键  -->
            	<generator class="native">
            		<param name="sequence">SEQ_${entity.tableName}_ID</param>
            	</generator>  
        	</id>  
        	<version name="version"  column="VERSION" type="java.lang.Long" />	<#-- 乐观锁版本号 -->
        	<property name="createUser"  column="CREATE_USER" type="java.lang.Long" update="false"/>	<#-- 创建者 -->
        	<property name="createTime"  column="CREATE_TIME" type="java.util.Date" update="false"/>	<#-- 创建时间 -->
        	<property name="lastUpdateUser"  column="LAST_UPDATE_USER" type="java.lang.Long" insert="false"/>	<#-- 最后修改者 -->
        	<property name="lastUpdateTime"  column="LAST_UPDATE_TIME" type="java.util.Date" insert="false"/>	<#-- 最后修改时间 -->
        	<property name="status"  column="STATUS" type="java.lang.String" length="50"/>  <#-- 状态：初始状态->草稿->审批中->审批结束  -->
        	<property name="sortOrderNo"  column="SORT_ORDER_NO" type="java.lang.Long" />  <#-- 排序编号，用于子表数据排序 -->
        	<property name="atmId"  column="ATM_ID" type="java.lang.Long" />  <#-- 附件编号，对应于系统附件表，一个编号对应于多个附件 -->
        	<property name="flowInsId"  column="FLOW_INS_ID" type="java.lang.String" length="100"/>  <#-- 流程实例ID -->
			<property name="cancelReason"  column="CANCEL_REASON" type="java.lang.String" length="500"/>  <#-- 作废原因 -->
			<#if entity.listType == "TreeGrid">
			<property name="autoCode"  column="AUTO_CODE" type="java.lang.String" length="200"/>  <#-- 树形结构编码 -->
			<property name="autoParentCode"  column="AUTO_PARENT_CODE" type="java.lang.String" length="200"/>  <#-- 树形结构父级编码  -->
			</#if>
			<#if (entity.isCommon)!false>
			<property name="entityName"  column="ENTITY_NAME" type="java.lang.String" length="200"/>  <#-- 公用表关联实体名 -->
			<property name="entityId"  column="ENTITY_ID" type="java.lang.Long"/>  <#-- 公用表关联实体ID -->
			</#if>
			<#if entity.formType == "stock">
			<property name="stockRatio"  column="STOCK_RATIO" type="java.lang.Long"/>  <#-- 库存系数，为1、-1值 -->
			</#if>
			<#-- 关联表，存存该条记录所属的模块 -->
			<#if entity.isSubEntity == false>
			<property name="relevanceModule" column="RELEVANCE_MODULE" type="java.lang.String" length="100"/>  <#-- 关联模块，存放从哪个模块保存的数据 -->
			</#if>
            <#list entity.teAttr as property>  
            	<#if (((property.entityName)!"") == "") && !(property.isSystemAttr)> <#-- 非系统字段及关联表字段 -->
	                <property name="${property.name}"  column="${property.columnName}"
	                <#--if property.isNotNull>not-null="true"</#if -->
	                <#--if property.isUnique>unique="true"</#if -->
	                <#if property.type=="dtString">
	                	type="java.lang.String"
	                    <#if property.size?exists>
	                    	<#if property.size gt 4000>
	                    	length="4000" 
	                    	<#else>
	                    	length="${property.size?c}" 
	                    	</#if>
	                    </#if>     
	                <#elseif property.type=="dtLong" || property.type=="dtInteger"
	                    || property.type=="dtShort">
	                    type="java.lang.Long"
	                        <#--if property.size?exists>
	                        	<#if property.size gt 38>
	                        	precision="38"
	                        	<#else>
	                        	precision="${property.size?c}"
	                        	</#if>
	                        </#if>
	                        scale="0" --> 
	                <#elseif property.type=="dtDateTime">
	                 		type="java.util.Date"
	                <#elseif property.type=="dtFloat" || property.type=="dtDouble"  >
	                 		type="java.math.BigDecimal" 
	                 		<#if property.size?exists>
	                        	<#if property.size gt 30>
	                        	precision="30"
	                        	<#else>
	                        	precision="${property.size?c}"
	                        	</#if>
	                        </#if>
	                        <#if property.scale?exists>
	                        	<#if property.scale gt 8>
	                        	scale="8"
	                        	<#else>
	                        	scale="${property.scale?c}"
	                        	</#if>
	                        <#else>
	                        	scale="2"
	                        </#if>
	                <#elseif property.type=="dtText">        
	                		type="text"
	                <#elseif property.type=="dtImage">
	                		type="text"		
	                </#if>
	                /> 
                </#if>
            </#list> 
            <#if entity.isSubEntity> <#-- 子表外键  -->
	            <many-to-one name="${entity.refEntityName}" entity-name="${entity.masterEntityName}" not-null="false" fetch="select">  
			         <column name="MASTER_ID"></column>
			    </many-to-one>  
            <#else>
            	<property name="dataShareLog"  column="DATA_SHARE_LOG" type="text" />  <#-- 数据共享日志 -->
            	<#list entity.subEntitys as property>  
	                <bag name="${property.refSubEntityName}"  
			             table="${property.tableName}"  
			             lazy="true"  
			             inverse="true"  
			             cascade="delete"  
			        >  
			            <key>  
			                <column name="MASTER_ID"/>  
			            </key>  
			            <one-to-many entity-name="${property.entityName}"/>  
			        </bag>  
	            </#list>
            </#if>
            
        </#if>  
    </class>  
</hibernate-mapping>