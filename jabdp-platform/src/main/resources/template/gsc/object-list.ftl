<#assign formList=root.dataSource.formList/>
<#list formList as form>
	<#if form.isMaster>
		<#assign masterForm=form />
		<#if (masterForm.listType!"") == "FullCalendar">
			<#include "list-calendar.ftl"/><#-- 日历展示 -->
		<#else>
			<#include "list-default.ftl"/><#-- 默认列表展示 -->
		</#if>
		<#break/>
	</#if>
</#list>