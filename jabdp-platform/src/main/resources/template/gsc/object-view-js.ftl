<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<#import "common/field.ftl" as fieldftl>
<#--include "common/field.ftl"-->
<#assign entityName=root.entityName />
<#assign titleName=root.moduleProperties.caption/>
<#assign moduleKey=root.moduleProperties.key/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign formList=root.dataSource.formList/>
<#assign tabsList=root.dataSource.tabsList/>
<#assign flowList= root.flowList />
<#assign isSelfApprove=true />
<#if flowList??&&flowList?size gt 0>
<#assign isSelfApprove=false />
</#if>
<#assign dynamicShowTable = false /><#-- 控制是否动态显示子表 -->
<#-- 关联表表 -->
<#assign realEntityName = entityName />
<#if root.moduleProperties.relevanceModule??>
<#assign relevanceModule=root.moduleProperties.relevanceModule/>
<#list formList as form>
<#if form.isMaster>
	<#assign realEntityName = form.entityName />
	<#break/>
</#if>
</#list>
<#else>
<#assign relevanceModule=""/>
</#if>
<#list formList as form><#-- 用于将隐藏子表数量去掉 -->
	<#if form.isMaster>
		<#assign dynamicShowTable = (form.dynamicShow)!false />
	</#if>
</#list>
