<#list appModules as appModule>
import ${appModule.key?lower_case}List from  './pages/gs/${appModule.key?lower_case}/${appModule.key?lower_case}-list.vue'
</#list>

/*global Vue*/
import Router from 'vue-router'

import   Index  from   '@/pages/index'
import   Login  from   '@/pages/login'

Vue.use(Router)

export const router = new Router({
   routes:[
    { path: '/', redirect: '/index' },
    { path: '/index', component: index },
    { path: '/login', component: login },
    <#list appModules as appModule>
    ,{ path: '/${appModule.key?lower_case}-list', component: ${appModule.key?lower_case}List}
    </#list>    
    ]
})