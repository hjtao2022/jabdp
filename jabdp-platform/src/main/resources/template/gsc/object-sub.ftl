<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta.jsp" %>
<#import "common/field.ftl" as fieldftl>
<#assign entityName=root.entityName />
<script type="text/javascript">
	var currentRowIndex=0; 
    $(document).ready(function() {
         comboboxInit();
         setTimeout(viewInfor,2000);
         jwpf.bindKeydownOnForm("childViewForm");
         jwpf.bindHotKeysOnViewPage();
    });
   
   function doPrepareForm(jsonData) {
		<@fieldftl.prepareForm form=root />
   }
   
   function viewInfor() {                                                                       
          var json2fromdata = parent.getObject(${r"${param.RowIndex}"},"${r"${param.ChildTableIndex}"}");
          doPrepareForm(json2fromdata);
          $('#childViewForm').form('load',json2fromdata);
   }

   function viewInfor2(currentRowIndex) {   
       var json2fromdata = parent.getObject(currentRowIndex,"${r"${param.ChildTableIndex}"}");
       doPrepareForm(json2fromdata);
       $('#childViewForm').form('load',json2fromdata);
	}
   
   function saveObj() {
	   var jsons = formToJSON('childViewForm');
	   parent.setObject("${r"${param.RowIndex}"}","${r"${param.ChildTableIndex}"}",jsons);
   }
   
   function clearForm() {
	   $("#childViewForm").form("clear");
   }
   
    //第一条
   function queryFirst() {
	   currentRowIndex=0;
	   clearForm();
	   viewInfor2(currentRowIndex);
   }
   
   //上一条
   function queryPrev() {
	   if(currentRowIndex>0){
		   currentRowIndex-=1;
		   clearForm();
		   viewInfor2(currentRowIndex);
	   }
   }
   
  //下一条
   function queryNext() {
	   var len = parent.getAllrowsLength("${r"${param.ChildTableIndex}"}");
	   if(currentRowIndex<len){
		   currentRowIndex+=1;
		   clearForm();
		   viewInfor2(currentRowIndex);
	   }
   }
   
   //最后一条
   function queryLast() {
	  var len = parent.getAllrowsLength("${r"${param.ChildTableIndex}"}");
	   clearForm();
	   viewInfor2(len);
   } 
   
   
 function comboboxInit() {
		<#assign fields=root.fieldList!>
		<#if fields??&&fields?size gt 0>
			  <#list fields as field>
			      <@fieldftl.initControlsData field=field type="query" prefix="" entityName=root.entityName/>
			  </#list>
		</#if>
 }
</script>
</head>
<body class="easyui-layout" fit="true">
<div region="center">
     <form
                        action=""
                        name="childViewForm" id="childViewForm">
                        <div class="datagrid-toolbar">
                            <a id="tclose" href="javascript:void(0);" onclick="parent.closeDetailDialog()"
                            class="easyui-linkbutton" plain="true" iconCls="icon-cancel"><s:text name="system.button.close.title"/>(Q)</a> 
                            <a id="tsave" href="javascript:void(0);" onclick="saveObj()" class="easyui-linkbutton" plain="true" iconCls="icon-save"><s:text name="system.button.save.title"/>(S)</a>
                            <a id="tfirst" href="javascript:void(0);" onclick="queryFirst()"
                            class="easyui-linkbutton" plain="true" iconCls="icon-first"><s:text name="system.button.first.title"/>(F)</a>
                            <a id="tprev" href="javascript:void(0);" onclick="queryPrev()"
                            class="easyui-linkbutton" plain="true" iconCls="icon-prev"><s:text name="system.button.prev.title"/>(P)</a>
                            <a id="tnext" href="javascript:void(0);" onclick="queryNext()"
                            class="easyui-linkbutton" plain="true" iconCls="icon-next"><s:text name="system.button.next.title"/>(N)</a>
                            <a id="tlast" href="javascript:void(0);" onclick="queryLast()"
                            class="easyui-linkbutton" plain="true" iconCls="icon-last"><s:text name="system.button.last.title"/>(L)</a>
                        </div>
                        <div style="display:none;">
                      			<#list root.fieldList as field>
                      				<#if !field.editProperties.visible>
                      					<@fieldftl.getFiledView field=field form=root/>
                      				</#if>
                      			</#list>
                      	</div>
                       <#--GsParseUtils类里generateJsp()方法穿过来的root就是一个form-->
				            <#assign filedsList=root.fieldList!>
                            <#assign column=root.cols!>
                            <#assign columns=column!>
                            <#assign filedsListSize=filedsList?size/>
                            <#assign count=0>    <#-- ------计数器，用来显示集合索引-------- -->
                            <#assign fields=root.fieldList/>
                            <#if fields??&&fields?size gt 0>
                             <table border="0" cellpadding="0" cellspacing="1" class="table_form">
	                        <tbody>
                            <tr>
                                <#list fields as field>
                                    <#assign count=count+1>
                                     <#if columns-field.editProperties.cols lt 0><#--单列大于了定义的全局列数是无效的-->
                                        xml里${field.caption}列数定义错误!
                                        <#else>

                                            <#assign column=column-field.editProperties.cols>
                                            <#if  column gt 0>  <#-- 判断行是不是满了-->
                                                <th><label for="${field.key}">
                                                    <s:text
                                                            name="${field.i18nKey!field.caption}"/>
                                                    :</label>
                                                </th>
                                                <td colspan="${field.editProperties.cols*2-1}">
                                                   <@fieldftl.getFiledView field=field form=root/>
                                                </td>
                                             <#if filedsListSize == count>
                                             <#if column gt 0>
                                                    <th>
                                                     &nbsp;
                                                    </th>
                                                    <td colspan="${column*2-1}">&nbsp;</td>
                                                </tr>
                                             </#if>

                                              </#if>
                                               <#elseif column==0>     <#-- 一行结束-->
                                                    <th><label for="${field.key}">
                                                        <s:text
                                                                name="${field.i18nKey!field.caption}"/>
                                                        :</label>
                                                    </th>
                                                    <td colspan="${field.editProperties.cols*2-1}">
                                                         <@fieldftl.getFiledView field=field form=root/>
                                                    </td>

                                              <#assign column=root.cols!><#-- 一行结束后列数重新开始-->
                                                </tr>
                                                    <#if filedsListSize gt count>
                                                    <tr>
                                                    </#if>
                                                <#else>
                                            <#if column+field.editProperties.cols gt 0>

                                                    <th>&nbsp;
                                                    </th>
                                                    <td colspan="${(column+field.editProperties.cols)*2-1}">&nbsp;</td>
                                             </#if>
                                              <#assign column=root.cols!>
                                            </tr>
                                              <tr>
                                           <th><label for="${field.key}">
                                                    <s:text
                                                            name="${field.i18nKey!field.caption}"/>
                                                    :</label>
                                                </th>
                                                <td colspan="${field.editProperties.cols*2-1}">
                                                    <@fieldftl.getFiledView field=field form=root/>
                                                </td>
                                                  <#assign column=column-field.editProperties.cols>
                                                     <#if filedsListSize == count>
                                             <#if column gt 0>
                                                    <th>&nbsp;
                                                    </th>
                                                    <td colspan="${column*2-1}">&nbsp;</td>

                                             </#if>
                                                    </tr>
                                                  </tbody>
                                                 </table>
                                              </#if>
                                            </#if>
                                    </#if>
                                </#list>
                            </#if>
	       
     </form>
  </div>
</body>
</html>
