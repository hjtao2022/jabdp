CREATE TABLE SYS_ATTACH(
	ID bigint auto_increment NOT NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE SYS_DESKTOP(
	ID bigint auto_increment NOT NULL,
	TITLE varchar(200) NULL,
	KEY_VAL varchar(100) NULL,
	TYPE varchar(100) NULL,
	LAYOUT_TYPE varchar(100) NULL,
	LAYOUT_VAL varchar(2000) NULL,
	LEVEL_VAL decimal(2, 0) NULL,
	DISPLAY_NUM decimal(5, 0) NULL,
	MODULE_URL varchar(2000) NULL,
	DATA_URL varchar(2000) NULL,
	SOURCE_TYPE varchar(100) NULL,
	SOURCE text NULL,
	HEIGHT decimal(5, 0) NULL,
	FONT_NUM decimal(5, 0) NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	PARENT_ID bigint NULL,
 	CONSTRAINT PK_SYS_DESKTOP PRIMARY KEY (ID)
);

CREATE TABLE SYS_COMMON_TYPE(
	ID bigint auto_increment NOT NULL,
	NAME varchar(500) NULL,
	PID bigint NULL,
	TYPE_VAL varchar(200) NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
 	CONSTRAINT PK_SYS_COMMON_TYPE PRIMARY KEY (ID)
);

CREATE TABLE SYS_ROLE(
	ID bigint auto_increment NOT NULL,
	ORGANIZATION_ID bigint NULL,
	ROLE_NAME varchar(100) NOT NULL,
	ROLE_DESC varchar(500) NULL,
	ROLE_STATUS varchar(1) NULL,
	CREATE_TIME datetime NULL,
	CREATE_USER bigint NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
 	CONSTRAINT PK_SYS_ROLE PRIMARY KEY (ID)
);

CREATE TABLE SYS_RESOURCE(
	ID bigint auto_increment NOT NULL,
	RESOURCE_NAME varchar(100) NOT NULL,
	RESOURCE_SIGN varchar(100) NOT NULL,
	RESOURCE_TYPE varchar(2) NOT NULL,
	PARENT_ID bigint NULL,
	RESOURCE_ICON varchar(50) NULL,
	OPEN_TYPE varchar(1) NULL,
	RESOURCE_STATUS varchar(1) NULL,
	RESOURCE_NO decimal(10, 0) NULL,
	RESOURCE_URL varchar(500) NULL,
	RESOURCE_LEVEL varchar(2) NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	REMARK varchar(500) NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	LOG_RULE varchar(1000) NULL,
 	CONSTRAINT PK_SYS_RESOURCE PRIMARY KEY (ID)
);

CREATE TABLE SYS_PROCESS_INFO(
	ID bigint auto_increment NOT NULL,
	PROCESS_INSTANCE_ID varchar(100) NULL,
	EXECUTION_ID varchar(100) NULL,
	USER_ID bigint NULL,
	DEAL_NAME varchar(100) NULL,
	IS_APPROVE varchar(50) NULL,
	REASON varchar(2000) NULL,
	DEAL_TIME datetime NULL,
	BUSINESS_ID bigint NULL,
	BUSINESS_NAME varchar(200) NULL,
	TASK_ID varchar(100) NULL,
	TASK_NAME varchar(1000) NULL,
	ACTIVITI_ID varchar(200) NULL,
	COUNT_VAL bigint NULL,
 	CONSTRAINT PK_SYS_PROCESS_INFO PRIMARY KEY (ID)
);

CREATE TABLE SYS_ORGANIZATION(
	ID bigint auto_increment NOT NULL,
	PARENT_ID bigint NULL,
	ORGANIZATION_NAME varchar(100) NOT NULL,
	ORGANIZATION_NO decimal(10, 0) NULL,
	ORGANIZATION_DESC varchar(500) NULL,
	CREATE_TIME datetime NULL,
	CREATE_USER bigint NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	ORGANIZATION_CODE varchar(2000) NULL,
	PARENT_ORGANIZATION_CODE varchar(2000) NULL,
 	CONSTRAINT PK_SYS_ORGANIZATION PRIMARY KEY(ID)
);

CREATE TABLE SYS_NOTICE(
	ID bigint auto_increment NOT NULL,
	TITLE varchar(200) NULL,
	KEY_VAL varchar(1000) NULL,
	CONTENT_VAL text NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	STATUS varchar(50) NULL,
	TYPE bigint NULL,
	ATM_ID bigint NULL,
	STYLE varchar(50) NULL,
	LEVEL_VAL varchar(50) NULL,
	MODULE_KEY varchar(200) NULL,
    MODULE_DATA_ID varchar(100) NULL,
    MODULE_FIELDS varchar(2000) NULL,
 	CONSTRAINT PK_SYS_NOTICE PRIMARY KEY(ID)
);

CREATE TABLE SYS_NEWS(
	ID bigint auto_increment NOT NULL,
	TITLE varchar(200) NULL,
	KEY_VAL varchar(1000) NULL,
	CONTENT_VAL text NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	STATUS varchar(50) NULL,
	TYPE bigint NULL,
	ATM_ID bigint NULL,
	CLICK_COUNT bigint NULL,
 	CONSTRAINT PK_SYS_NEWS PRIMARY KEY (ID)
);

CREATE TABLE SYS_LOG(
	ID bigint auto_increment NOT NULL,
	OPERATION_URL varchar(500) NULL,
	OPERATION_METHOD varchar(100) NULL,
	OPERATION_NAME varchar(100) NULL,
	OPERATION_DESC varchar(1000) NULL,
	OPERATION_TIME datetime NOT NULL,
	OPERATION_IP varchar(100) NULL,
	OPERATION_MACHINE_NAME varchar(100) NULL,
	REAMRK varchar(500) NULL,
	LOGIN_NAME varchar(50) NULL,
	REAL_NAME varchar(50) NULL,
	ORG_NAME varchar(100) NULL,
 	CONSTRAINT PK_SYS_LOG PRIMARY KEY(ID)
);

CREATE TABLE SYS_EXCEPTION_LOG(
	ID bigint auto_increment NOT NULL,
	THREAD_NAME varchar(1000) NULL,
	LOGGER_NAME varchar(1000) NULL,
	LEVEL varchar(100) NULL,
	MESSAGE text NULL,
	LOG_TIME datetime NOT NULL,
	METHOD_NAME varchar(4000) NULL,
	CLASS_NAME varchar(4000) NULL,
 	CONSTRAINT PK_SYS_EXCEPTION_LOG PRIMARY KEY(ID)
);

CREATE TABLE SYS_DICTIONARY_TYPE(
	ID bigint auto_increment NOT NULL,
	DICT_TYPE_NAME varchar(100) NOT NULL,
	DICT_TYPE_DESC varchar(100) NULL,
	TYPE varchar(1) NOT NULL,
	DICT_TYPE_KEY varchar(100) NULL,
	CREATE_TIME datetime NULL,
	CREATE_USER bigint NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
 	CONSTRAINT PK_SYS_DICTIONARY_TYPE PRIMARY KEY (ID)
);

CREATE TABLE SYS_DICTIONARY(
	ID bigint auto_increment NOT NULL,
	DICT_TYPE_ID bigint NOT NULL,
	DICT_NAME varchar(100) NOT NULL,
	DICT_VALUE varchar(100) NOT NULL,
	STATUS varchar(1) NULL,
	DICT_DESC varchar(500) NULL,
	DICT_LOCALE varchar(50) NULL,
	CREATE_TIME datetime NULL,
	CREATE_USER bigint NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
 	CONSTRAINT PK_SYS_DICTIONARY PRIMARY KEY(ID)
);

CREATE TABLE SYS_RESOURCE_TO_ROLE(
	ROLE_ID bigint NOT NULL,
	RESOURCE_ID bigint NOT NULL
);

CREATE TABLE SYS_RESOURCE_LOCALE(
	ID bigint auto_increment NOT NULL,
	RESOURCE_ID bigint NULL,
	RESOURCE_NAME varchar(100) NULL,
	RESOURCE_LOCALE varchar(50) NULL,
 	CONSTRAINT PK_SYS_RESOURCE_LOCALE PRIMARY KEY (ID)
);

CREATE TABLE SYS_ATTACH_DETAIL(
	ID bigint auto_increment NOT NULL,
	FILE_NAME varchar(500) NULL,
	FILE_SIZE bigint NOT NULL,
	FILE_PATH varchar(1000) NULL,
	ATTACH_ID bigint NOT NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE SYS_USER(
	ID bigint auto_increment NOT NULL,
	LOGIN_NAME varchar(50) NOT NULL,
	PASSWORD varchar(50) NOT NULL,
	ORGANIZATION_ID bigint NULL,
	MANAGER_ID bigint NULL,
	REMOTE_LOGIN varchar(1) NULL,
	REAL_NAME varchar(50) NOT NULL,
	SEX varchar(1) NULL,
	PHONE varchar(50) NULL,
	MOBILE_PHONE varchar(50) NULL,
	FAX varchar(50) NULL,
	EMAIL varchar(100) NULL,
	ADDRESS varchar(500) NULL,
	STATUS varchar(1) NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	ALLOWERROR decimal(3, 0) NULL,
	ALLEDYERROR decimal(3, 0) NULL,
	REMARK varchar(1000) NULL,
	IS_SUPER_ADMIN varchar(1) NULL,
	EMPLOYEE_ID varchar(100) NULL,
	NICKNAME varchar(100) NULL,
 	CONSTRAINT PK_SYS_USER PRIMARY KEY (ID)
);

CREATE TABLE SYS_DESKTOP_TO_ROLE(
	DESKTOP_ID bigint NOT NULL,
	ROLE_ID bigint NOT NULL
);

CREATE TABLE SYS_USER_TO_ROLE(
	USER_ID bigint NOT NULL,
	ROLE_ID bigint NOT NULL
);

CREATE TABLE SYS_ONLINE_USER(
	ID bigint auto_increment NOT NULL,
	LOGIN_USERID bigint NOT NULL,
	LOGIN_USERNAME varchar(50) NOT NULL,
	LOGIN_IP varchar(100) NULL,
	LOGIN_MACHINE varchar(100) NULL,
	LOGIN_TIME datetime NOT NULL,
	SESSION_ID varchar(100) NULL,
 	CONSTRAINT PK_SYS_ONLINE_USER PRIMARY KEY (ID)
);

CREATE TABLE SYS_NOTICE_TO_USER(
	ID bigint auto_increment NOT NULL,
	NOTICE_ID bigint NULL,
	USER_ID bigint NULL,
	READ_TIME datetime NULL,
	STATUS varchar(50) NULL,
 	CONSTRAINT PK_SYS_NOTICE_TO_USER PRIMARY KEY (ID)
);

CREATE TABLE SYS_DESKTOP_TO_USER(
	ID bigint auto_increment NOT NULL,
	DESKTOP_ID bigint NULL,
	TITLE varchar(200) NULL,
	USER_ID bigint NULL,
	LAYOUT_TYPE varchar(100) NULL,
	LAYOUT_VAL varchar(2000) NULL,
	DISPLAY_NUM decimal(5, 0) NULL,
	HEIGHT decimal(5, 0) NULL,
	OPTIONS varchar(1000) NULL,
	FONT_NUM decimal(5, 0) NULL,
	CREATE_USER bigint NULL,
	CREATE_TIME datetime NULL,
	LAST_UPDATE_USER bigint NULL,
	LAST_UPDATE_TIME datetime NULL,
	VERSION bigint NULL,
	PARENT_ID bigint NULL,
	STATUS varchar(1) NULL,
	LEVEL_VAL decimal(2, 0) NULL,
 	CONSTRAINT PK_SYS_DESKTOP_TO_USER PRIMARY KEY (ID)
);

ALTER TABLE SYS_ATTACH_DETAIL   ADD FOREIGN KEY(ATTACH_ID)
REFERENCES SYS_ATTACH (ID);


ALTER TABLE SYS_DESKTOP_TO_ROLE   ADD  CONSTRAINT FK_SYS_DESK_REFERENCE_SYS_ROLE FOREIGN KEY(ROLE_ID)
REFERENCES SYS_ROLE (ID);


ALTER TABLE SYS_DESKTOP_TO_USER   ADD  CONSTRAINT FK_SYS_DESK_REFERENCE_SYS_DESK FOREIGN KEY(DESKTOP_ID)
REFERENCES SYS_DESKTOP (ID);


ALTER TABLE SYS_DESKTOP_TO_USER   ADD  CONSTRAINT FK_SYS_DESK_REFERENCE_SYS_USER FOREIGN KEY(USER_ID)
REFERENCES SYS_USER (ID);


ALTER TABLE SYS_DICTIONARY   ADD  CONSTRAINT FK_SD_DICT_TYPE_ID FOREIGN KEY(DICT_TYPE_ID)
REFERENCES SYS_DICTIONARY_TYPE (ID);


ALTER TABLE SYS_NOTICE_TO_USER   ADD  CONSTRAINT FK_SYS_NOTI_REFERENCE_SYS_NOTI FOREIGN KEY(NOTICE_ID)
REFERENCES SYS_NOTICE (ID);


ALTER TABLE SYS_NOTICE_TO_USER   ADD  CONSTRAINT FK_SYS_NOTI_REFERENCE_SYS_USER FOREIGN KEY(USER_ID)
REFERENCES SYS_USER (ID);


ALTER TABLE SYS_ONLINE_USER   ADD  CONSTRAINT FK_SOU_USER_ID FOREIGN KEY(LOGIN_USERID)
REFERENCES SYS_USER (ID);


ALTER TABLE SYS_ORGANIZATION   ADD  CONSTRAINT FK_SO_OGR_ID FOREIGN KEY(PARENT_ID)
REFERENCES SYS_ORGANIZATION (ID);


ALTER TABLE SYS_RESOURCE   ADD  CONSTRAINT FK_SR_RESOURCE_ID FOREIGN KEY(PARENT_ID)
REFERENCES SYS_RESOURCE (ID);


ALTER TABLE SYS_RESOURCE_LOCALE   ADD  CONSTRAINT FK_SRL_RESOURCE_ID FOREIGN KEY(RESOURCE_ID)
REFERENCES SYS_RESOURCE (ID);


ALTER TABLE SYS_RESOURCE_TO_ROLE   ADD  CONSTRAINT FK_SRTR_RESOURCE_ID FOREIGN KEY(RESOURCE_ID)
REFERENCES SYS_RESOURCE (ID);


ALTER TABLE SYS_RESOURCE_TO_ROLE   ADD  CONSTRAINT FK_SRTR_ROLE_ID FOREIGN KEY(ROLE_ID)
REFERENCES SYS_ROLE (ID);


ALTER TABLE SYS_USER   ADD  CONSTRAINT FK_SYS_USER_ORG_ID FOREIGN KEY(ORGANIZATION_ID)
REFERENCES SYS_ORGANIZATION (ID);


ALTER TABLE SYS_USER_TO_ROLE   ADD  CONSTRAINT FK_SUTR_ROLE_ID FOREIGN KEY(ROLE_ID)
REFERENCES SYS_ROLE (ID);


ALTER TABLE SYS_USER_TO_ROLE   ADD  CONSTRAINT FK_SUTR_USER_ID FOREIGN KEY(USER_ID)
REFERENCES SYS_USER (ID);


CREATE TABLE SYS_DEPLOY_LOG (
   ID                   bigint          auto_increment NOT NULL,
   DEPLOY_URL           varchar(500)        NULL,
   DEPLOY_VERSION       varchar(100)         NULL,
   DEPLOY_TYPE          varchar(50)         NULL,
   DEPLOY_NAME          varchar(100)        NULL,
   DEPLOY_DESC          varchar(2000)       NULL,
   DEPLOY_TIME_START    datetime             NULL,
   DEPLOY_TIME_END      datetime             NULL,
   OPERATION_IP         varchar(100)        NULL,
   OPERATION_MACHINE_NAME varchar(100)      NULL,
   DEPLOY_FLAG          varchar(50)          NULL,
   EXCEPTION_LOG        text        NULL,
   LOGIN_NAME           varchar(50)         NULL,
   REAL_NAME            varchar(50)         NULL,
   ORG_NAME             varchar(100)        NULL,
   ROLLBACK_ID          bigint          NULL,
   CONSTRAINT PK_SYS_DEPLOY_LOG PRIMARY KEY (ID)
);

/** 2013-02-25 新增任务调度相关表  **/
CREATE TABLE SYS_TASK (
   ID                   bigint          auto_increment NOT NULL,
   CREATE_USER          bigint          NULL,
   CREATE_TIME          datetime             NULL,
   LAST_UPDATE_USER     bigint          NULL,
   LAST_UPDATE_TIME     datetime             NULL,
   VERSION              bigint          NULL,
   NAME                 varchar(200)        NULL,
   REMARK               varchar(500)        NULL,
   TASK_TYPE            varchar(50)          NULL,
   TASK_CONTENT         text        NULL,
   CONSTRAINT PK_SYS_TASK PRIMARY KEY (ID)
);


CREATE TABLE SYS_TASK_SCHEDULER (
   ID                   bigint          auto_increment NOT NULL,
   CREATE_USER          bigint          NULL,
   CREATE_TIME          datetime             NULL,
   LAST_UPDATE_USER     bigint          NULL,
   LAST_UPDATE_TIME     datetime             NULL,
   VERSION              bigint          NULL,
   TASK_ID              bigint          NULL,
   NAME                 varchar(200)        NULL,
   REMARK               varchar(500)        NULL,
   DATE_TYPE            varchar(50)          NULL,
   RUN_DAY              varchar(50)          NULL,
   RUN_HOUR             varchar(50)          NULL,
   RUN_MINUTE           varchar(50)          NULL,
   RUN_SECOND           varchar(50)          NULL,
   RUN_MONTH            varchar(50)          NULL,
   RUN_WEEK             varchar(50)          NULL,
   STATUS               varchar(50)          NULL,
   CONSTRAINT PK_SYS_TASK_SCHEDULER PRIMARY KEY (ID)
);


ALTER TABLE SYS_TASK_SCHEDULER
   ADD CONSTRAINT FK_TS_TASK_ID FOREIGN KEY (TASK_ID)
      REFERENCES SYS_TASK (ID);
      
/** 2013-05-07 新增系统分类功能支持 **/
CREATE TABLE SYS_COMMON_TYPE_TO_ROLE (
   TYPE_ID  bigint NOT NULL,
   ROLE_ID  bigint NOT NULL
);

ALTER TABLE SYS_COMMON_TYPE_TO_ROLE
   ADD CONSTRAINT FK_SYS_COMM_REFERENCE_SYS_COMM FOREIGN KEY (TYPE_ID)
      REFERENCES SYS_COMMON_TYPE (ID);

ALTER TABLE SYS_COMMON_TYPE_TO_ROLE
   ADD CONSTRAINT FK_SYS_COMM_REFERENCE_SYS_ROLE FOREIGN KEY (ROLE_ID)
      REFERENCES SYS_ROLE (ID);

alter table SYS_COMMON_TYPE add TYPE_BELONG VARCHAR(2) NULL;

alter table SYS_COMMON_TYPE add STATUS VARCHAR(1) NULL;

/** 2013-05-15 新增组织新闻公告关联表 **/
CREATE TABLE SYS_NEWS_TO_ORG (
   NEWS_ID              bigint          NOT NULL,
   ORG_ID               bigint          NOT NULL
);

ALTER TABLE SYS_NEWS_TO_ORG
   ADD CONSTRAINT FK_SYS_NEWS_REFERENCE_SYS_NEWS FOREIGN KEY (NEWS_ID)
      REFERENCES SYS_NEWS (ID);

ALTER TABLE SYS_NEWS_TO_ORG
   ADD CONSTRAINT FK_SYS_NEWS_REFERENCE_SYS_ORGA FOREIGN KEY (ORG_ID)
      REFERENCES SYS_ORGANIZATION (ID);

/** 2013-05-16 角色表新增门户首页地址与登录后首页地址 **/
alter table SYS_ROLE add INDEX_URL VARCHAR(500) NULL;

alter table SYS_ROLE add PORTAL_URL VARCHAR(500) NULL;

/** 2013-06-20 数据流转历史记录表 **/
CREATE TABLE SYS_PROCESS_MODEL_INFO (
   ID                   bigint          auto_increment NOT NULL,
   PROCESS_MODEL_ID     bigint          NOT NULL,
   MASTER_ID            bigint          NOT NULL,
   CREATE_USER          bigint          NOT NULL,
   MODEL_KEY            varchar(300)         NOT NULL,
   PROCESS_INSTANCE_ID  varchar(300)         NOT NULL,
   BUSINESS_KEY         varchar(300)         NOT NULL,
   EXECUTION_ID         varchar(300)         NULL,
   CREATE_TIME          datetime             NULL,
   CONSTRAINT PK_SYS_PROCESS_MODEL_INFO PRIMARY KEY (ID)
);

/** 2013-12-18 用户表增加绑定IP字段，角色可看用户关联表 **/
alter table SYS_USER add BIND_IP VARCHAR(100) NULL
;
CREATE TABLE SYS_ROLE_SEE_USER (
   ROLE_ID             bigint          NULL,
   USER_ID             bigint          NULL
)
;

ALTER TABLE SYS_ROLE_SEE_USER
   ADD CONSTRAINT FK_SYS_ROLE_REFERENCE_SYS_ROLE FOREIGN KEY (ROLE_ID)
      REFERENCES SYS_ROLE (ID)
;

ALTER TABLE SYS_ROLE_SEE_USER
   ADD CONSTRAINT FK_SYS_ROLE_REFERENCE_SYS_USER FOREIGN KEY (USER_ID)
      REFERENCES SYS_USER (ID)
;

/** 2013-12-21 变更日志记录 **/
CREATE TABLE SYS_CHANGE_LOG (
   ID                   bigint          auto_increment NOT NULL,
   MODULE_NAME_EN       VARCHAR(100)         NULL,
   MODULE_NAME_CN       VARCHAR(100)        NULL,
   TABLE_NAME_EN        VARCHAR(100)         NULL,
   TABLE_NAME_CN        VARCHAR(100)        NULL,
   FIELD_NAME_EN        VARCHAR(100)         NULL,
   FIELD_NAME_CN        VARCHAR(100)        NULL,
   ENTITY_NAME          VARCHAR(200)        NULL,
   ENTITY_ID            VARCHAR(100)         NULL,
   BEFORE_VAL           VARCHAR(1000)       NULL,
   AFTER_VAL            VARCHAR(1000)       NULL,
   CHANGE_DESC          text		        NULL,
   USER_ID              bigint          	 NULL,
   OPER_TIME            datetime             NULL,
   OPER_IP              VARCHAR(100)         NULL,
   LOGIN_NAME           VARCHAR(100)        NULL,
   REAL_NAME            VARCHAR(100)        NULL,
   EMPLOYEE_NAME        VARCHAR(100)        NULL,
   ORG_NAME             VARCHAR(100)        NULL,
   CONSTRAINT PK_SYS_CHANGE_LOG PRIMARY KEY (ID)
);

/** 2014-04-01 增加邮件发送功能支持  **/
CREATE TABLE SYS_SEND_MAIL_MSG (
   ID                   bigint          auto_increment NOT NULL,
   SENDER               VARCHAR(200)        NULL,
   SUBJECT              VARCHAR(500)        NULL,
   MAIL_BODY            text        NULL,
   ADDRESSEE            text        NULL,
   COPYTO               VARCHAR(1000)       NULL,
   SEND_TIME            DATETIME             NULL,
   STATUS               VARCHAR(1)           NULL,
   ATM_ID               bigint          NULL,
   SOURCE               VARCHAR(1)           NULL,
   CREATE_TIME          DATETIME             NULL,
   CREATE_USER          bigint          NULL,
   LAST_UPDATE_USER     bigint          NULL,
   LAST_UPDATE_TIME     DATETIME             NULL,
   VERSION              bigint          NULL,
   CONSTRAINT PK_SYS_SEND_MAIL_MSG PRIMARY KEY (ID)
);

/** 2014-04-15 增加短信发送功能支持  **/
CREATE TABLE SYS_SMS_MSG (
   ID                   bigint          auto_increment,
   CREATE_TIME          DATETIME             NULL,
   CREATE_USER          bigint          NULL,
   LAST_UPDATE_USER     bigint         NULL,
   LAST_UPDATE_TIME     DATETIME             NULL,
   VERSION              bigint          NULL,
   SERVICE_SMS          VARCHAR(100)        NULL,
   TARGET_PHONE         text        NULL,
   SOURCE_PHONE         VARCHAR(100)        NULL,
   SMS_BODY             text        NULL,
   SEND_TIME            DATETIME             NULL,
   STATUS               VARCHAR(1)           NULL,
   REG_SEND_TIME        DATETIME             NULL,
   SOURCE               VARCHAR(1)           NULL,
   RESULT             text        NULL,
   CONSTRAINT PK_SYS_SMS_MSG PRIMARY KEY (ID)
);

/* 邮件管理增加服务邮箱号字段 */
ALTER TABLE SYS_SEND_MAIL_MSG ADD SERVICE_MAIL VARCHAR(100);
ALTER TABLE SYS_SEND_MAIL_MSG ADD RESULT text;

/** 2014-06-08 增加个人配置表支持快捷方式功能 **/
CREATE TABLE SYS_USER_CONFIG (
   ID                   bigint          auto_increment,
   USER_ID              bigint          NULL,
   SHORTCUT             text        NULL,
   DESK_URL             VARCHAR(500)        NULL,
   CREATE_TIME          DATETIME             NULL,
   CREATE_USER          bigint          NULL,
   LAST_UPDATE_USER     bigint          NULL,
   LAST_UPDATE_TIME     DATETIME             NULL,
   VERSION              bigint          NULL,
   CONSTRAINT PK_SYS_USER_CONFIG PRIMARY KEY (ID)
);

/** 2014-11-09 增加子实体ID字段 **/
ALTER TABLE SYS_CHANGE_LOG ADD SUB_ENTITY_ID VARCHAR(100);

/** 2015-02-11 邮件增加密送字段 **/
ALTER TABLE SYS_SEND_MAIL_MSG ADD BCC VARCHAR(1000);

/** 2015-03-10 角色增加可见用户属性字段 **/
ALTER TABLE SYS_ROLE ADD VISIBLE_ATTR VARCHAR(500);

/** 2015-03-11 增加数据共享设置表 **/
CREATE TABLE SYS_DATA_SHARE_CFG (
   ID                   bigint          auto_increment,
   ENTITY_NAME          VARCHAR(200)        NULL,
   ENTITY_ID            VARCHAR(100)         NULL,
   OPER_USER_ID         bigint          NULL,
   OPER_TIME            DATETIME             NULL,
   SHARE_USER_ID        bigint          NULL,
   CONSTRAINT PK_SYS_DATA_SHARE_CFG PRIMARY KEY (ID)
);

/** 2015-03-12 增加bbs发帖表 **/
CREATE TABLE SYS_TIE (
  ID bigint NOT NULL AUTO_INCREMENT,
  CONTENT text,
  PARENT_ID bigint DEFAULT NULL,
  RELEVANCE_ID bigint NOT NULL,
  MODULE_NAME varchar(500) DEFAULT NULL,
  USER_ID bigint NOT NULL,
  TIME datetime DEFAULT NULL,
  CONSTRAINT PK_SYS_TIE PRIMARY KEY (ID)
);

/** 2015-04-08 增加备忘录表 **/
CREATE TABLE SYS_MEMO (
   ID                   bigint     auto_increment        NOT NULL,
   CREATE_TIME          DATETIME             NULL,
   CREATE_USER          bigint          NULL,
   LAST_UPDATE_USER     bigint          NULL,
   LAST_UPDATE_TIME     DATETIME             NULL,
   VERSION              bigint          NULL,
   TITLE                VARCHAR(100)        NULL,
   CONTENT_VAL          text        NULL,
   REMIND_TYPE          VARCHAR(20)         NULL,
   REMIND_TIME          DATETIME             NULL,
   REMIND_DATE          DATETIME             NULL,
   REMIND_WEEK          VARCHAR(100)        NULL,
   STATUS               VARCHAR(1)          NULL,
   CONSTRAINT PK_SYS_MEMO PRIMARY KEY (ID)
);

/** 2015-04-08 增加备忘录用户关联表 **/
CREATE TABLE SYS_MEMO_TO_USER (
   ID                   bigint     auto_increment     NOT NULL,
   MEMO_ID              bigint          NULL,
   USER_ID              bigint          NULL,
   STATUS               VARCHAR(1)          NULL,
   CONSTRAINT PK_SYS_MEMO_TO_USER PRIMARY KEY (ID)
);
ALTER TABLE SYS_MEMO_TO_USER
   ADD CONSTRAINT FK_SYS_MEMO_REFERENCE_SYS_MEMO FOREIGN KEY (MEMO_ID)
      REFERENCES SYS_MEMO (ID)
;

/** 2015-04-10 增加访问者关联表 **/
CREATE TABLE SYS_VISITOR (
   ID                   bigint     auto_increment     NOT NULL,
   RELEVANCE_ID         bigint                NULL,
   USER_ID              bigint                NULL,
   VISIT_TIME           DATETIME              NULL,
   CONSTRAINT PK_SYS_VISITOR PRIMARY KEY (ID)
);

/** 2015-05-25 增加签到表 **/
CREATE TABLE SYS_CHECKINS (
   ID                   BIGINT        auto_increment       NOT NULL,
   CHECHINS_TIME        DATETIME             NULL,
   LNG                  decimal(9,6)         NULL,
   LAT                  decimal(9,6)         NULL,
   USER_ID              BIGINT               NULL,
   TITLE                VARCHAR(100)          NULL,
   CONTENT              VARCHAR(1000)         NULL,
   PALCE                VARCHAR(100)          NULL,
   ATTACH_CONTENT       text        NULL,
   TYPE					VARCHAR(2)			NULL,
   CONSTRAINT PK_SYS_CHECKINS PRIMARY KEY (ID)
);

/** 2016-03-29 无主键表增加联合主键，方便使用Galera Cluster **/
ALTER TABLE sys_common_type_to_role ADD CONSTRAINT PK_SYS_CT_TO_ROLE PRIMARY KEY(TYPE_ID, ROLE_ID);
ALTER TABLE sys_desktop_to_role ADD CONSTRAINT PK_SYS_DESKTOP_TO_ROLE PRIMARY KEY(DESKTOP_ID, ROLE_ID);
ALTER TABLE sys_news_to_org ADD CONSTRAINT PK_SYS_NEWS_TO_ORG PRIMARY KEY(NEWS_ID, ORG_ID);
ALTER TABLE sys_resource_to_role ADD CONSTRAINT PK_SYS_RESOURCE_TO_ROLE PRIMARY KEY(RESOURCE_ID, ROLE_ID);
ALTER TABLE sys_user_to_role ADD CONSTRAINT PK_SYS_USER_TO_ROLE PRIMARY KEY(USER_ID, ROLE_ID);
SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE sys_role_see_user ADD CONSTRAINT PK_SYS_ROLE_SEE_USER PRIMARY KEY(ROLE_ID, USER_ID);
SET FOREIGN_KEY_CHECKS=1;

/** 2016-10-13 增加用户自定义字段相关表 **/
CREATE TABLE SYS_BS_TABLE (
   ID                   BIGINT        auto_increment       NOT NULL,
   SYSTEM_CODE        VARCHAR(100)             NULL,
   TABLE_KEY          VARCHAR(200)         NULL,
   ENTITY_NAME 		  VARCHAR(400)			NULL,
   CAPTION			  VARCHAR(200)			NULL,
   REMARK       VARCHAR(1000)			NULL,
   ALLOW_USER_DEF_FIELD       VARCHAR(100)			NULL,
   STATUS       VARCHAR(100)			NULL,
   CREATE_USER bigint NULL,
   CREATE_TIME datetime NULL,
   LAST_UPDATE_USER bigint NULL,
   LAST_UPDATE_TIME datetime NULL,
   VERSION bigint NULL,
   CONSTRAINT PK_SYS_BS_TABLE PRIMARY KEY (ID)
);

CREATE TABLE SYS_BS_FIELD (
   ID                   BIGINT        auto_increment       NOT NULL,
   MASTER_ID	BIGINT,
   TABLE_KEY          VARCHAR(200)         NULL,
   ENTITY_NAME 		  VARCHAR(400)			NULL,
   FIELD_KEY          VARCHAR(200)         NULL,
   CAPTION			  VARCHAR(200)			NULL,
   REMARK       VARCHAR(1000)			NULL,
   DATA_TYPE       VARCHAR(100)			NULL,
   EDIT_TYPE       VARCHAR(100)			NULL,
   STATUS       VARCHAR(100)			NULL,
   EDIT_DS       TEXT			NULL,
   FORMAT       VARCHAR(100)			NULL,
   EXP_RULE       VARCHAR(1000)			NULL,
   DEFAULT_VAL     VARCHAR(200)			NULL,
   FIELD_TYPE       VARCHAR(100)			NULL,
   CREATE_USER bigint NULL,
   CREATE_TIME datetime NULL,
   LAST_UPDATE_USER bigint NULL,
   LAST_UPDATE_TIME datetime NULL,
   VERSION bigint NULL,
   CONSTRAINT PK_SYS_BS_FIELD PRIMARY KEY (ID)
);

CREATE TABLE SYS_BS_FIELD_INS (
   ID                   BIGINT        auto_increment       NOT NULL,
   MASTER_ID	BIGINT,
   TABLE_KEY          VARCHAR(200)         NULL,
   ENTITY_NAME 		  VARCHAR(400)			NULL,
   ENTITY_ID		  VARCHAR(100)			NULL,
   FIELD_KEY          VARCHAR(200)         NULL,
   CAPTION			  VARCHAR(200)			NULL,
   DATA_TYPE       VARCHAR(100)			NULL,
   EDIT_TYPE       VARCHAR(100)			NULL,
   STATUS       VARCHAR(100)			NULL,
   STRING_VAL       VARCHAR(4000)			NULL,
   LONG_VAL       BIGINT			NULL,
   DOUBLE_VAL     decimal(30,8)		NULL,
   TEXT_VAL       TEXT			NULL,
   DATE_VAL datetime NULL,
   CREATE_USER bigint NULL,
   CREATE_TIME datetime NULL,
   LAST_UPDATE_USER bigint NULL,
   LAST_UPDATE_TIME datetime NULL,
   VERSION bigint NULL,
   CONSTRAINT PK_SYS_BS_FIELD_INS PRIMARY KEY (ID)
);

/* 2016-12-15 用户表增加头像和签名字段  */
ALTER TABLE SYS_USER ADD COLUMN AVATAR VARCHAR(1000);
ALTER TABLE SYS_USER ADD COLUMN SIGNATURE VARCHAR(200);
ALTER TABLE SYS_USER ADD COLUMN UID VARCHAR(100);

/* 2017-01-22 业务表增加模块key和主实体名  */
ALTER TABLE SYS_BS_TABLE ADD COLUMN MODULE_KEY VARCHAR(200);
ALTER TABLE SYS_BS_TABLE ADD COLUMN MASTER_ENTITY_NAME VARCHAR(400);
ALTER TABLE SYS_BS_FIELD ADD COLUMN MODULE_KEY VARCHAR(200);
ALTER TABLE SYS_BS_FIELD ADD COLUMN FIELD_INDEX VARCHAR(100);
ALTER TABLE SYS_BS_FIELD ADD COLUMN IS_TEXT_FIELD VARCHAR(10);

/* 2017-04-15 增加业务模块表 */
CREATE TABLE SYS_BS_MODULE (
   ID                   BIGINT        auto_increment       NOT NULL,
   CAPTION			  VARCHAR(200)			NULL,
   REMARK       VARCHAR(1000)			NULL,
   STATUS       VARCHAR(100)			NULL,
   MODULE_KEY	VARCHAR(200) NULL,
   REF_MODULE_KEY	VARCHAR(200) NULL,
   MODULE_TYPE	VARCHAR(100) NULL,
   CREATE_USER bigint NULL,
   CREATE_TIME datetime NULL,
   LAST_UPDATE_USER bigint NULL,
   LAST_UPDATE_TIME datetime NULL,
   VERSION bigint NULL,
   CONSTRAINT PK_SYS_BS_MODULE PRIMARY KEY (ID)
);

/* 2018-07-09 增加分类排序号 */
alter table SYS_COMMON_TYPE add COLUMN SORT_ORDER_NO VARCHAR(10) NULL;

/* 2018-09-01  添加菜单资源表的模块展示类型*/
alter table SYS_RESOURCE add COLUMN APP_SHOW TINYINT(1) DEFAULT 0;

