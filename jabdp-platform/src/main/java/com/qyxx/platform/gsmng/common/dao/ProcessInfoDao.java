package com.qyxx.platform.gsmng.common.dao;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.gsc.cache.SqlCache;
import com.qyxx.platform.gsc.utils.Constants;
import com.qyxx.platform.gsmng.common.entity.ProcessInfo;
import com.qyxx.platform.sysmng.exception.GsException;


/**
 * 流程实体管理
 */
@Component
public class ProcessInfoDao  extends HibernateDao<ProcessInfo, Long> {
	/**
	 * 根据sql语句及参数查询指定数量的记录
	 * 
	 * @param sql
	 * @param param
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findDataList(String sql,Map<String, Object> param) {
		SQLQuery sq = getSession().createSQLQuery(sql);
		if(null!=param) {
			sq.setProperties(param);
		}
		sq.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return sq.list();
	}
	
	/**
	 * 根据业务规则获取字符串值
	 * @param ruleName 业务规则-sql语句
	 * @param defaultValue 默认值
	 * @return
	 */
	public String getStringValueByParam(final Map<String, Object> paramMap, String ruleName, String defaultValue){
		String sqlKey = Constants.RULE_SUFFIX + Constants.NAME_SPLIT_SYMBOL + ruleName;
		final String namedSql = SqlCache.getInstance().getSqlContent(sqlKey);
		if(StringUtils.isBlank(namedSql)) {
			return defaultValue;
		}
		SQLQuery cq = getSession().createSQLQuery(namedSql);
		cq.setProperties(paramMap);
		String val = String.valueOf(cq.list().get(0));
		return StringUtils.defaultString(val, defaultValue);
	}
}
