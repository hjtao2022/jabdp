package com.qyxx.platform.gsmng.common.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 流程定义表
 * 
 * @author xk
 * @version 1.0 2012-12-27 下午17:42:18
 * @since jdk1.6
 */
@Entity
@Table(name = "ACT_RE_PROCDEF")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProcessDefinitionInstance  implements Serializable {

	private String id;
	private Integer rev;
	private String category;
	private String name;
	private String key;
	private Integer version;
	private String deploymentId;
	private String resourceName;
	private String dgrmResourceName;
	private String description;
	private Integer hasStartFormKey;
	private Integer suspensionState;
	private String moduleName;

	@Id
	@Column(name = "ID_", length = 64, nullable = false)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "REV_")
	public Integer getRev() {
		return rev;
	}

	public void setRev(Integer rev) {
		this.rev = rev;
	}

	@Column(name = "CATEGORY_")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "NAME_")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "KEY_", length = 255, nullable = false)
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Column(name = "VERSION_", nullable = false)
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@Column(name = "DEPLOYMENT_ID_")
	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	@Column(name = "RESOURCE_NAME_")
	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	@Column(name = "DGRM_RESOURCE_NAME_")
	public String getDgrmResourceName() {
		return dgrmResourceName;
	}

	public void setDgrmResourceName(String dgrmResourceName) {
		this.dgrmResourceName = dgrmResourceName;
	}

	@Column(name = "DESCRIPTION_")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "HAS_START_FORM_KEY_")
	public Integer getHasStartFormKey() {
		return hasStartFormKey;
	}

	public void setHasStartFormKey(Integer hasStartFormKey) {
		this.hasStartFormKey = hasStartFormKey;
	}

	@Column(name = "SUSPENSION_STATE_")
	public Integer getSuspensionState() {
		return suspensionState;
	}

	public void setSuspensionState(Integer suspensionState) {
		this.suspensionState = suspensionState;
	}

	@Transient
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
}
