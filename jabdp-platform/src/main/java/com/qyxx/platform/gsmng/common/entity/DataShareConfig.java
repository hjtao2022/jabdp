/*
 * @(#)DataShareConfig.java
 * 2015-3-11 下午01:33:35
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *  数据共享配置
 *  @author gxj
 *  @version 1.0 2015-3-11 下午01:33:35
 *  @since jdk1.6 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_DATA_SHARE_CFG")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DataShareConfig implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = 2423006568716215832L;
	
	private Long id;
	
	private String entityName; //实体名
	private String entityId; //实体主键值
	
	private Long operUserId; //操作用户ID
	private Long shareUserId; //共享用户ID
	
	private Date operTime;//操作时间

	
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_DATA_SHARE_CFG_ID")
	@SequenceGenerator(name="SEQ_SYS_DATA_SHARE_CFG_ID", sequenceName="SEQ_SYS_DATA_SHARE_CFG_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return entityName
	 */
	@Column(name="ENTITY_NAME",length=200)
	public String getEntityName() {
		return entityName;
	}

	
	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	
	/**
	 * @return entityId
	 */
	@Column(name="ENTITY_ID",length=100)
	public String getEntityId() {
		return entityId;
	}

	
	/**
	 * @param entityId
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	
	/**
	 * @return operUserId
	 */
	@Column(name="OPER_USER_ID")
	public Long getOperUserId() {
		return operUserId;
	}

	
	/**
	 * @param operUserId
	 */
	public void setOperUserId(Long operUserId) {
		this.operUserId = operUserId;
	}

	
	/**
	 * @return shareUserId
	 */
	@Column(name="SHARE_USER_ID")
	public Long getShareUserId() {
		return shareUserId;
	}

	
	/**
	 * @param shareUserId
	 */
	public void setShareUserId(Long shareUserId) {
		this.shareUserId = shareUserId;
	}

	
	/**
	 * @return operTime
	 */
	@Column(name="OPER_TIME")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getOperTime() {
		return operTime;
	}

	
	/**
	 * @param operTime
	 */
	public void setOperTime(Date operTime) {
		this.operTime = operTime;
	}
	
	

}
