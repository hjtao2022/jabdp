package com.qyxx.platform.gsmng.common.dao;
import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.gsmng.common.entity.ProcessModelInfo;


/**
 * 流程流转信息管理
 */
@Component
public class ProcessModelInfoDao  extends HibernateDao<ProcessModelInfo, Long> {

}
