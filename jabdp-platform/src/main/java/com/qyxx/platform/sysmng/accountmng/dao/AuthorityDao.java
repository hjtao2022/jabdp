/*
 * @(#)AuthorityDao.java
 * 2011-5-9 下午09:09:50
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.accountmng.entity.Authority;


/**
 *  授权对象的泛型DAO
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-9 下午09:09:50
 */
@Component
public class AuthorityDao extends HibernateDao<Authority, Long> {
	private static final String INSERT = "INSERT INTO SYS_RESOURCE_TO_ROLE(ROLE_ID, RESOURCE_ID) VALUES(?, ?)";
	private static final String DELETE = "DELETE FROM SYS_RESOURCE_TO_ROLE WHERE ROLE_ID=?";
	private static final String DELETE_RESOURCE_ID = "DELETE FROM SYS_RESOURCE_TO_ROLE WHERE RESOURCE_ID=?";
	
	private static final String INSERT_ROLE_SEE_USER = "INSERT INTO SYS_ROLE_SEE_USER(ROLE_ID, USER_ID) VALUES(?, ?)";
	private static final String DELETE_ROLE_SEE_USER = "DELETE FROM SYS_ROLE_SEE_USER WHERE ROLE_ID=?";
	
	/**
	 * 保存角色与资源的关联关系
	 * 
	 * @param roleId
	 * @param resourceId
	 */
	public void save(Long roleId,Long resourceId){
		 getSession().createSQLQuery(INSERT).setLong(0, roleId).setLong(1,resourceId).executeUpdate();
	}
	
	/**
	 * 根据角色ID删除资源与角色关联关系
	 * 
	 * @param roleId
	 */
	public void deleteByRoleId(Long roleId){
		 getSession().createSQLQuery(DELETE).setLong(0, roleId).executeUpdate(); 
	}
	
	/**
	 * 根据资源ID删除资源与角色关联关系
	 * 
	 * @param resourceId
	 */
	public void deleteByResourceId(Long resourceId){
		 getSession().createSQLQuery(DELETE_RESOURCE_ID).setLong(0, resourceId).executeUpdate();
	}
	
	/**
	 * 保存角色可见用户关系
	 * 
	 * @param roleId
	 * @param userId
	 */
	public void saveRoleSeeUser(Long roleId,Long userId){
		 getSession().createSQLQuery(INSERT_ROLE_SEE_USER).setLong(0, roleId).setLong(1,userId).executeUpdate();
	}
	
	/**
	 * 根据角色ID删除角色可见用户关系
	 * 
	 * @param roleId
	 */
	public void deleteRoleSeeUserByRoleId(Long roleId){
		 getSession().createSQLQuery(DELETE_ROLE_SEE_USER).setLong(0, roleId).executeUpdate();
	}
}
