/*
 * @(#)CheckinsManager.java
 * 2016年1月10日 下午10:47:19
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.checkins.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.accountmng.dao.UserDao;
import com.qyxx.platform.sysmng.checkins.dao.CheckinsDao;
import com.qyxx.platform.sysmng.checkins.entity.Checkins;

/**
 *  签到服务
 *  @author gxj
 *  @version 1.0 2016年1月10日 下午10:47:19
 *  @since jdk1.6 
 */
@Component
@Transactional
public class CheckinsManager {
	//根据USERID分组查询数据
	private static final String QUERY_HQL_BY_USERID = "select a from Checkins a where a.id = (select max(b.id) from Checkins b where b.userId = a.userId and b.type = '2')";
	
	private CheckinsDao checkinsDao;
	
	private UserDao userDao;
	
	/**
	 * @param userDao
	 */
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Autowired
	public void setCheckins(CheckinsDao checkinsDao) {
		this.checkinsDao = checkinsDao;
	}
	
	@Transactional(readOnly = true)
	public Page<Checkins> findPage(Page<Checkins> page, List<PropertyFilter> filters) {
		return checkinsDao.findPage(page, filters);
	}
	
	@Transactional(readOnly = true)
	public Page<Checkins> findPage(Page<Checkins> page) {
		return preHandlePage(checkinsDao.findPage(page));
	}
	
	public void save(Checkins entity) {
		checkinsDao.save(entity);
	}
	
	public void delete(Long[] ids) {
		if(null!=ids) {
			for(Long id : ids) {
				checkinsDao.delete(id);
			}
		}
	}
	
	/**
	 * 根据用户分组查询签到数据
	 * 
	 * @param page
	 * @param filters
	 * @return
	 */
	public Page<Checkins> findPageGroupByUserId(Page<Checkins> page, List<PropertyFilter> filters) {
		return preHandlePage(checkinsDao.findPage(QUERY_HQL_BY_USERID, page, filters));
	}

	/**
	 * 处理Page对象，翻译字段
	 * 
	 * @param page
	 * @return
	 */
	public Page<Checkins> preHandlePage(Page<Checkins> page) {
		List<Checkins> list = page.getResult();
		if(list != null) {
			for(Checkins n : list) {
				n.setUserName(userDao.getUserNameById(n.getUserId()));
			}
		}
		return page;
	}
	
	
}
