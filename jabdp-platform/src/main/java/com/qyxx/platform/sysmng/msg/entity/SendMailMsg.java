/*
 * @(#)MailMsg.java
 * 2014-1-7 下午09:25:48
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.msg.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  发送电子邮件信息
 *  @author gxj
 *  @version 1.0 2014-1-7 下午09:25:48
 *  @since jdk1.6 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_SEND_MAIL_MSG")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SendMailMsg extends BaseEntity {

	private static final long serialVersionUID = 7940595012638855516L;
	
	public static final String STATUS_DRAFT = "0";//发送状态--草稿
	public static final String STATUS_UNSEND = "1";//发送状态--待发
	public static final String STATUS_SEND = "2";//发送状态--已发
	
	public static final String SOURCE_BYHAND = "0";//来源--手工创建
	public static final String SOURCE_SYSTEM = "1";//来源--系统自动创建
	
	private Long id;//编号
	private String sender;//发件人
	private String subject;//主题
	private String mailBody;//正文
	private String addressee;//收件人
	private String copyto;//抄送
	private Date sendTime;//发送时间
	private String status;//发送状态
	private Long atmId;//附件编号
	private String source;//邮件来源
	private String serviceMail;//服务邮箱
	private String result; //发送结果
	private String bcc; //密送
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_SEND_MAIL_MSG_ID")
	@SequenceGenerator(name="SEQ_SYS_SEND_MAIL_MSG_ID", sequenceName="SEQ_SYS_SEND_MAIL_MSG_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return sender
	 */
	@Column(name="SENDER",length=200)
	public String getSender() {
		return sender;
	}

	
	/**
	 * @param sender
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	
	/**
	 * @return subject
	 */
	@Column(name="SUBJECT",length=500)
	public String getSubject() {
		return subject;
	}

	
	/**
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	
	/**
	 * @return mailBody
	 */
	@Lob
	@Column(name="MAIL_BODY", columnDefinition = "ntext")
	public String getMailBody() {
		return mailBody;
	}

	
	/**
	 * @param mailBody
	 */
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}

	
	/**
	 * @return addressee
	 */
	@Lob
	@Column(name="ADDRESSEE", columnDefinition = "ntext")
	public String getAddressee() {
		return addressee;
	}

	
	/**
	 * @param addressee
	 */
	public void setAddressee(String addressee) {
		this.addressee = addressee;
	}

	
	/**
	 * @return copyto
	 */
	@Column(name="COPYTO",length=1000)
	public String getCopyto() {
		return copyto;
	}

	
	/**
	 * @param copyto
	 */
	public void setCopyto(String copyto) {
		this.copyto = copyto;
	}

	
	/**
	 * @return sendTime
	 */
	@Column(name="SEND_TIME")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getSendTime() {
		return sendTime;
	}

	
	/**
	 * @param sendTime
	 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	
	/**
	 * @return status
	 */
	@Column(name="STATUS",length=1)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return atmId
	 */
	@Column(name="ATM_ID")
	public Long getAtmId() {
		return atmId;
	}

	
	/**
	 * @param atmId
	 */
	public void setAtmId(Long atmId) {
		this.atmId = atmId;
	}

	
	/**
	 * @return source
	 */
	@Column(name="SOURCE",length=1)
	public String getSource() {
		return source;
	}

	
	/**
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	
	/**
	 * @return serviceMail
	 */
	@Column(name="SERVICE_MAIL",length=100)
	public String getServiceMail() {
		return serviceMail;
	}

	
	/**
	 * @param serviceMail
	 */
	public void setServiceMail(String serviceMail) {
		this.serviceMail = serviceMail;
	}

	
	/**
	 * @return result
	 */
	@Lob
	@Column(name="RESULT", columnDefinition = "ntext")
	public String getResult() {
		return result;
	}

	
	/**
	 * @param result
	 */
	public void setResult(String result) {
		this.result = result;
	}

	
	/**
	 * @return bcc
	 */
	@Column(name="BCC",length=1000)
	public String getBcc() {
		return bcc;
	}

	
	/**
	 * @param bcc
	 */
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	
	
	
}
