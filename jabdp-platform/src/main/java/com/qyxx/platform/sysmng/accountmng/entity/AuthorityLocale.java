/*
 * @(#)AutorityLocal.java
 * 2011-5-29 下午03:38:05
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 资源国际化
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-5-29 下午03:38:05
 */
@Entity
// 表名与类名不相同时重新定义表名.
@Table(name = "SYS_RESOURCE_LOCALE")
// 默认的缓存策略.
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AuthorityLocale implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Authority authority;
	private String resourceName;
	private String resourceLocale;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_RESOURCE_LOCALE_ID")
	@SequenceGenerator(name = "SEQ_SYS_RESOURCE_LOCALE_ID", sequenceName = "SEQ_SYS_RESOURCE_LOCALE_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "RESOURCE_ID")
	@JsonIgnore
	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceLocale() {
		return resourceLocale;
	}

	public void setResourceLocale(String resourceLocale) {
		this.resourceLocale = resourceLocale;
	}

}
