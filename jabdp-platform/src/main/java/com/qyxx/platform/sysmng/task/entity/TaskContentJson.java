/*
 * @(#)TaskContentJson.java
 * 2013-2-28 下午05:04:39
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.task.entity;

import java.io.Serializable;


/**
 *  任务内容
 *  @author gxj
 *  @version 1.0 2013-2-28 下午05:04:39
 *  @since jdk1.6 
 */

public class TaskContentJson implements Serializable {
	
	/**
	 * long
	 */
	private static final long serialVersionUID = 1780053925405949450L;

	/**
	 * 执行类型，class--class类；sql--sql语句；proc--存储过程
	 */
	public static final String EXECUTE_TYPE_CLASS = "class";
	
	public static final String EXECUTE_TYPE_SQL = "sql";
	
	public static final String EXECUTE_TYPE_PROC = "proc";
	
	/**
	 * 通知目标类型，owner--单据所有者；role--角色
	 */
	public static final String DEST_TYPE_OWNER = "owner";
	
	public static final String DEST_TYPE_ROLE = "role";
	
	private String executeType;
	
	private String executeContent;
	
	private String moduleKey;
	
	private String querySql;
	
	private String noticeVal;
	
	private String destType;
	
	private String destVal;
	
	private String executeParam;//执行参数
	
	private String updateSql; //更新sql
	
	
	
	/**
	 * @return updateSql
	 */
	public String getUpdateSql() {
		return updateSql;
	}


	
	/**
	 * @param updateSql
	 */
	public void setUpdateSql(String updateSql) {
		this.updateSql = updateSql;
	}


	/**
	 * @return executeType
	 */
	public String getExecuteType() {
		return executeType;
	}

	
	/**
	 * @param executeType
	 */
	public void setExecuteType(String executeType) {
		this.executeType = executeType;
	}

	
	/**
	 * @return executeContent
	 */
	public String getExecuteContent() {
		return executeContent;
	}

	
	/**
	 * @param executeContent
	 */
	public void setExecuteContent(String executeContent) {
		this.executeContent = executeContent;
	}

	
	/**
	 * @return moduleKey
	 */
	public String getModuleKey() {
		return moduleKey;
	}

	
	/**
	 * @param moduleKey
	 */
	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	
	/**
	 * @return querySql
	 */
	public String getQuerySql() {
		return querySql;
	}

	
	/**
	 * @param querySql
	 */
	public void setQuerySql(String querySql) {
		this.querySql = querySql;
	}

	
	/**
	 * @return noticeVal
	 */
	public String getNoticeVal() {
		return noticeVal;
	}

	
	/**
	 * @param noticeVal
	 */
	public void setNoticeVal(String noticeVal) {
		this.noticeVal = noticeVal;
	}

	
	/**
	 * @return destType
	 */
	public String getDestType() {
		return destType;
	}

	
	/**
	 * @param destType
	 */
	public void setDestType(String destType) {
		this.destType = destType;
	}

	
	/**
	 * @return destVal
	 */
	public String getDestVal() {
		return destVal;
	}

	
	/**
	 * @param destVal
	 */
	public void setDestVal(String destVal) {
		this.destVal = destVal;
	}


	
	/**
	 * @return executeParam
	 */
	public String getExecuteParam() {
		return executeParam;
	}


	
	/**
	 * @param executeParam
	 */
	public void setExecuteParam(String executeParam) {
		this.executeParam = executeParam;
	}
	
	

}
