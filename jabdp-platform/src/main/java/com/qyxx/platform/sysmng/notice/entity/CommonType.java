/**
 * 
 */
package com.qyxx.platform.sysmng.notice.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.accountmng.entity.Role;

/**
 * @author ly
 *
 */
/**
 * 系统公用分类表
 */
@Entity
@Table(name = "SYS_COMMON_TYPE")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CommonType extends BaseEntity{
	public static final String TYPE_NOTICE="notice";
	public static final String TYPE_NEWS="news";
	public static final String TYPE_SYSTEM="1";
	public static final Long PID = 0L;
	private Long id;
	private String name;
	private Long pid;
	private String typeVal;//notice通知   news新闻
	private String typeBelong;
	private String status;
	private List<Role> roleList = Lists.newArrayList();
	private Boolean checked;
	
	private String sortOrderNo;//排序号
	private String idSort; //id排列顺序
	
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_COMMON_TYPE_ID")
	@SequenceGenerator(name = "SEQ_SYS_COMMON_TYPE_ID", sequenceName = "SEQ_SYS_COMMON_TYPE_ID")
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	// 多对多定义
	@ManyToMany
	// 中间表定义,表名采用默认命名规则
	@JoinTable(name = "SYS_COMMON_TYPE_TO_ROLE", joinColumns = { @JoinColumn(name = "TYPE_ID") }, inverseJoinColumns = { @JoinColumn(name = "ROLE_ID") })
	// Fecth策略定义
	@Fetch(FetchMode.SUBSELECT)
	// 集合按id排序.
	@OrderBy("id")
	// 集合中对象id的缓存.
	//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
	/**
	 * @return the name
	 */
	@Column(name = "NAME", length = 500)
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the pid
	 */
	@Column(name = "PID")
	public Long getPid() {
		return pid;
	}
	/*
	 * @param pid the pid to set
	 */
	public void setPid(Long pid) {
		this.pid = pid;
	}
	/**
	 * @return the typeVal
	 */
	@Column(name = "TYPE_VAL", length = 50)
	public String getTypeVal() {
		return typeVal;
	}
	/**
	 * @param typeVal the typeVal to set
	 */
	public void setTypeVal(String typeVal) {
		this.typeVal = typeVal;
	}
	
	@Column(name = "TYPE_BELONG", length = 2)
	public String getTypeBelong() {
		return typeBelong;
	}
	
	public void setTypeBelong(String typeBelong) {
		this.typeBelong = typeBelong;
	}
	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Transient
	public Boolean getChecked() {
		return checked;
	}
	
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	
	/**
	 * @return sortOrderNo
	 */
	@Column(name = "SORT_ORDER_NO", length = 10)
	public String getSortOrderNo() {
		return sortOrderNo;
	}
	
	/**
	 * @param sortOrderNo
	 */
	public void setSortOrderNo(String sortOrderNo) {
		this.sortOrderNo = sortOrderNo;
	}
	
	/**
	 * @return idSort
	 */
	@Transient
	public String getIdSort() {
		return idSort;
	}
	
	/**
	 * @param idSort
	 */
	public void setIdSort(String idSort) {
		this.idSort = idSort;
	}
	
	
	
}
