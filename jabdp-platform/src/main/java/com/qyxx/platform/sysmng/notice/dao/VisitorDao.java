package com.qyxx.platform.sysmng.notice.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.notice.entity.Visitor;

@Component
public class VisitorDao extends HibernateDao<Visitor, Long> {

}
