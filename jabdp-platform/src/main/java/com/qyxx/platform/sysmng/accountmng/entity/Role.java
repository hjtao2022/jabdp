/*
 * @(#)Role.java
 * 2011-5-9 下午09:01:03
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.common.utils.reflection.ConvertUtils;
import com.qyxx.platform.sysmng.desktop.entity.Desktop;
import com.qyxx.platform.sysmng.notice.entity.CommonType;

/**
 * 角色
 * 
 * @author gxj
 * @version 1.0
 * @since 1.6 2011-5-9 下午09:01:03
 */
@Entity
@Table(name = "SYS_ROLE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Role extends BaseEntity implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = 1743536162516272252L;
	private Long id;
	private String roleName;
	private List<Authority> authorityList = Lists.newArrayList();
	private List<User> userList = Lists.newArrayList();
	private List<Desktop> desktopList=Lists.newArrayList();
	private List<CommonType> commonTypeList=Lists.newArrayList();
	private Organization organization;
	private String  organizationName;
	private String organizationId;
	private String roleDesc;
	private String roleStatus;
	private String indexUrl;//登录后首页地址
	private String portalUrl;//门户首页地址
	private String visibleAttr;//可见属性
	
	private String seeUserIds;//可见用户ID
	private String userIds;//用户ID
	
	private List<User> seeUserList = Lists.newArrayList();//可见用户数据列表
    
	public Role() {

	}

	public Role(Long id, String roleName) {
		this.id = id;
		this.roleName = roleName;
	}
	
	public Role(Long id, String roleName, String indexUrl, String portalUrl) {
		this.id = id;
		this.roleName = roleName;
		this.indexUrl = indexUrl;
		this.portalUrl = portalUrl;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_ROLE_ID")
	@SequenceGenerator(name = "SEQ_SYS_ROLE_ID", sequenceName = "SEQ_SYS_ROLE_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	

	@ManyToMany
	@JoinTable(name = "SYS_RESOURCE_TO_ROLE", joinColumns = { @JoinColumn(name = "ROLE_ID") }, inverseJoinColumns = { @JoinColumn(name = "RESOURCE_ID") })
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<Authority> getAuthorityList() {
		return authorityList;
	}

	public String getRoleName() {
		return roleName;
	}

	
	/**
	 * @return the desktopLisst
	 */
	// 多对多定义
	@ManyToMany
	// 中间表定义,表名采用默认命名规则
	@JoinTable(name = "SYS_DESKTOP_TO_ROLE", joinColumns = { @JoinColumn(name = "ROLE_ID") }, inverseJoinColumns = { @JoinColumn(name = "DESKTOP_ID") })
	// Fecth策略定义
	@Fetch(FetchMode.SUBSELECT)
	// 集合按id排序.
	@OrderBy("id")
	// 集合中对象id的缓存.
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
	public List<Desktop> getDesktopList() {
		return desktopList;
	}

	/**
	 * @param desktopLisst the desktopLisst to set
	 */
	public void setDesktopList(List<Desktop> desktopList) {
		this.desktopList = desktopList;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * @return organizationName
	 */
	@Transient
	public String getOrganizationName() {
		if (this.getOrganization() != null) {
			return this.getOrganization().getOrganizationName();
		} else {
			return organizationName;
		}
	
	}

	
	/**
	 * @param organizationName
	 */
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	
	/**
	 * @return organizationId
	 */
	@Transient
	public String getOrganizationId() {
		if (this.getOrganization() != null) {
			return this.getOrganization().getId().toString();
		} else {
			return organizationId;
		}
		
	}

	
	/**
	 * @param organizationId
	 */
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public void setAuthorityList(List<Authority> authorityList) {
		this.authorityList = authorityList;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ORGANIZATION_ID")
	@JsonIgnore
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public String getRoleStatus() {
		return roleStatus;

	}

	public void setRoleStatus(String roleStatus) {
		this.roleStatus = roleStatus;
	}

	@ManyToMany
	@JoinTable(name = "SYS_USER_TO_ROLE", joinColumns = { @JoinColumn(name = "ROLE_ID") }, inverseJoinColumns = { @JoinColumn(name = "USER_ID") })
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<User> getUserList() {
		return userList;
	}

	
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	@Transient
	@JsonIgnore
	public String getAuthNames() {
		return ConvertUtils.convertElementPropertyToString(
				authorityList, "name", ", ");
	}

	@Transient
	@SuppressWarnings("unchecked")
	@JsonIgnore
	public List<Long> getAuthIds() {
		return ConvertUtils.convertElementPropertyToList(
				authorityList, "id");
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	
	
	@ManyToMany
	@JoinTable(name = "SYS_COMMON_TYPE_TO_ROLE", joinColumns = { @JoinColumn(name = "ROLE_ID") }, inverseJoinColumns = { @JoinColumn(name = "TYPE_ID") })
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<CommonType> getCommonTypeList() {
		return commonTypeList;
	}

	
	public void setCommonTypeList(List<CommonType> commonTypeList) {
		this.commonTypeList = commonTypeList;
	}

	
	/**
	 * @return indexUrl
	 */
	@Column(name="INDEX_URL",length=500)
	public String getIndexUrl() {
		return indexUrl;
	}

	
	/**
	 * @param indexUrl
	 */
	public void setIndexUrl(String indexUrl) {
		this.indexUrl = indexUrl;
	}

	
	/**
	 * @return portalUrl
	 */
	@Column(name="PORTAL_URL",length=500)
	public String getPortalUrl() {
		return portalUrl;
	}

	
	/**
	 * @param portalUrl
	 */
	public void setPortalUrl(String portalUrl) {
		this.portalUrl = portalUrl;
	}

	
	/**
	 * @return seeUserList
	 */
	@ManyToMany
	@JoinTable(name = "SYS_ROLE_SEE_USER", joinColumns = { @JoinColumn(name = "ROLE_ID") }, inverseJoinColumns = { @JoinColumn(name = "USER_ID") })
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<User> getSeeUserList() {
		return seeUserList;
	}

	
	/**
	 * @param seeUserList
	 */
	public void setSeeUserList(List<User> seeUserList) {
		this.seeUserList = seeUserList;
	}

	
	/**
	 * @return visibleAttr
	 */
	@Column(name="VISIBLE_ATTR",length=500)
	public String getVisibleAttr() {
		return visibleAttr;
	}

	
	/**
	 * @param visibleAttr
	 */
	public void setVisibleAttr(String visibleAttr) {
		this.visibleAttr = visibleAttr;
	}
	
	/**
	 * 获取可见用户ID
	 * 
	 * @return
	 */
	@Transient
	public String getSeeUserIds() {
		/*return ConvertUtils.convertElementPropertyToString(
				seeUserList, "id", ",");*/
		return seeUserIds;
	}
	
	/**
	 * 获取拥有该角色用户ID
	 * 
	 * @return
	 */
	@Transient
	public String getUserIds() {
		/*return ConvertUtils.convertElementPropertyToString(
				userList, "id", ",");*/
		return userIds;
	}

	
	/**
	 * @param seeUserIds
	 */
	public void setSeeUserIds(String seeUserIds) {
		this.seeUserIds = seeUserIds;
	}

	
	/**
	 * @param userIds
	 */
	public void setUserIds(String userIds) {
		this.userIds = userIds;
	}
	
	

}
