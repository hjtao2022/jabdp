/*
 * @(#)News.java
 * 2012-9-4 下午05:35:39
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.sysmng.accountmng.entity.Organization;


/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-4 下午05:35:39
 *  @since jdk1.6 
 */

/**
 * 系统新闻表
 */
@Entity
@Table(name = "SYS_NEWS")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class News extends BaseEntity{
	public static final String STATUS_0="0";// 0.草稿 1.已发布
	public static final String STATUS_1="1";
	private Long id;
    private String title;
    private String keyworlds;
    private String content;
    private String status;//0.草稿  1.已发布
    private Long type;
    private Long atmId;
    private Long clickCount;
    private List<Organization> organizationList = Lists.newArrayList();
	
    /**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_NEWS_ID")
	@SequenceGenerator(name = "SEQ_SYS_NEWS_ID", sequenceName = "SEQ_SYS_NEWS_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return title
	 */
	@Column(name = "TITLE", length = 200)
	public String getTitle() {
		return title;
	}

	
	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return keyworlds
	 */
	@Column(name = "KEY_VAL", length = 1000)
	public String getKeyworlds() {
		return keyworlds;
	}

	
	/**
	 * @param keyworlds
	 */
	public void setKeyworlds(String keyworlds) {
		this.keyworlds = keyworlds;
	}

	/**
	 * @return content
	 */
	@Lob
	@Column(name = "CONTENT_VAL", columnDefinition = "ntext")
	public String getContent() {
		return content;
	}

	
	/**
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	
	/**
	 * @return status
	 */
	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return type
	 *//*
	@Column(name = "TYPE", length = 50)
	public String getType() {
		return type;
	}

	
	*//**
	 * @param type
	 *//*
	public void setType(String type) {
		this.type = type;
	}*/


	
	/**
	 * @return atmId
	 */
	@Column(name = "ATM_ID")
	public Long getAtmId() {
		return atmId;
	}

	
	/**
	 * @return the type
	 */
	@Column(name = "TYPE")
	public Long getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(Long type) {
		this.type = type;
	}


	/**
	 * @param atmId
	 */
	public void setAtmId(Long atmId) {
		this.atmId = atmId;
	}


	/**
	 * @return the clickCount
	 */
	@Column(name = "CLICK_COUNT")
	public Long getClickCount() {
		return clickCount;
	}


	/**
	 * @param clickCount the clickCount to set
	 */
	public void setClickCount(Long clickCount) {
		this.clickCount = clickCount;
	}



	@ManyToMany
	@JoinTable(name = "SYS_NEWS_TO_ORG", joinColumns = { @JoinColumn(name = "NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "ORG_ID") })
	@Fetch(FetchMode.SUBSELECT)
	@OrderBy("id")
	//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	@JsonIgnore
	public List<Organization> getOrganizationList() {
		return organizationList;
	}


	
	public void setOrganizationList(List<Organization> organizationList) {
		this.organizationList = organizationList;
	}
    
	
	
    
}
