/*
 * @(#)NoticeDao.java
 * 2012-9-4 下午05:59:20
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.notice.entity.Notice;


/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-4 下午05:59:20
 *  @since jdk1.6 
 */

/**
 * 系统通知Dao类
 */
@Component
public class NoticeDao extends HibernateDao<Notice, Long>{

}
