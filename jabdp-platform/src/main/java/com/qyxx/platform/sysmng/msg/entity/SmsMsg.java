/*
 * @(#)SmsMsg.java
 * 2014-1-7 下午09:26:27
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.msg.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;


/**
 *  短消息
 *  @author gxj
 *  @version 1.0 2014-1-7 下午09:26:27
 *  @since jdk1.6 
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_SMS_MSG")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SmsMsg extends BaseEntity {

	private static final long serialVersionUID = 5030680877334773347L;
	
	public static final String STATUS_DRAFT = "0";//发送状态--草稿
	public static final String STATUS_UNSEND = "1";//发送状态--待发
	public static final String STATUS_SEND = "2";//发送状态--已发
	
	public static final String SOURCE_BYHAND = "0";//来源--手工创建
	public static final String SOURCE_SYSTEM = "1";//来源--系统自动创建
	
	private Long id;//编号
	private String serviceSms;//服务号
	private String targetPhone;//目标号
	private String sourcePhone;//源号，发起号码
	private String smsBody;//短信内容
	private Date sendTime;//发送时间
	private String status;//发送状态
	private String source;//短信来源
	private Date regSendTime;//定时发送时间
	private String result; //发送结果

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_SMS_MSG_ID")
	@SequenceGenerator(name="SEQ_SYS_SMS_MSG_ID", sequenceName="SEQ_SYS_SMS_MSG_ID")
    public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return serviceSms
	 */
	@Column(name="SERVICE_SMS",length=100)
	public String getServiceSms() {
		return serviceSms;
	}

	
	/**
	 * @param serviceSms
	 */
	public void setServiceSms(String serviceSms) {
		this.serviceSms = serviceSms;
	}

	
	/**
	 * @return targetPhone
	 */
	@Lob
	@Column(name="TARGET_PHONE", columnDefinition = "ntext")
	public String getTargetPhone() {
		return targetPhone;
	}

	
	/**
	 * @param targetPhone
	 */
	public void setTargetPhone(String targetPhone) {
		this.targetPhone = targetPhone;
	}

	
	/**
	 * @return sourcePhone
	 */
	@Column(name="SOURCE_PHONE",length=100)
	public String getSourcePhone() {
		return sourcePhone;
	}

	
	/**
	 * @param sourcePhone
	 */
	public void setSourcePhone(String sourcePhone) {
		this.sourcePhone = sourcePhone;
	}

	
	/**
	 * @return smsBody
	 */
	@Lob
	@Column(name="SMS_BODY", columnDefinition = "ntext")
	public String getSmsBody() {
		return smsBody;
	}

	
	/**
	 * @param smsBody
	 */
	public void setSmsBody(String smsBody) {
		this.smsBody = smsBody;
	}

	
	/**
	 * @return sendTime
	 */
	@Column(name="SEND_TIME")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getSendTime() {
		return sendTime;
	}

	
	/**
	 * @param sendTime
	 */
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	
	/**
	 * @return status
	 */
	@Column(name="STATUS",length=1)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	/**
	 * @return source
	 */
	@Column(name="SOURCE",length=1)
	public String getSource() {
		return source;
	}

	
	/**
	 * @param source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	
	/**
	 * @return regSendTime
	 */
	@Column(name="REG_SEND_TIME")
	@Temporal(value = TemporalType.TIMESTAMP)
	public Date getRegSendTime() {
		return regSendTime;
	}

	
	/**
	 * @param regSendTime
	 */
	public void setRegSendTime(Date regSendTime) {
		this.regSendTime = regSendTime;
	}

	/**
	 * @return result
	 */
	@Lob
	@Column(name="RESULT", columnDefinition = "ntext")
	public String getResult() {
		return result;
	}

	
	/**
	 * @param result
	 */
	public void setResult(String result) {
		this.result = result;
	}
}
