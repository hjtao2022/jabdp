/*
 * @(#)PathUtils.java
 * 2011-8-23 下午07:27:48
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc.utils;

import java.io.Console;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.ConsoleAppender;

import com.qyxx.jwp.bean.GsModule;
import com.qyxx.jwp.bean.Module;
import com.qyxx.platform.common.utils.encode.EncryptAndDecryptUtils;
import com.qyxx.platform.common.utils.encode.JaxbBinder;
import com.qyxx.platform.common.utils.template.DefaultFilenameFilter;

/**
 * 文件夹解析
 * @author YB
 * @version 1.0
 * @since 1.6 2011-8-23 下午07:27:48
 */

public class PathUtils {

	public static final String ENCODING = "UTF-8";

	private static JaxbBinder jaxbBinder = new JaxbBinder(
			Module.class);

	/**
	 * 迭代取某一個路徑下的所有xml文件
	 * 
	 * @param dirPath
	 * @param sourceList
	 * @return
	 * @throws Exception
	 */
	public static List<File> getSourceList(String dirPath,
			List<File> sourceList) throws Exception {
		File dir = new File(dirPath);
		File[] files = dir.listFiles(new DefaultFilenameFilter("xml",
				false));
		for (File f : files) {
			if (f.isDirectory()) {// 如果是文件夹则迭代
				getSourceList(dirPath + File.separator + f.getName(),
						sourceList);
			} else {
				sourceList.add(f);
			}
		}
		return sourceList;
	}

	/**
	 * 根据文件夹路径获取指定后缀名的子文件列表
	 * 
	 * @param dirPath
	 * @param extName
	 * @return
	 */
	public static String[] getDirFilePathList(String dirPath,
			String extName) {
		File dir = new File(dirPath);
		return dir.list(new DefaultFilenameFilter(extName));
	}

	/**
	 * 根据上传包路径封转成一个map
	 * 
	 * @param dirPath
	 * @return
	 * @throws Exception
	 */
	public static Map<String, GsModule> getGsModuleMap(String dirPath)
																		throws Exception {
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		Map<String, GsModule> moduleMap = new HashMap<String, GsModule>();
		for (File f : files) {
			if (f.isDirectory()) {// 如果是文件夹则迭代
				GsModule gsModule = new GsModule();
				gsModule.setName(f.getName());
				gsModule.setModuleMap(getGsModuleMaps(dirPath
						+ File.separator + f.getName()));
				moduleMap.put(f.getName(), gsModule);
			}
		}
		return moduleMap;
	}

	/**
	 * 根据模块路径封装成一个Map<String, Root>
	 */
	public static Map<String, Module> getGsModuleMaps(String dirPath)
																		throws Exception {
		File dir = new File(dirPath);
		File[] files = dir
				.listFiles(new DefaultFilenameFilter("xml"));
		Map<String, Module> moduleMap = new HashMap<String, Module>();
		for (File f : files) {
			if (f.isFile()) {
				Module root = getRootByFile(dirPath + File.separator
						+ f.getName());
				moduleMap.put(root.getModuleProperties().getKey(),
						root);
			}
		}
		return moduleMap;
	}

	/**
	 * 通过xml文件生成一个root
	 * 
	 * @param fileName
	 */
	public static Module getRootByFile(String fileName)
														throws Exception {
		String str = FileUtils.readFileToString(new File(fileName),ENCODING);
		str = EncryptAndDecryptUtils.aesDecrypt(str, "");
		Module root = (Module) jaxbBinder.fromXml(str);
		return root;
	}

	/**
	 * 根据模块路径及模块名获取对应模块文件xml对象
	 * 
	 * @param moduleDirPath
	 * @param moduleName
	 * @return
	 * @throws Exception
	 */
	public static Module getModuleFile(String moduleDirPath, String moduleName) throws Exception {
		Module m = new Module();
		File dir = new File(moduleDirPath);
		File[] files = dir.listFiles();
		String moduleXmlName = moduleName + ".xml";
		for (File f : files) {
			if (f.isDirectory()) {// 如果是文件夹则迭代
				String newDirPath = moduleDirPath+f.getName();
				File dirs = new File(newDirPath);
				String[] fileNames = dirs.list(new DefaultFilenameFilter("xml"));
				for (String fn : fileNames) {
					if (fn.equalsIgnoreCase(moduleXmlName)) {
						m = getRootByFile(newDirPath + File.separator + fn);
						return m;
					}
				}
			}
		}
		return m;
	}
	
	public static void main(String[] args) throws Exception {
		List<File> sourceList = new ArrayList<File>();
		PathUtils
				.getGsModuleMaps("E:\\javasoft\\jiawasoft\\doc\\GS20110618\\Config\\Module\\GSBill");
	}	
}
