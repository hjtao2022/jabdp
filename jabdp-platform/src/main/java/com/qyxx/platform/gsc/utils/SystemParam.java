/*
 * @(#)SystemParam.java
 * 2011-10-23 上午10:04:04
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsc.utils;

import com.qyxx.platform.common.utils.spring.SpringContextHolder;



/**
 *  系统参数常量
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-10-23 上午10:04:04
 */

public class SystemParam {
	
	private static String ptx = SpringContextHolder.getRootPath();
	
	private String uploadFilePath;
    private String uploadPicPath;
    private String virtualPicPath;
	private String qxAppPath;
	private String hotUpdateBatPath;
    
    private String systemTitle;//系统标题
    private String systemLogo;//系统logo
    private String systemIndexUrl;//首页地址
    
    private String systemPortalUrl;//门户地址
    private String systemLoginUrl;//登录地址
    private String systemPowerBy;//版权所有
	
	public String getVirtualPicPath() {
		return virtualPicPath;
	}


	public void setVirtualPicPath(String virtualPicPath) {
		this.virtualPicPath = parsePath(virtualPicPath);
	}


	public String getUploadPicPath() {
		return uploadPicPath;
	}


	public void setUploadPicPath(String uploadPicPath) {
		this.uploadPicPath = parsePath(uploadPicPath);
	}


	/**
	 * @return uploadFilePath
	 */
	public String getUploadFilePath() {
		return uploadFilePath;
	}

	
	/**
	 * @param uploadFilePath
	 */
	public void setUploadFilePath(String uploadFilePath) {
		this.uploadFilePath = parsePath(uploadFilePath);
	}
	
	/**
	 * 解析{ptx}、{ctx}路径
	 * 
	 * @param path
	 * @return
	 */
	public static String parsePath(String path) {
		String s = path.replace("{ptx}", ptx).replace("{ctx}", getCtx());
		return s;
	}
	
	/**
	 * 获取上下文路径
	 * 
	 * @return
	 */
	public static String getCtx() {
		String s = "";
		int index = ptx.lastIndexOf("\\");
		if(index >= 0) {
			s = ptx.substring(index + 1);
		} else {
			index = ptx.lastIndexOf("/");
			if(index >= 0) {
				s = ptx.substring(index+1);
			}
		}
		return "/" + s;
	}
	
	
	/**
	 * @return systemTitle
	 */
	public String getSystemTitle() {
		return systemTitle;
	}


	
	/**
	 * @param systemTitle
	 */
	public void setSystemTitle(String systemTitle) {
		this.systemTitle = systemTitle;
	}


	
	/**
	 * @return systemLogo
	 */
	public String getSystemLogo() {
		return systemLogo;
	}


	
	/**
	 * @param systemLogo
	 */
	public void setSystemLogo(String systemLogo) {
		this.systemLogo = systemLogo;
	}


	/**
	 * @return systemIndexUrl
	 */
	public String getSystemIndexUrl() {
		return systemIndexUrl;
	}


	/**
	 * @param systemIndexUrl
	 */
	public void setSystemIndexUrl(String systemIndexUrl) {
		this.systemIndexUrl = systemIndexUrl;
	}

	/**
	 * @return systemPortalUrl
	 */
	public String getSystemPortalUrl() {
		return systemPortalUrl;
	}


	/**
	 * @param systemPortalUrl
	 */
	public void setSystemPortalUrl(String systemPortalUrl) {
		this.systemPortalUrl = systemPortalUrl;
	}


	/**
	 * @return systemLoginUrl
	 */
	public String getSystemLoginUrl() {
		return systemLoginUrl;
	}


	/**
	 * @param systemLoginUrl
	 */
	public void setSystemLoginUrl(String systemLoginUrl) {
		this.systemLoginUrl = systemLoginUrl;
	}

	/**
	 * @return systemPowerBy
	 */
	public String getSystemPowerBy() {
		return systemPowerBy;
	}
	
	/**
	 * @param systemPowerBy
	 */
	public void setSystemPowerBy(String systemPowerBy) {
		this.systemPowerBy = systemPowerBy;
	}
	
	public String getQxAppPath() {
		return qxAppPath;
	}


	public void setQxAppPath(String qxAppPath) {
		this.qxAppPath = parsePath(qxAppPath);
	}

	public String getHotUpdateBatPath() {
		return hotUpdateBatPath;
	}

	public void setHotUpdateBatPath(String hotUpdateBatPath) {
		this.hotUpdateBatPath = hotUpdateBatPath;
	}


	public static void main(String[] args) {
		System.out.println(parsePath("{ptx}/upload/"));
	}

}
