/*
 * @(#)ModifyArrayDirective.java
 * 2012-7-5 下午03:44:39
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.freemarker;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;

/**
 *  修改数字序列指令
 *  
 *  @author gxj
 *  @version 1.0 2012-7-5 下午03:44:39
 *  @since jdk1.6
 */
public class ModifyArrayDirective implements TemplateDirectiveModel {

	@Override
	public void execute(Environment env, Map params,
			TemplateModel[] loopVars, TemplateDirectiveBody body)
																	throws TemplateException,
																	IOException {
		// 检查参数是否传入
		if (params.isEmpty()) {
			throw new TemplateModelException(
					"This directive parameters must be set.");
		}
		if (loopVars.length != 0) {
			throw new TemplateModelException(
					"This directive doesn't allow loop variables.");
		}
		
		NumberArraySequence seq = null;
		int index = 0;
		int value = 0;
		Iterator it = params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            String pname = (String) e.getKey();
            Object pvalue = e.getValue();

            if ("seq".equals(pname)) {
                if (!(pvalue instanceof NumberArraySequence)) {
                    throw new TemplateModelException(
                            "The \"seq\" parameter must be a "
                            + "NumberArraySequence variable.");
                }
                seq = (NumberArraySequence) pvalue;
            } else if ("index".equals(pname)) {
                if (!(pvalue instanceof TemplateNumberModel)) {
                    throw new TemplateModelException(
                            "The \"index\" parameter must be a "
                            + "numberical value.");
                }
                index = ((TemplateNumberModel) pvalue).getAsNumber()
                        .intValue();
            } else if ("value".equals(pname)) {
                value = ((TemplateNumberModel) pvalue).getAsNumber().intValue();
            }
        }
        if(seq!=null) {
        	seq.setValue(index, value);
        }
	}

}
