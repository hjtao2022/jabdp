/*
 * @(#)DynamicAnnotationSessionFactoryBean.java
 * 2011-8-10 下午08:59:05
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils.spring;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;



/**
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-8-10 下午08:59:05
 */

public class OldDynamicAnnotationSessionFactoryBean extends
		AnnotationSessionFactoryBean implements ApplicationListener<ApplicationEvent>{

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		System.out.println("触发事件1");
		if(event instanceof SessionFactoryChangeEvent){   
			System.out.println("触发事件2");
			rebuildSessionFactory();
			SessionFactoryChangeEvent sfcEvent = (SessionFactoryChangeEvent)event;  
        }   
	}
	
	
	@Override
	protected SessionFactory newSessionFactory(Configuration config)
																	throws HibernateException {
		// TODO Auto-generated method stub
		//System.out.println(System.currentTimeMillis());
		//System.out.println(config.getProperties());
		return super.newSessionFactory(config);
	}
	
    /** 
     * 重新加载hibernate映射 
     */  
    protected synchronized void rebuildSessionFactory() {  
    	System.out.println("重载sessionFactory开始");
        //destroy();  
        try {  
            afterPropertiesSet();  
        } catch (Exception ex) {  
            ex.printStackTrace();  
        }  
        System.out.println("重载sessionFactory结束");
    }  
	

}
