/*
 * @(#)DataProperties.java
 * 2012-6-25 下午06:04:50
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.menu;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  树形一级子节点
 *  
 *  @author thy
 *  @version 1.0 2012-6-25 下午04:05:11
 *  @since jdk1.6
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Business {
	/**
	 * 业务模块
	 */
	public static final String KEY_MODULE = "module";
	/**
	 * 系统模块
	 */
	public static final String KEY_SYSTEM = "system";
	/**
	 * 业务模块集合
	 */
	public static final String TYPE_MODULESS = "moduless";
	
	@XmlAttribute
	private String id;
	
	@XmlAttribute
	private String type;
	
	@XmlAttribute
	private String name;
	
	@XmlAttribute
	private String key;

	/**
	 * 图标
	 */
	@XmlAttribute
	private String iconSkin;
	
	@JsonProperty("children")
	@XmlElement
	private List<Modules> modules = new ArrayList<Modules>();

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the modules
	 */
	public List<Modules> getModules() {
		return modules;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @param modules the modules to set
	 */
	public void setModules(List<Modules> modules) {
		this.modules = modules;
	}

	/**
	 * @return the iconSkin
	 */
	public String getIconSkin() {
		return iconSkin;
	}

	/**
	 * @param iconSkin the iconSkin to set
	 */
	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}	
	
}
