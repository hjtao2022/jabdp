/*
 * @(#)DataProperties.java
 * 2012-6-25 下午06:04:50
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.qyxx.jwp.menu;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  树形二级子节点
 *  
 *  @author thy
 *  @version 1.0 2012-6-25 下午04:05:11
 *  @since jdk1.6
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Modules {
	
	/**
	 * 一级普通模块集合
	 */
	public static final String TYPE_MODULES = "modules";
	
	/**
	 * 一级业务字典集合
	 */
	public static final String TYPE_DICTS = "dicts";
	
	/**
	 * 一级自定义表单集合
	 */
	public static final String TYPE_FORMS = "forms";
	
	/**
	 * 一级移动端模块集合
	 */
	public static final String TYPE_APPS = "apps";
	
	@XmlAttribute
	private String id;
	
	@XmlAttribute
	private String type;
	
	@XmlAttribute
	private String name;
	
	@XmlAttribute
	private String key;

	/**
	 * 图标
	 */
	@XmlAttribute
	private String iconSkin;
	
	@JsonProperty("children")
	@XmlElement
	private List<Module> module = new ArrayList<Module>();

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the module
	 */
	public List<Module> getModule() {
		return module;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @param module the module to set
	 */
	public void setModule(List<Module> module) {
		this.module = module;
	}

	/**
	 * @return the iconSkin
	 */
	public String getIconSkin() {
		return iconSkin;
	}

	/**
	 * @param iconSkin the iconSkin to set
	 */
	public void setIconSkin(String iconSkin) {
		this.iconSkin = iconSkin;
	}

}
